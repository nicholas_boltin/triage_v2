**Triage Version 2**
=====================

Triage v2 is an Android OS application designed to assist nurses and caregivers in the emergency 
department by providing decision support for chemical triage during a Mass Casualty Incident.

**Getting Started**
=====================

Home Screen
---------------------
Login with a First Name and Last Name.  The name entered will then appear in the top left corner 
indicating a successful login.  This automatically opens the navigation drawer.

![Home Screen](Images/apphome1.resized.png)

![Navigation Drawer](Images/apphome2.resized.png)

Kiosk Mode
---------------------
When selecting Kiosk Mode from the Navigation Drawer Triage will operate as if it were a
stand-alone Kiosk terminal.  This mode is intended for patients to enter basic personal information 
and answers specific questions related to an Irritant Gas Syndrome.

![Kiosk Home](Images/kiosk1.resized.png)

### Barcode
A Barcode is used to identify the patient.  Triage uses the Camera2 API built into the hardware 
to scan the patients barcode.  When in Kiosk mode the barcode value becomes associated with the patient
and all their information entered into the system.

![Kiosk Barcode]()

### Patient's Demographics
Basic Patient identifiers are collected here.

![Kiosk Demographics](Images/kiosk2.resized.png)

### Irritant Gas Syndrome Questions
After gathering patient identifiers the user will progress through a series of questions related to 
an Irritant Gas Syndrome.  These questions help identify if the patient has been exposed to a toxic 
chemical.

![Kiosk Question](Images/kiosk3.resized.png)

### Patient Vitals
Oxygen Saturation and the patient's Heart Rate are collect here.  For the 2017 Drill users entered 
vitals given on a character's card.  Future versions will use bluetooth sensors.  Threshold alerts
for patient's SpO<sub>2</sub> and HR were developed and based on the patients age.

![Kiosk Vitals](Images/kiosk4.resized.png)

### Kiosk Map
Triage uses the Google Map API.  Patients are asked to enter the location where they first started 
feeling signs and symptoms.  For the 2017 Drill users entered addresses given on a character's card.
This information helps Triage identify if the patient was in the known effected area.  Patients can 
enter traditional addresses or click on the map to point to an area.

![Kiosk Map](Images/kiosk5.resized.png)

### Kiosk Complete Screen
Once the patient has gone through all the questions in the Kiosk System they can review all their 
questions in a overview screen.  Changes can be made by pressing the edit button.  When they are happy
with their answers they move to the finish screen and confirm that they are complete with the kiosk
process.  For the 2017 Drill simple instructions were given to proceed to the secondary triage area.

![Kiosk Overview]()

![Kiosk Finish](Images/kiosk6.resized.png)
