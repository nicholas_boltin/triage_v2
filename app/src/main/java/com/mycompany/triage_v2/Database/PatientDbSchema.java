package com.mycompany.triage_v2.Database;

public class PatientDbSchema {

    public static final class PatientTable{

        public static final String NAME = "patients";

        public static final class Cols{
            public static final String PATIENT_ID = "patient_id";
            public static final String FIRSTNAME =  "first_name";
            public static final String LASTNAME = "last_name";

            public static final String INITIAL_ENTRY = "initial_entry";

            public static final String DOB = "date_of_birth";
            public static final String AGE = "age";

            public static final String ADDRESS = "address";
            public static final String LAT = "latitude";
            public static final String LON = "longitude";

            public static final String VITALS_HR = "heart_rate";
            public static final String VITALS_O2 = "oxygen";
            public static final String VITALS_RR = "respiration";

            public static final String TRIAGE_LVL = "triage_lvl";
            public static final String EXPOSURE_LVL = "exposure_lvl";
            public static final String ACTION = "action_lvl";

            public static final String REC_EXPOSURE_LVL = "rec_exposure_lvl";
            public static final String REC_ACTION_LVL = "rec_action_lvl";

            public static final String QUESTION_PRELIM = "preliminary";

            public static final String QUESTION_ONE = "question_one";
            public static final String QUESTION_ONE_A = "question_one_a";
            public static final String QUESTION_ONE_B = "quetion_one_b";
            public static final String QUESTION_ONE_C = "quetion_one_c";
            public static final String QUESTION_ONE_D = "quetion_one_d";

            public static final String QUESTION_TWO = "question_two";
            public static final String QUESTION_TWO_A = "question_two_a";
            public static final String QUESTION_TWO_B = "quetion_two_b";
            public static final String QUESTION_TWO_C = "quetion_two_c";

            public static final String QUESTION_THREE = "question_three";
            public static final String QUESTION_FOUR = "question_four";
            public static final String QUESTION_FIVE = "question_five";

            public static final String COMPLETED_KIOSK = "completed_kiosk";

            public static final String NOTES_EXPOSURE = "notes_exposure";
            public static final String NOTES_ACTION = "notes_action";
        }


    }

    public static final class UpdatedTable{

        public static final String NAME = "updated";

        public static final class Cols{
            public static final String LAST_UPDATE = "last_update";
        }

    }

    public static final class ToSendQueueTable{

        public static final String NAME = "to_send";

        public static final class Cols{
            public static final String METHOD = "method";
            public static final String PATIENT_ID = "patient_id";
            public static final String DATA_ONE = "data_1";
            public static final String DATA_TWO = "data_2";
            public static final String DATA_THREE = "data_3";
            public static final String DATA_FOUR = "data_4";
            public static final String DATA_FIVE = "data_5";
            public static final String DATA_SIX = "data_6";
            public static final String DATA_SEVEN = "data_7";
            public static final String DATA_EIGHT = "data_8";
        }
    }

}
