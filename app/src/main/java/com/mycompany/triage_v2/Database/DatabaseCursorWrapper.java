package com.mycompany.triage_v2.Database;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.mycompany.triage_v2.Database.PatientDbSchema.PatientTable;
import com.mycompany.triage_v2.Database.PatientDbSchema.UpdatedTable;
import com.mycompany.triage_v2.Database.PatientDbSchema.ToSendQueueTable;
import com.mycompany.triage_v2.Models.Action;
import com.mycompany.triage_v2.Models.Answer;
import com.mycompany.triage_v2.Models.AnswerEnum;
import com.mycompany.triage_v2.Models.Exposure;
import com.mycompany.triage_v2.Models.Patient;
import com.mycompany.triage_v2.Models.ServerAnswer;
import com.mycompany.triage_v2.Models.Triage;

import java.util.Date;


public class DatabaseCursorWrapper extends CursorWrapper {

    private static final String TAG = "trace";


    /***********************************************************************************************
     * constructor
     * creates an instance of the cursor to search database
     */
    public DatabaseCursorWrapper(Cursor cursor){
        super(cursor);
    }

    /***********************************************************************************************
     * gets the values from the database and place them in a patient object
     * return patient
     */
    public Patient getPatient(){
        String patientId = getString(getColumnIndex(PatientTable.Cols.PATIENT_ID));
        String firstName = getString(getColumnIndex(PatientTable.Cols.FIRSTNAME));
        String lastName = getString(getColumnIndex(PatientTable.Cols.LASTNAME));

        long initialEntry = getLong(getColumnIndex(PatientTable.Cols.INITIAL_ENTRY));

        long date = getLong(getColumnIndex(PatientTable.Cols.DOB));
        int age = getInt(getColumnIndex(PatientTable.Cols.AGE));

        String address = getString(getColumnIndex(PatientTable.Cols.ADDRESS));
        double lat = getDouble(getColumnIndex(PatientTable.Cols.LAT));
        double lon = getDouble(getColumnIndex(PatientTable.Cols.LON));

        int heartR = getInt(getColumnIndex(PatientTable.Cols.VITALS_HR));
        int oxygen = getInt(getColumnIndex(PatientTable.Cols.VITALS_O2));
        int respirationR = getInt(getColumnIndex(PatientTable.Cols.VITALS_RR));

        String triageLvl = getString(getColumnIndex(PatientTable.Cols.TRIAGE_LVL));
        String exposureLvl = getString(getColumnIndex(PatientTable.Cols.EXPOSURE_LVL));
        String action = getString(getColumnIndex(PatientTable.Cols.ACTION));

        String recExposureLvl = getString(getColumnIndex(PatientTable.Cols.REC_EXPOSURE_LVL));
        String recAction = getString(getColumnIndex(PatientTable.Cols.REC_ACTION_LVL));

        String prelimQues = getString(getColumnIndex(PatientTable.Cols.QUESTION_PRELIM));
        Answer prelimAnswer = new ServerAnswer(prelimQues, false, false, false, false, false);

        boolean quesOne = (getInt(getColumnIndex(PatientTable.Cols.QUESTION_ONE)) != 0);
        boolean quesOneA = (getInt(getColumnIndex(PatientTable.Cols.QUESTION_ONE_A)) != 0);
        boolean quesOneB = (getInt(getColumnIndex(PatientTable.Cols.QUESTION_ONE_B)) != 0);
        boolean quesOneC = (getInt(getColumnIndex(PatientTable.Cols.QUESTION_ONE_C)) != 0);
        boolean quesOneD = (getInt(getColumnIndex(PatientTable.Cols.QUESTION_ONE_D)) != 0);
        Answer one = new ServerAnswer("", quesOne, quesOneA, quesOneB, quesOneC, quesOneD);

        boolean quesTwo = (getInt(getColumnIndex(PatientTable.Cols.QUESTION_TWO)) != 0);
        boolean quesTwoA = (getInt(getColumnIndex(PatientTable.Cols.QUESTION_TWO_A)) != 0);
        boolean quesTwoB = (getInt(getColumnIndex(PatientTable.Cols.QUESTION_TWO_B)) != 0);
        boolean quesTwoC = (getInt(getColumnIndex(PatientTable.Cols.QUESTION_TWO_C)) != 0);
        Answer two = new ServerAnswer("", quesTwo, quesTwoA, quesTwoB, quesTwoC, false);

        boolean quesThree = (getInt(getColumnIndex(PatientTable.Cols.QUESTION_THREE)) != 0);
        Answer three = new ServerAnswer("", quesThree, false, false, false, false);

        boolean quesFour = (getInt(getColumnIndex(PatientTable.Cols.QUESTION_FOUR)) != 0);
        Answer four = new ServerAnswer("", quesFour, false, false, false, false);

        boolean quesFive = (getInt(getColumnIndex(PatientTable.Cols.QUESTION_FIVE)) != 0);
        Answer five = new ServerAnswer("", quesFive, false, false, false, false);

        boolean completedKiosk = (getInt(getColumnIndex(PatientTable.Cols.COMPLETED_KIOSK)) != 0);

        String notesExposure = getString(getColumnIndex(PatientTable.Cols.NOTES_EXPOSURE));
        String notesAction = getString(getColumnIndex(PatientTable.Cols.NOTES_ACTION));

        Patient patient = new Patient(patientId);
        patient.setFirstName(firstName);
        patient.setLastName(lastName);

        patient.setInitialEntry(new Date(initialEntry));

        patient.setDob(new Date(date));
        patient.setAge(age);

        patient.setAddress(address);
        patient.setLatLng(new LatLng(lat, lon));

        patient.setHeart(heartR);
        patient.setOxygen(oxygen);
        patient.setRespiration(respirationR);

        patient.setTriage(Triage.valueOf(triageLvl));
        patient.setExposureLvl(Exposure.valueOf(exposureLvl));
        patient.setAction(Action.valueOf(action));

        patient.setRecExposureLvl(Exposure.valueOf(recExposureLvl));
        patient.setRecAction(Action.valueOf(recAction));

        patient.putAnswer(AnswerEnum.PRELIM, prelimAnswer);
        patient.putAnswer(AnswerEnum.ONE, one);
        patient.putAnswer(AnswerEnum.TWO, two);
        patient.putAnswer(AnswerEnum.THREE, three);
        patient.putAnswer(AnswerEnum.FOUR, four);
        patient.putAnswer(AnswerEnum.FIVE, five);

        patient.setCompletedKiosk(completedKiosk);

        patient.setExposureNotes(notesExposure);
        patient.setActionNotes(notesAction);

        return patient;
    }

    public String getTimeStamp(){
        return getString(getColumnIndex(UpdatedTable.Cols.LAST_UPDATE));
    }

    public String [] getData(){
        String array [] = new String[10];

        array [0] = getString(getColumnIndex(ToSendQueueTable.Cols.METHOD));
        array [1] = getString(getColumnIndex(ToSendQueueTable.Cols.PATIENT_ID));
        array [2] = getString(getColumnIndex(ToSendQueueTable.Cols.DATA_ONE));
        array [3] = getString(getColumnIndex(ToSendQueueTable.Cols.DATA_TWO));
        array [4] = getString(getColumnIndex(ToSendQueueTable.Cols.DATA_THREE));
        array [5] = getString(getColumnIndex(ToSendQueueTable.Cols.DATA_FOUR));
        array [6] = getString(getColumnIndex(ToSendQueueTable.Cols.DATA_FIVE));
        array [7] = getString(getColumnIndex(ToSendQueueTable.Cols.DATA_SIX));
        array [8] = getString(getColumnIndex(ToSendQueueTable.Cols.DATA_SEVEN));
        array [9] = getString(getColumnIndex(ToSendQueueTable.Cols.DATA_EIGHT));

        return array;
    }

}
