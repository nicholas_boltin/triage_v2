package com.mycompany.triage_v2.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.google.android.gms.maps.model.LatLng;
import com.mycompany.triage_v2.Database.PatientDbSchema.PatientTable;
import com.mycompany.triage_v2.Database.PatientDbSchema.UpdatedTable;
import com.mycompany.triage_v2.Database.PatientDbSchema.ToSendQueueTable;
import com.mycompany.triage_v2.Models.Action;
import com.mycompany.triage_v2.Models.Exposure;
import com.mycompany.triage_v2.Models.Patient;
import com.mycompany.triage_v2.Models.PatientMngr;
import com.mycompany.triage_v2.Models.Triage;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PatientDatabaseHelper extends SQLiteOpenHelper{

    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "patientDatabase.db";
    private static final String START_DATE = "1980-05-02 09:00:00.00";

    /***********************************************************************************************
     * constructor
     */
    public PatientDatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, VERSION);
    }

    /***********************************************************************************************
     * checks if a database exists on device
     * if one exists, do nothing
     * if one doesn't exist, create one with the designated table
     */
    @Override
    public void onCreate(SQLiteDatabase db){

        // table to track patients
        db.execSQL("create table " + PatientTable.NAME + "(" +
                " _id integer primary key autoincrement, " +
                PatientTable.Cols.PATIENT_ID + ", " +
                PatientTable.Cols.FIRSTNAME + ", " +
                PatientTable.Cols.LASTNAME + ", " +
                PatientTable.Cols.DOB + ", " +
                PatientTable.Cols.AGE + ", " +
                PatientTable.Cols.ADDRESS + ", " +
                PatientTable.Cols.LAT + ", " +
                PatientTable.Cols.LON + ", " +
                PatientTable.Cols.VITALS_HR + ", " +
                PatientTable.Cols.VITALS_O2 + ", " +
                PatientTable.Cols.VITALS_RR + ", " +
                PatientTable.Cols.TRIAGE_LVL + ", " +
                PatientTable.Cols.EXPOSURE_LVL + ", " +
                PatientTable.Cols.ACTION + ", " +
                PatientTable.Cols.QUESTION_PRELIM + ", " +
                PatientTable.Cols.QUESTION_ONE + ", " +
                PatientTable.Cols.QUESTION_ONE_A + ", " +
                PatientTable.Cols.QUESTION_ONE_B + ", " +
                PatientTable.Cols.QUESTION_ONE_C + ", " +
                PatientTable.Cols.QUESTION_ONE_D + ", " +
                PatientTable.Cols.QUESTION_TWO + ", " +
                PatientTable.Cols.QUESTION_TWO_A + ", " +
                PatientTable.Cols.QUESTION_TWO_B + ", " +
                PatientTable.Cols.QUESTION_TWO_C + ", " +
                PatientTable.Cols.QUESTION_THREE + ", " +
                PatientTable.Cols.QUESTION_FOUR + ", " +
                PatientTable.Cols.QUESTION_FIVE + ", " +
                PatientTable.Cols.COMPLETED_KIOSK + ", " +
                PatientTable.Cols.REC_EXPOSURE_LVL + ", " +
                PatientTable.Cols.REC_ACTION_LVL + ", " +
                PatientTable.Cols.NOTES_EXPOSURE + ", " +
                PatientTable.Cols.NOTES_ACTION + ", " +
                PatientTable.Cols.INITIAL_ENTRY +
                ")"
        );

        // table to track updates
        db.execSQL("create table " + UpdatedTable.NAME + "(" +
                " _id integer primary key autoincrement, " +
                UpdatedTable.Cols.LAST_UPDATE +
                ")"
        );

        insertInitialDate(db);

        // table to track data not sent to server
        db.execSQL("create table " + ToSendQueueTable.NAME + "(" +
                " _id integer primary key autoincrement, " +
                ToSendQueueTable.Cols.METHOD + ", " +
                ToSendQueueTable.Cols.PATIENT_ID + ", " +
                ToSendQueueTable.Cols.DATA_ONE + ", " +
                ToSendQueueTable.Cols.DATA_TWO + ", " +
                ToSendQueueTable.Cols.DATA_THREE + ", " +
                ToSendQueueTable.Cols.DATA_FOUR + ", " +
                ToSendQueueTable.Cols.DATA_FIVE + ", " +
                ToSendQueueTable.Cols.DATA_SIX + ", " +
                ToSendQueueTable.Cols.DATA_SEVEN + ", " +
                ToSendQueueTable.Cols.DATA_EIGHT +
                ")"
        );

        // for testing purposes only
        //insertPatient(db,createBruce());
    }

    /***********************************************************************************************
     * upgrades the database
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){

    }

    /***********************************************************************************************
     * inserts a patient into the given database
     */
    private void insertInitialDate(SQLiteDatabase db){
        ContentValues values = PatientMngr.getLastUpdated(START_DATE);
        db.insert(UpdatedTable.NAME, null, values);

    }

    /***********************************************************************************************
     * inserts a patient into the given database
     */
    public void insertPatient(SQLiteDatabase db, Patient patient){
        ContentValues values = PatientMngr.getContentValuesUpdate(patient);

        db.insert(PatientTable.NAME, null, values);
    }

    /***********************************************************************************************
     * creates an instance of the greatest super hero that ever lived - The Batman
     */
    public Patient createBruce(){
        String bigInteger = "878326002073";
        LatLng latLng = new LatLng(34.042, -80.962);
        DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
        String bruceDob = "2/19/1980";
        Date bruce = null;

        try{
            bruce = format.parse(bruceDob);
        }catch (Exception e){}

        Patient bruceWayne = new Patient(bigInteger);
        bruceWayne.setFirstName("Bruce");
        bruceWayne.setLastName("Wayne");
        bruceWayne.setLatLng(latLng);
        bruceWayne.setAddress("Trenholm Rd. Columbia SC. 29210");
        bruceWayne.setDob(bruce);
        bruceWayne.setOxygen(85);
        bruceWayne.setHeart(65);
        bruceWayne.setTriage(Triage.NONCRITICAL);
        bruceWayne.setExposureLvl(Exposure.POTENTIALEXPOSURE);
        bruceWayne.setAction(Action.MONITOR);
        bruceWayne.setActionNotes("this is a test");
        bruceWayne.setExposureNotes("this is another test");

        /*
        boolean array[] = {false, false, false, false};
        boolean arrayTwo[] = {true, false, false, true};
        Answer prelim = new PrelimQues("Car accident");
        Answer a1 = new ComboQues(false, array);
        Answer a2 = new ComboQues(true, arrayTwo);
        Answer a3 = new BooleanQues(true);
        Answer a4 = new BooleanQues(false);
        Answer a5 = new BooleanQues(true);
        bruceWayne.putAnswer(AnswerEnum.PRELIM, prelim);
        bruceWayne.putAnswer(AnswerEnum.ONE, a1);
        bruceWayne.putAnswer(AnswerEnum.TWO, a2);
        bruceWayne.putAnswer(AnswerEnum.THREE, a3);
        bruceWayne.putAnswer(AnswerEnum.FOUR, a4);
        bruceWayne.putAnswer(AnswerEnum.FIVE, a5);
        */

        bruceWayne.setCompletedKiosk(true);

        return bruceWayne;
    }

}
