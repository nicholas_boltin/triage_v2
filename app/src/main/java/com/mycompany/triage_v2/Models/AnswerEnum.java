package com.mycompany.triage_v2.Models;

public enum AnswerEnum {
    PRELIM, ONE, TWO, THREE, FOUR, FIVE
}
