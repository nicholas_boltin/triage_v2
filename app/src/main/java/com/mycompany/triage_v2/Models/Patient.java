package com.mycompany.triage_v2.Models;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Patient {

    private static final String TAG = "trace";

    private static final String PATIENT = "PATIENT";
    private static final String ADDRESS = "NO ADDRESS ENTERED";
    private static final String DATE_OF_BIRTH = "NO BIRTH DATE ENTERED";
    private static final int ARRAY_LENGTH_ANSWERS = 6;
    private static final int ARRAY_LENGTH_VITALS = 3;

    private String patientId;
    private String firstName;
    private String lastName;
    private Date dob;
    private int age;
    private String address;
    private int vitals[];
    private LatLng latLng;
    private Triage triageLvl;
    private Exposure exposureLvl;
    private Action action;

    private Exposure recExposureLvl;
    private Action recAction;

    private Answer[] answersArray;

    private boolean completedKiosk = false;

    private String exposureNotes;
    private String actionNotes;

    private Date initialEntry;

    /***********************************************************************************************
     * constructor
     * creates a patient object
     */
    public Patient(String patientId){
        this.patientId = patientId;
        firstName = PATIENT;
        lastName = patientId;

        dob = new Date();
        initialEntry = new Date();

        triageLvl = Triage.NONE;
        exposureLvl = Exposure.NONE;
        action = Action.NONE;

        recExposureLvl = Exposure.NONE;
        recAction = Action.NONE;

        answersArray = new Answer[ARRAY_LENGTH_ANSWERS];
        vitals = new int[ARRAY_LENGTH_VITALS];
    }

    /***********************************************************************************************
     * returns patientId
     */
    public String getPatientId(){
        return patientId;
    }

    /***********************************************************************************************
     * returns firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /***********************************************************************************************
     * set patient firstName
     */
    public void setFirstName(String name) {
        this.firstName = name;
    }

    /***********************************************************************************************
     * return lastName
     */
    public String getLastName() {
        return lastName;
    }

    /***********************************************************************************************
     * set patient lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /***********************************************************************************************
     * return patient date of birth
     */
    public Date getDob() {
        return dob;
    }

    /***********************************************************************************************
     * return patient birthday as a string
     */
    private String getStringDob(){
        DateFormat dateFormat = new SimpleDateFormat("M/dd/yyyy");
        Date today = new Date();
        Date test = new Date(0);
        long difference = today.getTime() - dob.getTime();

        // check if a dob was entered
        if(dob.compareTo(test) == 0) return DATE_OF_BIRTH;

        // check if patient is under the age of one
        if(age == 0){
            long diffDays = (difference / (24 * 60 * 60 * 1000)) / 30;
            return dateFormat.format(dob) + ",  Age: " + diffDays + " months";
        }

        return dateFormat.format(dob) + ",  Age: " + age;
    }

    /***********************************************************************************************
     * set patient date of birth and patient's age
     */
    public void setDob(Date dob) {
        this.dob = dob;
        if(dob != null){
            Date today = new Date();
            long difference = today.getTime() - dob.getTime();
            long diffDays = (difference / (24 * 60 * 60 * 1000)) / 365;
            age = (int) diffDays;
        }
    }

    /***********************************************************************************************
     * return patient age
     */
    public int getAge(){
        return age;
    }

    /***********************************************************************************************
     * return patient age
     */
    public void setAge(int age){
        this.age = age;
    }

    /***********************************************************************************************
     * set the initial entry of a patient
     */
    public void setInitialEntry(Date initialEntry){
        this.initialEntry = initialEntry;
    }

    /***********************************************************************************************
     * set the initial entry of a patient
     */
    public Date getInitialEntry(){
        return initialEntry;
    }

    /***********************************************************************************************
     * return an array of vitals
     */
    public int[] getVitals() {
        return vitals;
    }

    /***********************************************************************************************
     * set patient vitals
     */
    public void setVitals(int vitals[]){
        this.vitals = vitals;
    }

    /***********************************************************************************************
     * return patient oxygen
     */
    public int getOxygen(){
        return vitals[0];
    }

    /***********************************************************************************************
     * set patient oxygen
     */
    public void setOxygen(int oxygen){
        vitals[0] = oxygen;
    }

    /***********************************************************************************************
     * return patient heart rate
     */
    public int getHeartRate(){
        return vitals[1];
    }

    /***********************************************************************************************
     * set patient heart rate
     */
    public void setHeart(int heart){
        vitals[1] = heart;
    }

    /***********************************************************************************************
     * return patient respiration
     */
    public int getRespiration(){
        return vitals[2];
    }

    /***********************************************************************************************
     * set patient respiration
     */
    public void setRespiration(int respiration){
        vitals[2] = respiration;
    }

    /***********************************************************************************************
     * return patient address
     */
    public String getAddress() {
        if(address == null && latLng == null){
            return ADDRESS;
        }
        return address;
    }

    /***********************************************************************************************
     * set patient address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /***********************************************************************************************
     * return latitude and longitude object for patient address
     */
    public LatLng getLatLng() {
        if(latLng == null){
            latLng = new LatLng(0,0);
            addressAdjust();
        }
        return latLng;
    }

    /***********************************************************************************************
     * ensures that patients string address is null if no address has been given by a patient
     * used to ensure static string fields display in recycler view if no address was provided
     * called from getLatLng
     */
    private void addressAdjust(){
        address = null;
    }

    /***********************************************************************************************
     * set latitude and longitude for a patient address
     */
    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    /***********************************************************************************************
     * set patient triage level
     */
    public void setTriage(Triage triageLvl) { this.triageLvl = triageLvl; }

    /***********************************************************************************************
     * get patient triage level
     */
    public Triage getTriage() { return triageLvl; }

    /***********************************************************************************************
     * get patient exposure level
     */
    public Exposure getExposureLvl() {
        return exposureLvl;
    }

    /***********************************************************************************************
     * set patient exposure level
     */
    public void setExposureLvl(Exposure exposureLvl) {
        this.exposureLvl = exposureLvl;
    }

    /***********************************************************************************************
     * get patient recommended exposure level
     */
    public Exposure getRecExposureLvl() {
        return recExposureLvl;
    }

    /***********************************************************************************************
     * set patient recommended exposure level
     */
    public void setRecExposureLvl(Exposure exposureLvl) {
        this.recExposureLvl = exposureLvl;
    }

    /***********************************************************************************************
     * get patient action level
     */
    public Action getAction() {
        return action;
    }

    /***********************************************************************************************
     * set patient action level
     */
    public void setAction(Action action) {
        this.action = action;
    }

    /***********************************************************************************************
     * get patient action level
     */
    public Action getRecAction() {
        return recAction;
    }

    /***********************************************************************************************
     * set patient action level
     */
    public void setRecAction(Action action) {
        this.recAction = action;
    }

    /***********************************************************************************************
     * returns a title with general patient data
     */
    public String getTitle(){
        return "Patient: " + fullName() + ",  DOB: " + getStringDob() +  ",  SpO2: " + getOxygen()
                + ",  Pulse: " + getHeartRate();
    }

    /***********************************************************************************************
     * returns patient full name
     */
    public String fullName(){
        return firstName + " " + lastName;
    }

    /***********************************************************************************************
     * returns Answer object from answersArray
     */
    public Answer getAnswer(AnswerEnum whichAnswer){
        switch (whichAnswer){
            case PRELIM:
                return answersArray[0];
            case ONE:
                return answersArray[1];
            case TWO:
                return answersArray[2];
            case THREE:
                return answersArray[3];
            case FOUR:
                return answersArray[4];
            case FIVE:
                return answersArray[5];
            default:
                return null;
        }
    }

    /***********************************************************************************************
     * sets Answer object in answersArray
     */
    public void putAnswer(AnswerEnum whichLocation, Answer answer){

        switch (whichLocation){
            case PRELIM:
                answersArray[0] = answer;
                boolean answer0 = answer.isAnswerOne();
                break;
            case ONE:
                answersArray[1] = answer;
                break;
            case TWO:
                answersArray[2] = answer;
                break;
            case THREE:
                answersArray[3] = answer;
                break;
            case FOUR:
                answersArray[4] = answer;
                break;
            case FIVE:
                answersArray[5] = answer;
                break;
        }
    }

    /***********************************************************************************************
     * returns completedKiosk
     */
    public boolean getCompletedKiosk(){
        return completedKiosk;
    }

    /***********************************************************************************************
     * sets patient completed the kiosk
     * sets true only when patient has submitted their questionnaire in kiosk mode
     * called from KioskTabPagerActivity
     */
    public void setCompletedKiosk(boolean completedKiosk){
        this.completedKiosk = completedKiosk;
    }

    /***********************************************************************************************
     * returns patient exposure notes
     */
    public String getExposureNotes() {
        return exposureNotes;
    }

    /***********************************************************************************************
     * sets patient exposure notes
     */
    public void setExposureNotes(String exposureNotes) {
        this.exposureNotes = exposureNotes;
    }

    /***********************************************************************************************
     * returns patient action notes
     */
    public String getActionNotes() {
        return actionNotes;
    }

    /***********************************************************************************************
     * sets patient action notes
     */
    public void setActionNotes(String actionNotes) {
        this.actionNotes = actionNotes;
    }

}
