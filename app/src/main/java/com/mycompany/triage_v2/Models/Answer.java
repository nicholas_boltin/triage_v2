package com.mycompany.triage_v2.Models;

import android.util.Log;

public abstract class Answer {

    private static final int ARRAY_SIZE = 5;
    private static final String TAG = "trace";

    private boolean[] checkable;

    // switching over to new answer model
    private boolean answerOne, answerTwo, answerThree, answerFour, answerFive;
    private String prelimAns;


    public boolean isAnswerOne() {
        return answerOne;
    }

    public boolean isAnswerTwo() {
        return answerTwo;
    }

    public boolean isAnswerThree() {
        return answerThree;
    }

    public boolean isAnswerFour() {
        return answerFour;
    }

    public boolean isAnswerFive() {
        return answerFive;
    }

    public String getPrelimAns() {
        return prelimAns;
    }

    Answer(String prelimQues, boolean answerOne, boolean answerTwo, boolean answerThree,
           boolean answerFour, boolean answerFive){
        this.prelimAns = prelimQues;
        this.answerOne = answerOne;
        this.answerTwo = answerTwo;
        this.answerThree = answerThree;
        this.answerFour = answerFour;
        this.answerFive = answerFive;

        checkable = new boolean[ARRAY_SIZE];
        setCheckable(answerOne, answerTwo, answerThree, answerFour, answerFive);
    }

    private void setCheckable(boolean answerOne, boolean answerTwo, boolean answerThree,
                             boolean answerFour, boolean answerFive){

        checkable [0] = answerOne;
        checkable [1] = answerTwo;
        checkable [2] = answerThree;
        checkable [3] = answerFour;
        checkable [4] = answerFive;
    }

    public boolean[] getCheckable() {
        return checkable;
    }

}
