package com.mycompany.triage_v2.Models;

public enum Triage {
    NONCRITICAL, CRITICAL, NONE
}
