package com.mycompany.triage_v2.Models;

public enum Action {
    URGENT, // IMMEDIATE
    MONITOR,
    EXIT, //RETURN TO HOSPITAL SYSTEM
    NONE
}
