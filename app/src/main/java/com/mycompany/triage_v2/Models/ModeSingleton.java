package com.mycompany.triage_v2.Models;

/**
 * Created by Daniel Vu on 9/24/2015.
 */
public class ModeSingleton {
    private static ModeSingleton instance;

    public enum Mode {
        KIOSK,
        MOBILE
    }

    Mode mode;

    private ModeSingleton() {
    }

    public static synchronized ModeSingleton getInstance() {
        if (instance == null) {
            instance = new ModeSingleton();
        }
        return instance;
    }

    public Mode getMode() {
        return mode;
    }

    public static synchronized void setMode(Mode mode) {
        instance.mode = mode;
    }
}
