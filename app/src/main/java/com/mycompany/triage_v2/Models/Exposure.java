package com.mycompany.triage_v2.Models;

public enum Exposure {
    EXPOSED, // EXPOSURE
    NOTEXPOSED, // NO EXPOSURE
    POTENTIALEXPOSURE,
    NONE
}
