package com.mycompany.triage_v2.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.mycompany.triage_v2.Database.DatabaseCursorWrapper;
import com.mycompany.triage_v2.Database.PatientDatabaseHelper;
import com.mycompany.triage_v2.Database.PatientDbSchema;
import com.mycompany.triage_v2.Database.PatientDbSchema.PatientTable;
import com.mycompany.triage_v2.Database.PatientDbSchema.UpdatedTable;
import com.mycompany.triage_v2.Database.PatientDbSchema.ToSendQueueTable;
import com.mycompany.triage_v2.Helpers.Magic;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PatientMngr {

    private static final String TAG = "trace";

    private static final String START_DATE = "1980-05-02 09:00:00.00";

    private static final String GET_PATIENTS = "get_patients";

    private static final String SERVER_RESPONSE_PATIENTS = "server_response";
    private static final String SERVER_RESPONSE_ANSWERS = "patient_answers";
    private static final String SERVER_RESPONSE_VITALS = "patient_vitals";
    private static final String SERVER_RESPONSE_TRIAGE = "patient_triage";

    private static final String LAST_UPDATE = "updated";

    private static final String PATIENT_ID = "patient_id";
    private static final String LAST_NAME = "last_name";
    private static final String FIRST_NAME = "first_name";
    private static final String ADDRESS = "address";
    private static final String DOB = "dob";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";

    private static final String INITIAL_ENTRY = "initial_entry";

    private static final String QUESTION_NUMBER = "question_number";

    private static final String COMPLETE = "kiosk_complete";

    private static final String ANSWER_ONE = "q_one";
    private static final String ANSWER_ONE_A = "q_one_a";
    private static final String ANSWER_ONE_B = "q_one_b";
    private static final String ANSWER_ONE_C = "q_one_c";
    private static final String ANSWER_ONE_D = "q_one_d";

    private static final String ANSWER_TWO = "q_two";
    private static final String ANSWER_TWO_A = "q_two_a";
    private static final String ANSWER_TWO_B = "q_two_b";
    private static final String ANSWER_TWO_C = "q_two_c";

    private static final String ANSWER_THREE = "q_three";
    private static final String ANSWER_FOUR = "q_four";
    private static final String ANSWER_FIVE = "q_five";
    private static final String ANSWER_SIX = "q_six";
    private static final String BLANK = "";

    private static final String VITALS_HEART = "heart_rate";
    private static final String VITALS_OXYGEN = "oxygen";
    private static final String VITALS_RESPIRATION = "respiration_rate";

    private static final String TRIAGE_LVL = "p_triage";
    private static final String TRIAGE_EXPOSURE = "exposure";
    private static final String TRIAGE_ACTION = "action";

    private static final String EXPOSURE_NOTES = "e_comments";
    private static final String ACTION_NOTES = "a_comments";

    private static PatientMngr patientMngr;
    private Context context;
    private SQLiteDatabase database;
    private String userId = "";

    private static int criticalPatients, nonCriticalPatients;
    private static int exposedPatients, potentialExposedPatients, nonExposedPatients;
    private static int exitPatient, monitorPatient, urgentPatient;
    private static int noCriticalAssigned, noExposureAssigned, noActionAssigned;

    /***********************************************************************************************
     * checks if an instance of patientMngr exists.
     * if one doesn't exist call to create one
     * if one does exist return the patientMngr that has already been created
     */
    public static PatientMngr get(Context context){

        if(patientMngr == null){
            patientMngr = new PatientMngr(context);
        }
        return patientMngr;
    }

    /***********************************************************************************************
     * constructor
     * creates an instance of PatientMngr and gets the database
     */
    private PatientMngr(Context context){
        context = context.getApplicationContext();
        database = new PatientDatabaseHelper(context).getWritableDatabase();
    }

    /***********************************************************************************************
     * Sets the user id to the current user
     * @param userId - the string id of the current user
     */
    public void setUserId(String userId){
        this.userId = userId;
    }

    /***********************************************************************************************
     * returns the id of the current user
     * @return      userId - the id of the current user
     */
    public String getUserId(){
        return userId;
    }

    /***********************************************************************************************
     * sets the user id to empty on logout
     */
    public void resetUserId(){
        this.userId = "";
    }

    /***********************************************************************************************
     * parses the jsonArray and creates patients for the sqlite db
     */
    public void parseThis(String jsonString){

        try{
            JSONObject jsonObject = new JSONObject(jsonString);
            JSONArray jsonArrayPatients = jsonObject.getJSONArray(SERVER_RESPONSE_PATIENTS);

            int count = 0;

            String lastUpdated = BLANK;
            String patientId, firstName, lastName, address, birth, initialEntry, complete;
            Date dob = null;
            Date initial = null;
            Double lat, lng;

            Log.i(TAG, "length of array (PatientMngr): " + jsonArrayPatients.length());

            while (count < jsonArrayPatients.length()){

                JSONObject JO = jsonArrayPatients.getJSONObject(count);
                patientId = JO.getString(PATIENT_ID);
                Log.i(TAG, "Patient id (PatientMngr): " + patientId);
                firstName = JO.getString(FIRST_NAME);
                lastName = JO.getString(LAST_NAME);
                address = JO.getString(ADDRESS);
                lat = JO.getDouble(LATITUDE);
                lng = JO.getDouble(LONGITUDE);
                birth = JO.getString(DOB);
                initialEntry = JO.getString(INITIAL_ENTRY);

                complete = JO.getString(COMPLETE);

                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                try{
                    dob = dateFormat.parse(birth);
                } catch (ParseException e){

                }

                dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                try{
                    initial = dateFormat.parse(initialEntry);
                } catch (ParseException e){

                }

                Patient patient = new Patient(patientId);
                patient.setFirstName(firstName);
                patient.setLastName(lastName);
                patient.setAddress(address);
                LatLng latLng = new LatLng(lat, lng);
                patient.setLatLng(latLng);
                patient.setDob(dob);
                patient.setInitialEntry(initial);

                if(complete.equals("1")){
                    patient.setCompletedKiosk(true);
                } else {
                    patient.setCompletedKiosk(false);
                }

                    boolean quesOne = (JO.getInt(ANSWER_ONE) != 0);
                    boolean quesOneA = (JO.getInt(ANSWER_ONE_A) != 0);
                    boolean quesOneB = (JO.getInt(ANSWER_ONE_B) != 0);
                    boolean quesOneC = (JO.getInt(ANSWER_ONE_C) != 0);
                    boolean quesOneD = (JO.getInt(ANSWER_ONE_D) != 0);

                    Answer one = new ServerAnswer(BLANK, quesOne, quesOneA, quesOneB, quesOneC, quesOneD);
                    patient.putAnswer(AnswerEnum.ONE, one);

                    boolean quesTwo = (JO.getInt(ANSWER_TWO) != 0);
                    boolean quesTwoA = (JO.getInt(ANSWER_TWO_A) != 0);
                    boolean quesTwoB = (JO.getInt(ANSWER_TWO_B) != 0);
                    boolean quesTwoC = (JO.getInt(ANSWER_TWO_C) != 0);

                    Answer two = new ServerAnswer(BLANK, quesTwo, quesTwoA, quesTwoB, quesTwoC, false);
                    patient.putAnswer(AnswerEnum.TWO, two);

                    boolean quesThree = (JO.getInt(ANSWER_THREE) != 0);
                    Answer three = new ServerAnswer(BLANK, quesThree, false, false, false, false);
                    patient.putAnswer(AnswerEnum.THREE, three);

                    boolean quesFour = (JO.getInt(ANSWER_FOUR) != 0);
                    Answer four = new ServerAnswer(BLANK, quesFour, false, false, false, false);
                    patient.putAnswer(AnswerEnum.FOUR, four);

                    boolean quesFive = (JO.getInt(ANSWER_FIVE) != 0);
                    Answer five = new ServerAnswer(BLANK, quesFive, false, false, false, false);
                    patient.putAnswer(AnswerEnum.FIVE, five);

                    String prelimQues = JO.getString(ANSWER_SIX);
                    Answer prelimAnswer = new ServerAnswer(prelimQues, false, false, false, false, false);
                    patient.putAnswer(AnswerEnum.PRELIM, prelimAnswer);

                patient.setHeart(JO.getInt(VITALS_HEART));
                patient.setOxygen(JO.getInt(VITALS_OXYGEN));
                patient.setRespiration(JO.getInt(VITALS_RESPIRATION));

                patient.setTriage(Triage.valueOf(JO.getString(TRIAGE_LVL)));
                patient.setExposureLvl(Exposure.valueOf(JO.getString(TRIAGE_EXPOSURE)));
                patient.setAction(Action.valueOf(JO.getString(TRIAGE_ACTION)));

                patient.setExposureNotes(JO.getString(ACTION_NOTES));
                patient.setActionNotes(JO.getString(EXPOSURE_NOTES));

                if(patientExist(patientId)){
                    updatePatient(patient);
                    // deletePatient(patientId);
                    // addPatient(patient, context);
                } else {
                    addPatient(patient, context);
                    updatePatient(patient);
                }

                // updatePatient(patient);
                lastUpdated = JO.getString(LAST_UPDATE);

                count++;

                addUpdatedTimeStamp(lastUpdated);
                Log.i(TAG, "Next update request w/n while (PatientMngr): " + lastUpdated);
            }

            addUpdatedTimeStamp(lastUpdated);
            Log.i(TAG, "Next update request (PatientMngr): " + lastUpdated);

        } catch (JSONException e){
            e.printStackTrace();
        }

    }

    /***********************************************************************************************
     * adds patients to the database
     * gets the values of patient by calling getContentValuesInitial
     * initial patient and values added to database
     * all other fields left null
     */
    public void addPatient(Patient patient, Context context){
        ContentValues values = getContentValuesInitial(patient);
        database.insert(PatientTable.NAME, null, values);
    }

    /***********************************************************************************************
     * creates an array list of patients by traversing the database
     * used to populate recycler views
     * returns ArrayList patients
     */
    public List<Patient> getPatientList(){
        List<Patient> patients = new ArrayList<>();

        DatabaseCursorWrapper cursor = queryDatabase(PatientTable.NAME, null, null);

        try{
            cursor.moveToFirst();
            while(!cursor.isAfterLast()){
                patients.add(cursor.getPatient());
                cursor.moveToNext();
            }
        }finally {
            cursor.close();
        }
        return patients;
    }

    /***********************************************************************************************
     * creates an array list of patients by traversing the database
     * used to populate recycler views
     * returns ArrayList patients
     */
    public List<Patient> getPatientListNeedAssessment(){
        List<Patient> patients = new ArrayList<>();

        DatabaseCursorWrapper cursor = queryDatabase(PatientTable.NAME, null, null);

        try{
            cursor.moveToFirst();
            while(!cursor.isAfterLast()){
                if(cursor.getPatient().getExposureLvl().equals(Exposure.NONE) ||
                        cursor.getPatient().getAction().equals(Action.NONE)){
                    patients.add(cursor.getPatient());
                }
                cursor.moveToNext();
            }
        }finally {
            cursor.close();
        }
        return patients;
    }

    /***********************************************************************************************
     * Gets counts for situation fragment by counting from local sqlite db
     *
     * @return      an array of int values
     */
    public int[] getSituationCount(){
        int [] counts = new int[11];

        // total count of patients
        Cursor cursor = database.rawQuery(
                "select count(*) from " + PatientTable.NAME + "", null);
        cursor.moveToFirst();
        counts[0] = cursor.getInt(0);
        cursor.close();

        /* Triage Lvl Counts **********************************************************************/

        // count of critical patients
        cursor = database.rawQuery(
                "select count(*) from " + PatientTable.NAME + " where " +
                        PatientTable.Cols.TRIAGE_LVL + " = '" + Triage.CRITICAL.toString()
                        + "'", null);
        cursor.moveToFirst();
        counts[1] = cursor.getInt(0);
        cursor.close();

        // count of noncritical patients
        cursor = database.rawQuery(
                "select count(*) from " + PatientTable.NAME + " where " +
                        PatientTable.Cols.TRIAGE_LVL + " = '" + Triage.NONCRITICAL.toString()
                        + "'", null);
        cursor.moveToFirst();
        counts[2] = cursor.getInt(0);
        cursor.close();

        /* Exposure counts ************************************************************************/
        // count of none
        cursor = database.rawQuery(
                "select count(*) from " + PatientTable.NAME + " where " +
                        PatientTable.Cols.EXPOSURE_LVL + " = '" + Exposure.NONE.toString()
                        + "'", null);
        cursor.moveToFirst();
        counts[3] = cursor.getInt(0);
        cursor.close();

        // count of notexposed
        cursor = database.rawQuery(
                "select count(*) from " + PatientTable.NAME + " where " +
                        PatientTable.Cols.EXPOSURE_LVL + " = '" + Exposure.NOTEXPOSED.toString()
                        + "'", null);
        cursor.moveToFirst();
        counts[4] = cursor.getInt(0);
        cursor.close();

        // count of potentialexposed
        cursor = database.rawQuery(
                "select count(*) from " + PatientTable.NAME + " where " +
                        PatientTable.Cols.EXPOSURE_LVL + " = '" +
                        Exposure.POTENTIALEXPOSURE.toString() + "'", null);
        cursor.moveToFirst();
        counts[5] = cursor.getInt(0);
        cursor.close();

        // count of exposed
        cursor = database.rawQuery(
                "select count(*) from " + PatientTable.NAME + " where " +
                        PatientTable.Cols.EXPOSURE_LVL + " = '" + Exposure.EXPOSED.toString()
                        + "'", null);
        cursor.moveToFirst();
        counts[6] = cursor.getInt(0);
        cursor.close();

        /* Action counts **************************************************************************/
        // count of none
        cursor = database.rawQuery(
                "select count(*) from " + PatientTable.NAME + " where " +
                        PatientTable.Cols.ACTION + " = '" + Action.NONE.toString() + "'", null);
        cursor.moveToFirst();
        counts[7] = cursor.getInt(0);
        cursor.close();

        // count of exit
        cursor = database.rawQuery(
                "select count(*) from " + PatientTable.NAME + " where " +
                        PatientTable.Cols.ACTION + " = '" + Action.EXIT.toString() + "'", null);
        cursor.moveToFirst();
        counts[8] = cursor.getInt(0);
        cursor.close();

        // count of monitor
        cursor = database.rawQuery(
                "select count(*) from " + PatientTable.NAME + " where " +
                        PatientTable.Cols.ACTION + " = '" + Action.MONITOR.toString() + "'", null);
        cursor.moveToFirst();
        counts[9] = cursor.getInt(0);
        cursor.close();

        // count of urgent
        cursor = database.rawQuery(
                "select count(*) from " + PatientTable.NAME + " where " +
                        PatientTable.Cols.ACTION + " = '" + Action.URGENT.toString() + "'", null);
        cursor.moveToFirst();
        counts[10] = cursor.getInt(0);
        cursor.close();

        return  counts;
    }

    /***********************************************************************************************
     * searches database for patient by patientId
     * returns patient
     */
    public Patient getPatient(String patientId){
        DatabaseCursorWrapper cursor = queryDatabase(
                PatientTable.NAME,
                PatientTable.Cols.PATIENT_ID + " = ?",
                new String[] { patientId }
        );

        try {
            if (cursor.getCount() == 0) {
                return null;
            }
            cursor.moveToFirst();
                return cursor.getPatient();
        }finally {
            cursor.close();
        }
    }

    /***********************************************************************************************
     * updates patient data
     * gets new values of patient by calling getContentValuesUpdate
     * places updated values into database
     */
    public void updatePatient(Patient patient){
        String patientId = patient.getPatientId();
        ContentValues values = getContentValuesUpdate(patient);

        database.update(PatientTable.NAME, values,
                PatientTable.Cols.PATIENT_ID + " = ?",
                new String[] {patientId});
    }

    /***********************************************************************************************
     * delete patient data
     */
    public void deletePatient(String patient_id){
        database.delete(PatientTable.NAME,
                PatientTable.Cols.PATIENT_ID + " = ?",
                new String[] {patient_id});
    }


    /***********************************************************************************************
     * gets the values of a patient object after initial creation that will be written to sqlite
     * called from addPatient
     * return values
     */
    private static ContentValues getContentValuesInitial(Patient patient){

        ContentValues values = new ContentValues();

        values.put(PatientTable.Cols.PATIENT_ID, patient.getPatientId());
        values.put(PatientTable.Cols.FIRSTNAME, patient.getFirstName());
        values.put(PatientTable.Cols.LASTNAME, patient.getLastName());

        values.put(PatientTable.Cols.TRIAGE_LVL, patient.getTriage().name());
        values.put(PatientTable.Cols.EXPOSURE_LVL, patient.getExposureLvl().name());
        values.put(PatientTable.Cols.ACTION, patient.getAction().name());

        values.put(PatientTable.Cols.REC_EXPOSURE_LVL, patient.getRecExposureLvl().name());
        values.put(PatientTable.Cols.REC_ACTION_LVL, patient.getRecAction().name());

        values.put(PatientTable.Cols.COMPLETED_KIOSK, patient.getCompletedKiosk());

        return values;
    }

    /***********************************************************************************************
     * gets patient values to be updated
     * called from updatePatient
     * returns values
     */
    public static ContentValues getContentValuesUpdate(Patient patient){

        ContentValues values = new ContentValues();

        values.put(PatientTable.Cols.PATIENT_ID, patient.getPatientId());
        values.put(PatientTable.Cols.FIRSTNAME, patient.getFirstName());
        values.put(PatientTable.Cols.LASTNAME, patient.getLastName());

        values.put(PatientTable.Cols.DOB, patient.getDob().getTime());
        values.put(PatientTable.Cols.INITIAL_ENTRY, patient.getInitialEntry().getTime());

        values.put(PatientTable.Cols.AGE, patient.getAge());
        values.put(PatientTable.Cols.ADDRESS, patient.getAddress());
        values.put(PatientTable.Cols.LAT, patient.getLatLng().latitude);
        values.put(PatientTable.Cols.LON, patient.getLatLng().longitude);

        values.put(PatientTable.Cols.VITALS_HR, patient.getHeartRate());
        values.put(PatientTable.Cols.VITALS_O2, patient.getOxygen());
        values.put(PatientTable.Cols.VITALS_RR, patient.getRespiration());

        values.put(PatientTable.Cols.TRIAGE_LVL, patient.getTriage().name());
        values.put(PatientTable.Cols.EXPOSURE_LVL, patient.getExposureLvl().name());
        values.put(PatientTable.Cols.ACTION, patient.getAction().name());

        values.put(PatientTable.Cols.REC_EXPOSURE_LVL, patient.getRecExposureLvl().name());
        values.put(PatientTable.Cols.REC_ACTION_LVL, patient.getRecAction().name());

        values.put(PatientTable.Cols.QUESTION_PRELIM,
                patient.getAnswer(AnswerEnum.PRELIM).getPrelimAns());

        values.put(PatientTable.Cols.QUESTION_ONE,
                patient.getAnswer(AnswerEnum.ONE).isAnswerOne() ? 1 : 0);
        values.put(PatientTable.Cols.QUESTION_ONE_A,
                patient.getAnswer(AnswerEnum.ONE).isAnswerTwo() ? 1 : 0);
        values.put(PatientTable.Cols.QUESTION_ONE_B,
                patient.getAnswer(AnswerEnum.ONE).isAnswerThree() ? 1 : 0);
        values.put(PatientTable.Cols.QUESTION_ONE_C,
                patient.getAnswer(AnswerEnum.ONE).isAnswerFour() ? 1 : 0);
        values.put(PatientTable.Cols.QUESTION_ONE_D,
                patient.getAnswer(AnswerEnum.ONE).isAnswerFive() ? 1 : 0);

        values.put(PatientTable.Cols.QUESTION_TWO,
                patient.getAnswer(AnswerEnum.TWO).isAnswerOne() ? 1 : 0);
        values.put(PatientTable.Cols.QUESTION_TWO_A,
                patient.getAnswer(AnswerEnum.TWO).isAnswerTwo() ? 1 : 0);
        values.put(PatientTable.Cols.QUESTION_TWO_B,
                patient.getAnswer(AnswerEnum.TWO).isAnswerThree() ? 1 : 0);
        values.put(PatientTable.Cols.QUESTION_TWO_C,
                patient.getAnswer(AnswerEnum.TWO).isAnswerFour() ? 1 : 0);

        values.put(PatientTable.Cols.QUESTION_THREE,
                patient.getAnswer(AnswerEnum.THREE).isAnswerOne() ? 1 : 0);

        values.put(PatientTable.Cols.QUESTION_FOUR,
                patient.getAnswer(AnswerEnum.FOUR).isAnswerOne() ? 1 : 0);

        values.put(PatientTable.Cols.QUESTION_FIVE,
                patient.getAnswer(AnswerEnum.FIVE).isAnswerOne() ? 1 : 0);

        values.put(PatientTable.Cols.COMPLETED_KIOSK, patient.getCompletedKiosk());

        values.put(PatientTable.Cols.NOTES_EXPOSURE, patient.getExposureNotes());
        values.put(PatientTable.Cols.NOTES_ACTION, patient.getActionNotes());

        return values;
    }

    /***********************************************************************************************
     * query the database for specified data
     * create sql queries and return the cursor with the data
     */
    private DatabaseCursorWrapper queryDatabase(String tableName, String whereClause, String[] whereArgs){
        Cursor cursor = database.query(
                tableName,
                null, // selects all columns
                whereClause,
                whereArgs,
                null, // groupBy
                null, // having
                null  // orderBy
        );
        return new DatabaseCursorWrapper(cursor);
    }

    /***********************************************************************************************
     * adds timestamp to sqlite for the purpose of updating.
     */
    public void addUpdatedTimeStamp(String timestamp){
        ContentValues values = getLastUpdated(timestamp);
        database.insert(UpdatedTable.NAME, null, values);
    }

    /***********************************************************************************************
     * searches sqlite for the last updated
     * returns timestamp
     */
    public String getTimeStamp(){
        DatabaseCursorWrapper cursor = queryDatabase(
                UpdatedTable.NAME, UpdatedTable.Cols.LAST_UPDATE, null);

        try {
            if (cursor.getCount() == 0) {
                return null;
            }
            cursor.moveToLast();
            return cursor.getTimeStamp();
        }finally {
            cursor.close();
        }
    }

    /***********************************************************************************************
     * gets the last timestamp sqlite was updated
     * called from addPatient
     * return values
     */
    public static ContentValues getLastUpdated(String timestamp){
        ContentValues values = new ContentValues();
        values.put(UpdatedTable.Cols.LAST_UPDATE, timestamp);
        return values;
    }

    /***********************************************************************************************
     *
     */
    public void storeData(String... data){
        ContentValues values = storeDate(data);
        database.insert(ToSendQueueTable.NAME, null, values);
    }

    /***********************************************************************************************
     *
     */
    public static ContentValues storeDate(String... data){
        ContentValues values = new ContentValues();
        values.put(ToSendQueueTable.Cols.METHOD, data[0]);
        values.put(ToSendQueueTable.Cols.PATIENT_ID, data[1]);
        values.put(ToSendQueueTable.Cols.DATA_ONE, data[2]);
        values.put(ToSendQueueTable.Cols.DATA_TWO, data[3]);
        values.put(ToSendQueueTable.Cols.DATA_THREE, data[4]);
        values.put(ToSendQueueTable.Cols.DATA_FOUR, data[5]);
        values.put(ToSendQueueTable.Cols.DATA_FIVE, data[6]);
        values.put(ToSendQueueTable.Cols.DATA_SIX, data[7]);
        values.put(ToSendQueueTable.Cols.DATA_SEVEN, data[8]);
        values.put(ToSendQueueTable.Cols.DATA_EIGHT, data[9]);
        return values;
    }

    /***********************************************************************************************
     * searches sqlite for data that wasn't sent to the server
     *
     * @return  a list of all the data not sent to the server
     */
    public List<String[]>  getData(){
        List<String[]> data = new ArrayList<>();

        DatabaseCursorWrapper cursor = queryDatabase(ToSendQueueTable.NAME, null, null);

        try {
            cursor.moveToFirst();
            while(!cursor.isAfterLast()){
                data.add(cursor.getData());
                cursor.moveToNext();
            }
        }finally {
            cursor.close();
        }

        return data;
    }

    /***********************************************************************************************
     * delete patient data from queue
     */
    public void deleteData(){
        database.delete(ToSendQueueTable.NAME, null, null);
    }

    /***********************************************************************************************
     * delete all data in sqlite db
     */
    public void clearDataBase(){
        database.delete(ToSendQueueTable.NAME, null, null);
        database.delete(PatientTable.NAME, null, null);
        database.delete(UpdatedTable.NAME, null, null);
        ContentValues values = PatientMngr.getLastUpdated(START_DATE);
        database.insert(UpdatedTable.NAME, null, values);

    }

    /***********************************************************************************************
     * checks for double entries in patient search results
     */
    public void checkForDoubles(List<Patient> patients){
        for(int i = 0; i < patients.size(); i++){
            for(int j = 0; j < patients.size(); j++){
                if(i != j){
                    if (patients.get(i).getPatientId().equals(patients.get(j).getPatientId()))
                        patients.remove(j);
                }
            }
        }
    }

    /***********************************************************************************************
     * returns size of current patients in database
     */
    public int size(){
        return getPatientList().size();
    }

    /***********************************************************************************************
     * gets the counts of patient triage, exposure, and action levels
     * sets static variables
     */
    public void getPatientCounts(){
        List<Patient> patientList = getPatientList();

        criticalPatients = nonCriticalPatients = 0;
        exposedPatients = potentialExposedPatients = nonExposedPatients = 0;
        exitPatient = monitorPatient = urgentPatient = 0;
        noCriticalAssigned = noExposureAssigned = noActionAssigned = 0;
        for(int i = 0; i < size(); i++){
            if(patientList.get(i).getTriage() == Triage.CRITICAL) criticalPatients++;
            if(patientList.get(i).getTriage() == Triage.NONCRITICAL) nonCriticalPatients++;
            if(patientList.get(i).getExposureLvl() == Exposure.EXPOSED) exposedPatients++;
            if(patientList.get(i).getExposureLvl() ==
                    Exposure.POTENTIALEXPOSURE) potentialExposedPatients++;
            if(patientList.get(i).getExposureLvl() == Exposure.NOTEXPOSED) nonExposedPatients++;
            if(patientList.get(i).getAction() == Action.URGENT) urgentPatient++;
            if(patientList.get(i).getAction() == Action.MONITOR) monitorPatient++;
            if(patientList.get(i).getAction() == Action.EXIT) exitPatient++;
            if(patientList.get(i).getTriage() == Triage.NONE) noCriticalAssigned++;
            if(patientList.get(i).getExposureLvl() == Exposure.NONE) noExposureAssigned++;
            if(patientList.get(i).getAction() == Action.NONE) noActionAssigned++;
            Log.i(TAG, "Value of i: " + i);
        }
    }

    /***********************************************************************************************
     * returns number of criticalPatients
     */
    public static int getCriticalPatients() {
        return criticalPatients;
    }

    /***********************************************************************************************
     * returns number of nonCriticalPatients
     */
    public static int getNonCriticalPatients() {
        return nonCriticalPatients;
    }

    /***********************************************************************************************
     * returns number of exposedPatients
     */
    public static int getExposedPatients() {
        return exposedPatients;
    }

    /***********************************************************************************************
     * returns number of potentialExposedPatients
     */
    public static int getPotentialExposedPatients() {
        return potentialExposedPatients;
    }

    /***********************************************************************************************
     * returns number of exposedPatients
     */
    public static int getNonExposedPatients() {
        return nonExposedPatients;
    }

    /***********************************************************************************************
     * returns number of exitPatients
     */
    public static int getExitPatient() {
        return exitPatient;
    }

    /***********************************************************************************************
     * returns number of monitorPatients
     */
    public static int getMonitorPatient() {
        return monitorPatient;
    }

    /***********************************************************************************************
     * returns number of urgentPatients
     */
    public static int getUrgentPatient() {
        return urgentPatient;
    }

    /***********************************************************************************************
     * returns number of noCriticalAssigned
     */
    public static int getNoCriticalAssigned() {
        return noCriticalAssigned;
    }

    /***********************************************************************************************
     * returns number of noExposureAssigned
     */
    public static int getNoExposureAssigned() {
        return noExposureAssigned;
    }

    /***********************************************************************************************
     * returns number of noActionAssigned
     */
    public static int getNoActionAssigned() {
        return noActionAssigned;
    }

    /***********************************************************************************************
     * checks if a patient has been entered in the database based on patientId
     * returns true if patient exists
     * returns false if patient doesn't exist
     */
    public boolean patientExist(String patientId){
        List<Patient> patientList = getPatientList();
        for(int i = 0; i < patientList.size(); i++){
            if(patientId.equals(patientList.get(i).getPatientId())){
                return true;
            }
        }
        return false;
    }

    /***********************************************************************************************
     * sorts patient list to place patient with patientId at the top
     * called from AddedLocationsFragment
     * return sorted list
     */
    public List<Patient> sortPatientList(String patientId){
        List<Patient> patientList = getPatientList();
        List<Patient> list = new ArrayList<>();
        for(int i = 0; i < size(); i++){
            if(patientId.equals(patientList.get(i).getPatientId())){
                list.add(patientList.get(i));
            }
        }
        for(int i = 0; i < size(); i++){
            list.add(patientList.get(i));
        }
        checkForDoubles(list);
        return list;
    }

}
