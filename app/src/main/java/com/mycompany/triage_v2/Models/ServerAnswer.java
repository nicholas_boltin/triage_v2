package com.mycompany.triage_v2.Models;

public class ServerAnswer extends Answer{

    public ServerAnswer(String prelimQues, boolean answerOne, boolean answerTwo, boolean answerThree,
                        boolean answerFour, boolean answerFive){
        super(prelimQues, answerOne, answerTwo, answerThree, answerFour, answerFive);

    }

}