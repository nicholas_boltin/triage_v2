package com.mycompany.triage_v2.SuperNurse;

import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.vision.text.Text;
import com.mycompany.triage_v2.Models.Triage;
import com.mycompany.triage_v2.R;

public class PrimaryTriageResult extends Fragment {

    private static final String PRE_TRIAGE_RESULT = "triage_result";

    private TextView selection, critical, nonCritical;

    public static PrimaryTriageResult newInstance(Triage triageLvl) {
        PrimaryTriageResult fragment = new PrimaryTriageResult();
        Bundle bundle = new Bundle();
        bundle.putSerializable(PRE_TRIAGE_RESULT, triageLvl);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.primary_triage_result, container, false);

        Triage x = (Triage) getArguments().getSerializable(PRE_TRIAGE_RESULT);

        selection = (TextView) view.findViewById(R.id.text_triage_result);
        critical = (TextView) view.findViewById(R.id.instructions_critical);

        switch (x){
            case CRITICAL:
                selection.setText(R.string.critical);
                selection.setBackgroundColor(Color.WHITE);
                //selection.setBackgroundColor(getResources().getColor(R.color.colorRed));
                critical.setText(R.string.critical_instructions);
                critical.setBackgroundColor(Color.WHITE);
                break;
            case NONCRITICAL:
                selection.setText(R.string.not_critical);
                selection.setBackgroundColor(Color.WHITE);
                //selection.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                critical.setText(R.string.non_critical_instructions);
                critical.setBackgroundColor(Color.WHITE);
                break;
            case NONE:
                break;
        }

        return view;
    }
}
