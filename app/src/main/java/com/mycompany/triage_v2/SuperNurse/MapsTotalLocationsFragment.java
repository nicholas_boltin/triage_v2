package com.mycompany.triage_v2.SuperNurse;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import com.mycompany.triage_v2.Helpers.GPSHelper;
import com.mycompany.triage_v2.Models.Patient;
import com.mycompany.triage_v2.Models.PatientMngr;
import com.mycompany.triage_v2.R;

public class MapsTotalLocationsFragment  extends Fragment implements
        OnMapReadyCallback{

    private static final String TAG = "trace";
    private static final String PATIENT_LAT = "patient_lat";
    private static final String PATIENT_LON = "patient_lon";
    private static final double LONGITUDE = -81.8375;

    private GoogleMap map;
    private MapView mapView;
    private MarkerOptions markerOptions;
    private LatLng location;
    private float zoom = 10;

    /***********************************************************************************************
     * required fragment constructor
     */
    public MapsTotalLocationsFragment(){}

    /***********************************************************************************************
     * creates new instance of fragment. Called from AddedLocationsFragment
     */
    public static MapsTotalLocationsFragment newInstance(LatLng latLng){
        MapsTotalLocationsFragment fragment = new MapsTotalLocationsFragment();
        Bundle bundle = new Bundle();
        bundle.putDouble(PATIENT_LAT, latLng.latitude);
        bundle.putDouble(PATIENT_LON, latLng.longitude);
        fragment.setArguments(bundle);
        return fragment;
    }

    /***********************************************************************************************
     *
     */
    @Override
    public void onCreate(Bundle SavedInstanceState){
        super.onCreate(SavedInstanceState);
    }

    /***********************************************************************************************
     * creates fragment view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_maps_total_locations, container, false);

        // create an instance of a MapView. set to xml layout and sync
        mapView = (MapView) view.findViewById(R.id.map_view_total);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        // check if LatLng is valid
        if(getArguments() != null){
            // set lat and lng values, default to 0 if none are there
            double x = getArguments().getDouble(PATIENT_LAT, 0);
            double y = getArguments().getDouble(PATIENT_LON, 0);

            // if default values are 0, set location to current location of tablet
            if(x == 0 && y == 0){
                GPSHelper gpsHelper = new GPSHelper(getContext());
                gpsHelper.getMyLocation();

                location = new LatLng(gpsHelper.getLatitude(), gpsHelper.getLongitude());
            // set location to valid coordinates
            }else{
                location = new LatLng(x, y);
            }
        // on null LatLng, set location to current location of tablet
        }else{
            GPSHelper gpsHelper = new GPSHelper(getContext());
            gpsHelper.getMyLocation();
            location = new LatLng(gpsHelper.getLatitude(), gpsHelper.getLongitude());
        }

        return view;
    }

    /***********************************************************************************************
     * gets map ready to be displayed
     */
    @Override
    public void onMapReady(GoogleMap googleMap){
        map = googleMap;
        addToMap();
        // moves camera to center on patient location w/ zoom factor
        // map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, zoom));

        /*******************************************************************************************
         * start exercise use - center map on Clearwater SC
         */
        LatLng exerciseLocation = new LatLng(33.4968035, -81.8920579);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(exerciseLocation, zoom));
        /*******************************************************************************************
         * finish exercise use
         */

    }

    /***********************************************************************************************
     * adds addresses from sqlite db to map
     */
    private void addToMap(){
        PatientMngr patientMngr = PatientMngr.get(getActivity());
        List<Patient> patientList = patientMngr.getPatientList();

        // create markers
        markerOptions = new MarkerOptions();

        for (int i = 0; i < patientList.size(); i++) {
            LatLng position = patientList.get(i).getLatLng();
            if(position != null){
                // set patient name as marker title
                markerOptions.title(patientList.get(i).fullName());
                // set marker to latlng
                markerOptions.position(position);

                // make current selected patient blue
                if(location.equals(position)){
                    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                // all other patients are red
                }else{
                    //markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                    //change marker option based on patient location
                    if(position.longitude > LONGITUDE){
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.red_dot));
                    } else {
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.green_dot));
                    }
                }
                map.addMarker(markerOptions);
            }
        }
    }

    /***********************************************************************************************
     *
     */
    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    /***********************************************************************************************
     *
     */
    @Override
    public void onPause(){
        super.onPause();
    }

    /***********************************************************************************************
     *
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    /***********************************************************************************************
     *
     */
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}
