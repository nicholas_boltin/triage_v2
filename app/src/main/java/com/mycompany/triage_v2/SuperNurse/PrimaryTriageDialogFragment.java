package com.mycompany.triage_v2.SuperNurse;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.mycompany.triage_v2.R;
import com.mycompany.triage_v2.Views.LogoutDialogFragment;

public class PrimaryTriageDialogFragment extends DialogFragment {

    private static final String TAG = "trace";

    private static final String METHOD = "primary_comments";

    private EditText notes;

    private PrimaryTriageDialogListener listener;

    public interface PrimaryTriageDialogListener{
        void onSelectedComments(String method, String notes);
    }

    public static PrimaryTriageDialogFragment newInstance(){ return new PrimaryTriageDialogFragment(); }

    public PrimaryTriageDialogFragment() {}

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_primary_triage_dialog, null);

        notes = (EditText) view.findViewById(R.id.edit_text_dialog_notes);

        return new AlertDialog.Builder(getActivity())
                .setView(view)
                .setTitle(R.string.dialog_primary_triage_title)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                        listener.onSelectedComments(METHOD, notes.getText().toString());
                    }
                }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which){

                    }
                }).create();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (PrimaryTriageDialogListener) context;
    }

}
