package com.mycompany.triage_v2.SuperNurse;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import java.math.BigInteger;
import java.util.List;

import com.google.android.gms.maps.model.LatLng;
import com.mycompany.triage_v2.Models.Patient;
import com.mycompany.triage_v2.Models.PatientMngr;
import com.mycompany.triage_v2.R;

public class AddedLocationsFragment extends Fragment {

    //private static final String TAG = "trace";
    private static final String BUNDLE_LAT = "initial_lat";
    private static final String BUNDLE_LONG = "initial_long";
    private static final String BUNDLE_PATIENT_ID = "patient_id";

    private RecyclerView recyclerView;
    private AddressAdapter adapter;
    private String patientID;

    public static AddedLocationsFragment newInstance(LatLng initialLatLng, String patientID){
        AddedLocationsFragment fragment =  new AddedLocationsFragment();
        Bundle bundle = new Bundle();
        bundle.putDouble(BUNDLE_LAT, initialLatLng.latitude);
        bundle.putDouble(BUNDLE_LONG, initialLatLng.longitude);
        bundle.putString(BUNDLE_PATIENT_ID, patientID);
        fragment.setArguments(bundle);
        return fragment;
    }

    public AddedLocationsFragment() {}

    @Override
    public void onCreate(Bundle SavedInstanceState){
        super.onCreate(SavedInstanceState);
        patientID = getArguments().getString(BUNDLE_PATIENT_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_added_locations, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        double lat = getArguments().getDouble(BUNDLE_LAT);
        double lon = getArguments().getDouble(BUNDLE_LONG);
        LatLng latLng = new LatLng(lat, lon);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        MapsTotalLocationsFragment fragment = MapsTotalLocationsFragment.newInstance(latLng);
        fragmentTransaction.add(R.id.fragment_map, fragment).commit();

        updateUI();
        return view;
    }

    private void updateUI(){
        PatientMngr patientMngr = PatientMngr.get(getActivity());
        List<Patient> patientList = patientMngr.sortPatientList(patientID);
        if(adapter == null){
            adapter = new AddressAdapter(patientList);
            recyclerView.setAdapter(adapter);
        }else{
            adapter.notifyDataSetChanged();
        }
    }



    public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.AddressHolder>{

        private List<Patient> patientList;
        public AddressAdapter(List<Patient> addressLocations){
            patientList = addressLocations;
        }


        @Override
        public AddressHolder onCreateViewHolder(ViewGroup parent, int viewType){
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.list_item_address, parent, false);
            return new AddressHolder(view);
        }

        @Override
        public void onBindViewHolder(AddressHolder addressholder, int position){
            Patient patient = patientList.get(position);
            addressholder.bindAddress(patient);
        }

        @Override
        public int getItemCount(){ return patientList.size(); }

        public class AddressHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

            private TextView address;
            private TextView patientText;
            private TextView patientId;
            private TextView triageLvl, exposure, action;
            private Patient patientObject;


            public  AddressHolder(View view){
                super(view);
                view.setOnClickListener(this);
                address = (TextView) view.findViewById(R.id.textview_address);
                patientText = (TextView) view.findViewById(R.id.textview_patient);
                patientId = (TextView) view.findViewById(R.id.textview_patient_id_number);
                triageLvl = (TextView) view.findViewById(R.id.triage_color_box);
                exposure = (TextView) view.findViewById(R.id.exposure_color_box);
                action = (TextView) view.findViewById(R.id.action_color_box);
            }

            public void bindAddress(Patient patient){
                patientObject = patient;
                address.setText(patient.getAddress());
                patientText.setText(patient.fullName());
                patientId.setText(patient.getPatientId());

                switch (patientObject.getTriage()){
                    case CRITICAL:
                        triageLvl.setBackgroundColor(Color.parseColor("#D32F2F")); //Red
                        triageLvl.setText(R.string.blank);
                        break;
                    case NONCRITICAL:
                        triageLvl.setBackgroundColor(Color.parseColor("#8BC34A")); //Green
                        triageLvl.setText(R.string.blank);
                        break;
                    case NONE:
                        triageLvl.setBackgroundColor(Color.WHITE);
                        triageLvl.setText(R.string.na);
                        break;
                }

                switch (patientObject.getExposureLvl()){
                    case EXPOSED:
                        exposure.setBackgroundColor(Color.parseColor("#F57C00")); //Orange
                        exposure.setText(R.string.blank);
                        break;
                    case NOTEXPOSED:
                        exposure.setBackgroundColor(Color.parseColor("#8BC34A")); //Green
                        exposure.setText(R.string.blank);
                        break;
                    case POTENTIALEXPOSURE:
                        exposure.setBackgroundColor(Color.parseColor("#FFEB3B")); //Yellow
                        exposure.setText(R.string.blank);
                        break;
                    case NONE:
                        exposure.setBackgroundColor(Color.WHITE);
                        exposure.setText(R.string.na);
                        break;
                }

                switch (patientObject.getAction()){
                    case URGENT:
                        action.setBackgroundColor(Color.parseColor("#D32F2F")); //Red
                        action.setText(R.string.blank);
                        break;
                    case MONITOR:
                        action.setBackgroundColor(Color.parseColor("#FFEB3B")); //Yellow
                        action.setText(R.string.blank);
                        break;
                    case EXIT:
                        action.setBackgroundColor(Color.GRAY);
                        action.setText(R.string.blank);
                        break;
                    case NONE:
                        action.setBackgroundColor(Color.WHITE);
                        action.setText(R.string.na);
                        break;
                }

            }

            @Override
            public void onClick(View view){
/*                if (selectedItems.get(getAdapterPosition(), false)) {
                    selectedItems.delete(selectedPos);
                    itemView.setSelected(false);
                }
                else {
                    selectedItems.put(getAdapterPosition(), true);
                    itemView.setSelected(true);
                }
*/
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                MapsTotalLocationsFragment fragment = MapsTotalLocationsFragment.newInstance(patientObject.getLatLng());
                fragmentTransaction.add(R.id.fragment_map, fragment).commit();
            }

        }

    }

}

