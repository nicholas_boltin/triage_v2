package com.mycompany.triage_v2.SuperNurse;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioAttributes;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.mycompany.triage_v2.R;

import static java.lang.Boolean.FALSE;

public class NotesDialogFragment extends DialogFragment{

    private static final String INTENT_DESTINATION = "destination";
    private static final String INTENT_NOTES = "notes";
    private static final String DIALOG_ANSWER = "dialog_answer";
    private static final int REQUEST_EXPOSURE = 100;
    private static final int REQUEST_ACTION = 200;
    private String notes;
    private EditText notesEditText;
    private int destination;




    public static NotesDialogFragment newInstance(int destination, String notes){
        NotesDialogFragment dialogFragment = new NotesDialogFragment();
        Bundle bundle = new Bundle();

        bundle.putInt(INTENT_DESTINATION, destination);

        bundle.putString(INTENT_NOTES, notes);

        dialogFragment.setArguments(bundle);
        return dialogFragment;


    }

    public NotesDialogFragment(){}

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_notes, null);

        notesEditText = (EditText) view.findViewById(R.id.edit_text_dialog_notes);
        notes = getArguments().getString(INTENT_NOTES);
        if(!notes.equals("")){
            notesEditText.setText(notes);
        }

        String title = "";
        String edit = "";
        switch (getArguments().getInt(INTENT_DESTINATION)){
            case R.id.spinner_action:
                title = getResources().getString(R.string.dialog_action_title);
                break;
            case R.id.spinner_exposure:
                title = getResources().getString(R.string.dialog_exposure_title);
                break;
/*            case R.id.button_view_action_text:
                title = getResources().getString(R.string.dialog_action_notes);
                edit = getResources().getString(R.string.dialog_edit);
                notesEditText.setEnabled(false);
                destination = R.id.spinner_action;
                break;
            case R.id.button_view_exposure_text:
                title = getResources().getString(R.string.dialog_exposure_notes);
                edit = getResources().getString(R.string.dialog_edit);
                notesEditText.setEnabled(false);
                destination = R.id.spinner_exposure;
                break;
                */
        }

        return new AlertDialog.Builder(getActivity())
                .setView(view)
                .setTitle(title)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                        notes = notesEditText.getText().toString();
                        sendResult(Activity.RESULT_OK, notes);
                    }
                })
                /*.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                        dismiss();

                    }

                })*/


 /*               .setNeutralButton(edit, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                            FragmentManager fragmentManager = getFragmentManager();
                            NotesDialogFragment dialogFragment;
                            dialogFragment = NotesDialogFragment.newInstance(destination, notes);
                            int test = getArguments().getInt(INTENT_DESTINATION) ==
                                    R.id.button_view_action_text ? REQUEST_ACTION : REQUEST_EXPOSURE;
                            dialogFragment.setTargetFragment(getTargetFragment(), test);
                            dialogFragment.show(fragmentManager, DIALOG_ANSWER);
                    }
                })  */
                .create();

    }






    private void sendResult(int resultCode, String notes){
        if(getTargetFragment() == null){
            return;
        }
        Intent intent = new Intent();
        intent.putExtra(INTENT_NOTES, notes);

        getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, intent);

    }

}
