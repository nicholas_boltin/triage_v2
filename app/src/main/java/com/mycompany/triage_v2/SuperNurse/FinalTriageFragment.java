package com.mycompany.triage_v2.SuperNurse;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.mycompany.triage_v2.Helpers.Magic;
import com.mycompany.triage_v2.Models.Action;
import com.mycompany.triage_v2.Models.Exposure;
import com.mycompany.triage_v2.Models.Triage;
import com.mycompany.triage_v2.R;

import static java.lang.Boolean.FALSE;

public class FinalTriageFragment extends Fragment {

    //new intent variables
    private static final String FINAL_TRIAGE_RESULT = "triage_result";
    private static final String EXPOSURE_RESULT = "exposure_result";
    private static final String ACTION_RESULT = "action_result";
    private static final String REC_EXPOSURE = "rec_exposure";
    private static final String REC_ACTION = "rec_action";
    private static final String VITALS_ARRAY = "vitals_array";
    private static final String PATIENT_ID = "patientID";
    private static final String BUNDLE_LAT = "initial_lat";
    private static final String BUNDLE_LONG = "initial_long";
    private static final String BUNDLE_ACTION_NOTES = "action_notes";
    private static final String BUNDLE_EXPOSURE_NOTES = "exposure_notes";

    //static variables for pushing to server
    private static final String ACTION_NOTES = "action_notes";
    private static final String EXPOSURE_NOTES = "exposure_notes";

    private static final String FINAL_EXIT = "final_exit";

    //dialog variables
    private static final int REQUEST_EXPOSURE = 100;
    private static final int REQUEST_ACTION = 200;
    private static final String DIALOG_ANSWER = "dialog_answer";
    private static final String INTENT_NOTES = "notes";

    private static final String TAG = "trace";

    // String to be displayed for recommendations
    private static final String ELIPSE = "...";
    private static final String NOT_EXPOSED = "NO EXPOSURE";
    private static final String POTENTIAL_EXPOSURE = "POTENTIAL EXPOSURE";
    private static final String EXPOSED = "EXPOSURE";
    private static final String EXIT = "RETURN TO HOSPITAL SYSTEM";
    private static final String MONITOR = "MONITOR";
    private static final String URGENT = "URGENT";

    private String patient_id;

    private Spinner exposureSpinner;
    private Spinner actionSpinner;
    private ImageButton exposureImageButton;
    private ImageButton actionImageButton;
    private Button submitButton;

    private TextView recExposure;
    private TextView recAction;

    TextView oxygen, heartRate, respiration;
    String actionNotes, exposureNotes;
    Exposure exposure;
    Action action;

    /***********************************************************************************************
     * Interface listener sends data to activity for data collection
     */
    finalTriageListener listener;

    public interface finalTriageListener{
        void onSelectedExposure(Exposure exposure);
        void onSelectedAction(Action action);
        void onSelectedNotesExposure(String notes);
        void onSelectedNotesAction(String notes);

        void onSelectedTriagelvl(Triage primary, Exposure exposure, Action action);
        void onSelectedComments(String method, String notes);
        void finished();
    }

    /***********************************************************************************************
     * Required empty constructor
     */
    public FinalTriageFragment() {}

    /***********************************************************************************************
     * Returns a new instance of FinalTriage fragment. This method is called from
     * SuperNursePagerActivity
     *
     * @param vitals        an array of patient vitals
     * @param patient_id    the current patient's id
     * @param triageLvl     the patient's current triage level
     * @param exposureLvl   the patient's current exposure level
     * @param actionLvl     the patient's current action level
     * @param recExposure   the recommended exposure level of the patient
     * @param recAction     the recommended action level of the patient
     * @param initialLatLng the coordinates of the exposure area
     * @param actionNotes   the action notes associated with the patient
     * @param exposureNotes the exposure notes associated with the patient
     * @return              a new instance of a fragment
     */
    public static FinalTriageFragment newInstance(int[] vitals, String patient_id,
                                                  Triage triageLvl, Exposure exposureLvl, Action actionLvl,
                                                  Exposure recExposure, Action recAction,
                                                  LatLng initialLatLng,
                                                  String actionNotes, String exposureNotes) {
        FinalTriageFragment fragment = new FinalTriageFragment();
        Bundle bundle = new Bundle();

        bundle.putIntArray(VITALS_ARRAY, vitals);

        bundle.putString(PATIENT_ID,patient_id);

        bundle.putSerializable(FINAL_TRIAGE_RESULT, triageLvl);
        bundle.putSerializable(EXPOSURE_RESULT, exposureLvl);
        bundle.putSerializable(ACTION_RESULT, actionLvl);

        bundle.putSerializable(REC_EXPOSURE, recExposure);
        bundle.putSerializable(REC_ACTION, recAction);

        bundle.putDouble(BUNDLE_LAT, initialLatLng.latitude);
        bundle.putDouble(BUNDLE_LONG, initialLatLng.longitude);

        bundle.putString(BUNDLE_ACTION_NOTES, actionNotes);
        bundle.putString(BUNDLE_EXPOSURE_NOTES, exposureNotes);

        fragment.setArguments(bundle);
        return fragment;
    }

    /***********************************************************************************************
     * Creates fragment layout
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_final_triage, container, false);

        patient_id = getArguments().getString(PATIENT_ID);

        //get patient notes if there are any
        actionNotes = getArguments().getString(BUNDLE_ACTION_NOTES, "");
        exposureNotes = getArguments().getString(BUNDLE_EXPOSURE_NOTES, "");

        //get triage levels
        final Triage triageLvl = (Triage) getArguments().getSerializable(FINAL_TRIAGE_RESULT);
        final Exposure exposureLvl = (Exposure) getArguments().getSerializable(EXPOSURE_RESULT);
        final Action actionLvl = (Action) getArguments().getSerializable(ACTION_RESULT);

        exposure = exposureLvl;
        action = actionLvl;

        final Exposure recExposureLvl = (Exposure) getArguments().getSerializable(REC_EXPOSURE);
        final Action recActionLvl = (Action) getArguments().getSerializable(REC_ACTION);

        //check to see if patient action and exposure levels were set before
        final boolean checkAction = toShowDialogAction(actionLvl);
        final boolean checkExposure = toShowDialogExposure(exposureLvl);

        //get vitals
        int[] vitals = getArguments().getIntArray(VITALS_ARRAY);

        //set vitals
        oxygen = (TextView) view.findViewById(R.id.textview_o2);
        oxygen.setText(Integer.toString(vitals[0]));
        heartRate = (TextView) view.findViewById(R.id.textview_heart);
        heartRate.setText(Integer.toString(vitals[1]));
        respiration = (TextView) view.findViewById(R.id.textview_respiration);
        respiration.setText(Integer.toString(vitals[2]));

        recExposure = (TextView) view.findViewById(R.id.rec_exposure);
        switch (recExposureLvl){
            case NONE:
                break;
            case NOTEXPOSED:
                recExposure.setBackgroundColor(Color.parseColor("#8BC34A"));
                recExposure.setText(NOT_EXPOSED);
                break;
            case POTENTIALEXPOSURE:
                recExposure.setBackgroundColor(Color.parseColor("#FFEB3B"));
                recExposure.setText(POTENTIAL_EXPOSURE);
                break;
            case EXPOSED:
                recExposure.setBackgroundColor(Color.parseColor("#F57C00"));
                recExposure.setText(EXPOSED);
                break;

        }

        recAction = (TextView) view.findViewById(R.id.rec_action);
        switch (recActionLvl){
            case NONE:
                break;
            case URGENT:
                recAction.setBackgroundColor(Color.parseColor("#D32F2F"));
                recAction.setText(URGENT);
                break;
            case MONITOR:
                recAction.setBackgroundColor(Color.parseColor("#FFEB3B"));
                recAction.setText(MONITOR);
                break;
            case EXIT:
                recAction.setBackgroundColor(Color.GRAY);
                recAction.setText(EXIT);
                break;
        }


        //set spinner
        exposureSpinner = (Spinner) view.findViewById(R.id.spinner_exposure);
        ArrayAdapter<CharSequence> adapterExposure = ArrayAdapter.createFromResource(this.getContext(),
                R.array.array_exposure, android.R.layout.simple_spinner_item);
        adapterExposure.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        exposureSpinner.setAdapter(adapterExposure);

        exposureSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (parent.getItemAtPosition(position).toString()){
                    case ELIPSE:
                        break;
                    case NOT_EXPOSED:
                        exposure = Exposure.NOTEXPOSED;
                        exposureSpinner.setBackgroundColor(Color.parseColor("#8BC34A")); //Green
                        // if recommended doesn't match given, call for notes
                        if(!recExposureLvl.equals(Exposure.NOTEXPOSED)) getNotes(R.id.spinner_exposure);
                        break;
                    case POTENTIAL_EXPOSURE:
                        exposure = Exposure.POTENTIALEXPOSURE;
                        exposureSpinner.setBackgroundColor(Color.parseColor("#FFEB3B")); //Yellow
                        // if recommended doesn't match given, call for notes
                        if(!recExposureLvl.equals(Exposure.POTENTIALEXPOSURE)) getNotes(R.id.spinner_exposure);
                        break;
                    case EXPOSED:
                        exposure = Exposure.EXPOSED;
                        exposureSpinner.setBackgroundColor(Color.parseColor("#F57C00")); //Orange
                        // if recommended doesn't match given, call for notes
                        if(!recExposureLvl.equals(Exposure.EXPOSED)) getNotes(R.id.spinner_exposure);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        //set spinner
        actionSpinner = (Spinner) view.findViewById(R.id.spinner_action);
        ArrayAdapter<CharSequence> adapterAction = ArrayAdapter.createFromResource(this.getContext(),
                R.array.array_action, android.R.layout.simple_spinner_item);
        adapterAction.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        actionSpinner.setAdapter(adapterAction);

        actionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (parent.getItemAtPosition(position).toString()){
                    case ELIPSE:
                        break;
                    case EXIT:
                        action = Action.EXIT;
                        actionSpinner.setBackgroundColor(Color.GRAY);
                        // if recommended doesn't match given, call for notes
                        if(!recActionLvl.equals(Action.EXIT)) getNotes(R.id.spinner_action);
                        break;
                    case MONITOR:
                        action = Action.MONITOR;
                        actionSpinner.setBackgroundColor(Color.parseColor("#FFEB3B")); //Yellow
                        // if recommended doesn't match given, call for notes
                        if(!recActionLvl.equals(Action.MONITOR)) getNotes(R.id.spinner_action);
                        break;
                    case URGENT:
                        action = Action.URGENT;
                        actionSpinner.setBackgroundColor(Color.parseColor("#D32F2F")); //Red
                        // if recommended doesn't match given, call for notes
                        if(!recActionLvl.equals(Action.URGENT)) getNotes(R.id.spinner_action);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        submitButton = (Button) view.findViewById(R.id.button_submit);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onSelectedAction(action);
                listener.onSelectedExposure(exposure);
                if(Magic.isConnected(getContext())){
                    Magic.stampThis(getContext(), FINAL_EXIT, patient_id);
                }
                listener.onSelectedTriagelvl(triageLvl, exposure, action);
                listener.finished();
            }
        });
/*
        //notes buttons only appear if notes are present to be displayed
        exposureImageButton =(ImageButton) view.findViewById(R.id.button_view_exposure_text);
        exposureImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getNotes(R.id.button_view_exposure_text);
            }
        });

        if(exposureNotes.equals("")) exposureImageButton.setVisibility(View.INVISIBLE);

        actionImageButton = (ImageButton) view.findViewById(R.id.button_view_action_text);
        actionImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getNotes(R.id.button_view_action_text);
            }
        });

        if(actionNotes.equals("")) actionImageButton.setVisibility(View.INVISIBLE);
*/
        //get latlng
        double lat = getArguments().getDouble(BUNDLE_LAT);
        double lon = getArguments().getDouble(BUNDLE_LONG);
        LatLng latLng = new LatLng(lat, lon);

        //map fragment with current patient at the center w/ highlighted marker
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        MapsTotalLocationsFragment fragment = MapsTotalLocationsFragment.newInstance(latLng);
        fragmentTransaction.add(R.id.final_triage_map, fragment).commit();


        return view;
    }

    /***********************************************************************************************
     * check if patient action level has been set before
     */
    private boolean toShowDialogAction(Action action){
        return action.equals(Action.NONE);
    }

    /***********************************************************************************************
     * check if patient exposure level has been set before
     */
    private boolean toShowDialogExposure(Exposure exposure){
        return exposure.equals(Exposure.NONE);
    }

    /***********************************************************************************************
     * launces dialog fragment to collect notes. if notes are present they are displayed
     */
    private void getNotes(int destination){
        FragmentManager fragmentManager = getFragmentManager();
        NotesDialogFragment dialogFragment;

        switch (destination){
            case R.id.spinner_exposure:
                dialogFragment = NotesDialogFragment.newInstance(destination, exposureNotes);
                dialogFragment.setTargetFragment(FinalTriageFragment.this, REQUEST_EXPOSURE);
                dialogFragment.show(fragmentManager, DIALOG_ANSWER);
                break;
            case R.id.spinner_action:
                dialogFragment = NotesDialogFragment.newInstance(destination, actionNotes);
                dialogFragment.setTargetFragment(FinalTriageFragment.this, REQUEST_ACTION);
                dialogFragment.show(fragmentManager, DIALOG_ANSWER);
                break;
       /*     case R.id.button_view_exposure_text:
                dialogFragment = NotesDialogFragment.newInstance(destination, exposureNotes);
                dialogFragment.setTargetFragment(FinalTriageFragment.this, REQUEST_EXPOSURE);
                dialogFragment.show(fragmentManager, DIALOG_ANSWER);
                break;
            case R.id.button_view_action_text:
                dialogFragment = NotesDialogFragment.newInstance(destination, actionNotes);
                dialogFragment.setTargetFragment(FinalTriageFragment.this, REQUEST_ACTION);
                dialogFragment.show(fragmentManager, DIALOG_ANSWER);
                break; */
        }
    }

    /***********************************************************************************************
     * gets comments from dialog fragment and passes to activity
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode != Activity.RESULT_OK){
            return;
        }
        String notes = data.getStringExtra(INTENT_NOTES);

        switch (requestCode){
            case REQUEST_EXPOSURE:
                listener.onSelectedComments(EXPOSURE_NOTES, notes);
                break;
            case REQUEST_ACTION:
                listener.onSelectedComments(ACTION_NOTES, notes);
                break;
        }
    }

    /***********************************************************************************************
     * attach listener
     */
    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        listener = (finalTriageListener) context;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
