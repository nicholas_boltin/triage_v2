package com.mycompany.triage_v2.SuperNurse;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.mycompany.triage_v2.Kiosk.EditAnswerThreeDialogFragment;
import com.mycompany.triage_v2.Models.Action;
import com.mycompany.triage_v2.Models.Exposure;
import com.mycompany.triage_v2.Models.Triage;
import com.mycompany.triage_v2.R;

public class PrimaryTriage extends Fragment {

    private static final String PRE_TRIAGE_RESULT = "triage_result";
    private static final String EXPOSURE_RESULT = "exposure_result";
    private static final String ACTION_RESULT = "action_result";
    private static final String DIALOG_ANSWER = "dialog_answer";

    private Button red, redButton;
    private Button yellow, yellowButton;
    private Button submit;
    private Triage triage;

    /***********************************************************************************************
     * interface with methods to be used
     */
    primaryTriageListener listener;

    public  interface primaryTriageListener{
        void onSelectedTriagelvl(Triage primary, Exposure exposure, Action action);
        void onSelectedPrimaryTriage(Triage value);
        void finished();
    }

    /***********************************************************************************************
     * creates new instance of fragment
     * triage level used to display current level of patient if one has been assigned
     */
    public static PrimaryTriage newInstance(Triage triageLvl, Exposure exposure, Action action) {
        PrimaryTriage fragment = new PrimaryTriage();
        Bundle bundle = new Bundle();
        bundle.putSerializable(PRE_TRIAGE_RESULT, triageLvl);
        bundle.putSerializable(EXPOSURE_RESULT, exposure);
        bundle.putSerializable(ACTION_RESULT, action);
        fragment.setArguments(bundle);
        return fragment;
    }

    /***********************************************************************************************
     * requried empty constructor
     */
    public PrimaryTriage(){}

    /***********************************************************************************************
     * creates fragment view
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.primary_triage, container, false);

        // get patient current triage level
        Triage triageLvl = (Triage) getArguments().getSerializable(PRE_TRIAGE_RESULT);
        final Exposure exposure = (Exposure) getArguments().getSerializable(EXPOSURE_RESULT);
        final Action action = (Action) getArguments().getSerializable(ACTION_RESULT);

        red = (Button) view.findViewById(R.id.button_red);
        redButton = (Button) view.findViewById(R.id.button_red);

        // focus on button so only one of them displays color at a time
        red.setFocusableInTouchMode(true);
        red.setFocusable(false);

        redButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit.setEnabled(true);
                triage = Triage.CRITICAL;
                displayTriage(Triage.CRITICAL);
                redButton.setFocusableInTouchMode(true);
                redButton.setFocusable(true);
                yellowButton.setFocusableInTouchMode(false);
                yellowButton.setFocusable(false);
            }
        });

        yellow = (Button) view.findViewById(R.id.button_yellow);
        yellowButton = (Button) view.findViewById(R.id.button_yellow);

        // focus on button so only one of them displays color at a time
        yellow.setFocusableInTouchMode(true);
        yellow.setFocusable(false);

        yellowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit.setEnabled(true);
                triage = Triage.NONCRITICAL;
                displayTriage(Triage.NONCRITICAL);
                yellowButton.setFocusableInTouchMode(true);
                yellowButton.setFocusable(true);
                redButton.setFocusableInTouchMode(false);
                redButton.setFocusable(false);
            }
        });

        submit = (Button) view.findViewById(R.id.button_submit);
        submit.setEnabled(false);
        //submit.setFocusableInTouchMode(true);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(triage.equals(Triage.CRITICAL)) getOneMorePieceOfData();
                listener.onSelectedPrimaryTriage(triage);
                listener.onSelectedTriagelvl(triage, exposure, action);
            }
        });

        displayTriage(triageLvl);

        return  view;
    }

    /***********************************************************************************************
     * displays version of PrimarayTriageFragment with the corresponding instructions based
     * on the patient's selected triage level
     */
    private void displayTriage(Triage value){
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        PrimaryTriageResult fragment = PrimaryTriageResult.newInstance(value);
        fragmentTransaction.replace(R.id.primary_triage_fragment_container, fragment);
        fragmentTransaction.commit();
    }

    /***********************************************************************************************
     *
     */
    private void getOneMorePieceOfData(){
        FragmentManager fragmentManager = getFragmentManager();
        PrimaryTriageDialogFragment dialogFragment = PrimaryTriageDialogFragment.newInstance();
        dialogFragment.show(fragmentManager, DIALOG_ANSWER);
    }

    /***********************************************************************************************
     * attach listener
     */
    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        listener = (primaryTriageListener) context;
    }
}
