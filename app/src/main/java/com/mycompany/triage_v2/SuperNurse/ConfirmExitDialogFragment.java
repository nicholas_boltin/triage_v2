package com.mycompany.triage_v2.SuperNurse;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.mycompany.triage_v2.R;
import com.mycompany.triage_v2.Views.LogoutDialogFragment;

public class ConfirmExitDialogFragment extends DialogFragment {

    private static final String TAG = "trace";

    private static String VITALS_SUBMITTED = "vitals_submitted";
    private static String TRIAGE_SUBMITTED = "triage_submitted";


    private TextView confirm;

    private ConfirmExitDialogListener listener;

    public interface ConfirmExitDialogListener{
        void exitNurseMode();
    }

    public static ConfirmExitDialogFragment newInstance(int vitalsSubmitted, int triageSubmitted){
        ConfirmExitDialogFragment fragment = new ConfirmExitDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(VITALS_SUBMITTED, vitalsSubmitted);
        bundle.putInt(TRIAGE_SUBMITTED, triageSubmitted);

        fragment.setArguments(bundle);

        return fragment;
    }

    public ConfirmExitDialogFragment() {}

    @Nullable
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_super_nurse_pager, null);

        // get integers to check if data submitted
        int vitals = getArguments().getInt(VITALS_SUBMITTED);
        int triage = getArguments().getInt(TRIAGE_SUBMITTED);

        confirm = (TextView) view.findViewById(R.id.text_view_exit_confirm);

        // load the appropriate msg for exit dialog
        if(vitals > 0 && triage > 0) confirm.setText(R.string.submissions_complete);
        if(vitals > 0 && triage < 1) confirm.setText(R.string.no_triage_lvls_submitted);
        if(vitals < 1 && triage > 0) confirm.setText(R.string.no_vitals_submitted);
        if(vitals < 1 && triage < 1) confirm.setText(R.string.nothing_submitted);

        return new AlertDialog.Builder(getActivity())
                .setView(view)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                        listener.exitNurseMode();
                    }
                }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which){

                    }
                }).create();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (ConfirmExitDialogListener) context;
    }

}
