package com.mycompany.triage_v2.SuperNurse;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.mycompany.triage_v2.Helpers.Magic;
import com.mycompany.triage_v2.R;

public class VitalsFragment extends Fragment {

    private static final String VITALS_ARRAY = "vitals_array";

    private static final String TAG = "trace";

    private Button submit;
    private NumberPicker oxygenNumberPicker, heartNumberPicker, respirationNumberPicker;
    private int oxygen, heart, respiration;

    private int heartMin = 0; //30
    private int heartMax = 200; // 200
    private int respirationMin = 0;
    private int respirationMax = 50;
    private int oxygenMin = 0;
    private int oxygenMax = 100;
    // private int setValue = 20;

    public vitalsFragmentListener listener;

    public interface vitalsFragmentListener{
        void onSelectedVitals(int heartRate, int oxygen, int respiration, String timeStamp);

    }

    public VitalsFragment() {}

    public static VitalsFragment newInstance(int vitals[]) {
        VitalsFragment fragment = new VitalsFragment();
        Bundle bundle = new Bundle();
        bundle.putIntArray(VITALS_ARRAY, vitals);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vitals, container, false);

        final int vitals[] = getArguments().getIntArray(VITALS_ARRAY);
        oxygen = vitals[0];
        heart = vitals[1];
        respiration = vitals[2];
        //if(respiration == 0) respiration = setValue;

        oxygenNumberPicker = (NumberPicker) view.findViewById(R.id.number_picker_o2);
        oxygenNumberPicker.setMinValue(oxygenMin);
        oxygenNumberPicker.setMaxValue(oxygenMax);
        Log.i(TAG, "Value of o2: " + oxygen);
        oxygenNumberPicker.setValue(oxygen);
        oxygenNumberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                oxygen = newVal;
            }
        });

        heartNumberPicker = (NumberPicker) view.findViewById(R.id.number_picker_hr);
        heartNumberPicker.setMinValue(heartMin);
        heartNumberPicker.setMaxValue(heartMax);
        Log.i(TAG, "Value of pulse: " + oxygen);
        heartNumberPicker.setValue(heart);
        heartNumberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                heart = newVal;
            }
        });

        respirationNumberPicker = (NumberPicker) view.findViewById(R.id.number_picker_respiration);
        respirationNumberPicker.setMinValue(respirationMin);
        respirationNumberPicker.setMaxValue(respirationMax);
        Log.i(TAG, "Value of resp: " + respiration);
        respirationNumberPicker.setValue(respiration);
        respirationNumberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                respiration = newVal;
            }
        });


        submit = (Button) view.findViewById(R.id.button_nurse_vitals);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onSelectedVitals(heart, oxygen, respiration, Magic.stampThatFool());

            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        listener = (vitalsFragmentListener) context;
    }

}
