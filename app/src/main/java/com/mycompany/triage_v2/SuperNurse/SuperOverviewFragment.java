package com.mycompany.triage_v2.SuperNurse;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mycompany.triage_v2.Helpers.Magic;
import com.mycompany.triage_v2.Kiosk.EditAnswerFiveDialogFragment;
import com.mycompany.triage_v2.Kiosk.EditAnswerFourDialogFragment;
import com.mycompany.triage_v2.Kiosk.EditAnswerOneDialogFragment;
import com.mycompany.triage_v2.Kiosk.EditAnswerThreeDialogFragment;
import com.mycompany.triage_v2.Kiosk.EditAnswerTwoDialogFragment;
import com.mycompany.triage_v2.Models.Answer;
import com.mycompany.triage_v2.R;

public class SuperOverviewFragment extends Fragment {

    private static final String TAG = "trace";

    //final strings for editing answers
    private static final int REQUEST_ANSWER_ONE = 100;
    private static final int REQUEST_ANSWER_TWO = 200;
    private static final int REQUEST_ANSWER_THREE = 300;
    private static final int REQUEST_ANSWER_FOUR = 400;
    private static final int REQUEST_ANSWER_FIVE = 500;
    private static final String DIALOG_ANSWER = "dialog_answer";

    private static final int ARRAY_SIZE = 5;

    private static final String INTENT_YESNO = "yes_no";
    private static final String INTENT_CHECK_ONE = "check_one";
    private static final String INTENT_CHECK_TWO = "check_two";
    private static final String INTENT_CHECK_THREE = "check_three";
    private static final String INTENT_CHECK_FOUR = "check_four";

    //final strings for newIntent start
    private static final String ANSWER_HEADER = "header_answer";

    private static final String ANSWER_ONE_BOOLEAN_ARRAY = "one_boolean_array";
    private static final String ANSWER_TWO_BOOLEAN_ARRAY = "two_boolean_array";
    private static final String ANSWER_THREE_BOOLEAN_ARRAY = "three_boolean_array";
    private static final String ANSWER_FOUR_BOOLEAN_ARRAY = "four_boolean_array";
    private static final String ANSWER_FIVE_BOOLEAN_ARRAY = "five_boolean_array";
    private static final String ANSWER_PRELIM = "preliminary_answer";
    //final strings for newIntent end

    //server controls
    private static final String QUESTION_ONE = "1";
    private static final String QUESTION_TWO = "2";
    private static final String QUESTION_THREE = "3";
    private static final String QUESTION_FOUR = "4";
    private static final String QUESTION_FIVE = "5";
    private static final String QUESTION_SIX = "6";

    //widgets
    private TextView header;
    private TextView preliminaryAnswer;
    private ImageButton editOne, editTwo, editThree, editFour, editFive;

    //answer one widgets
    private CheckBox checkBoxOneYes, checkBoxOneNo;
    private TextView breathing, wheezing, coughing, choking;

    //answer two widgets
    private CheckBox checkBoxTwoYes, checkBoxTwoNo;
    private TextView burning ,tightness,pain;

    //answer three, four, five widgets
    private CheckBox checkBoxThreeYes, checkBoxThreeNo,
            checkBoxFourYes, checkBoxFourNo,
            checkBoxFiveYes, checkBoxFiveNo;


    SuperOverviewListener listener;

    /***********************************************************************************************
     * interface listener sends corrected Answers to KioskTabPagerActivity for update
     */
    public interface SuperOverviewListener{
        void onSelectedQuestionOne(boolean radio, boolean checkOne, boolean checkTwo,
                                   boolean checkThree, boolean checkFour);
        void onSelectedQuestionTwo(boolean radio, boolean checkOne, boolean checkTwo,
                                   boolean checkThree, boolean checkFour);
        void onSelectedQuestionThree(boolean radio);
        void onSelectedQuestionFour(boolean radio);
        void onSelectedQuestionFive(boolean radio);

        void onSelectedAnswer(String question, boolean radio, boolean checkOne, boolean checkTwo,
                              boolean checkThree, boolean checkFour,
                              String preliminary, String timeStamp);
    }

    /***********************************************************************************************
     *required empty constructor
     */
    public SuperOverviewFragment() {}

    /***********************************************************************************************
     * creates new instance of KioskOverView fragment. Called from SuperNursePagerActivity
     * parameters:
     *  String header - keeps track of page counts
     *  String title - displays patient info
     *  Answer one, two, three,f four, five - answer objects to be displayed
     */
    public static SuperOverviewFragment newInstance(String header, String prelim, boolean[] one,
                                                    boolean[] two, boolean[] three, boolean[] four,
                                                    boolean[] five) {

        SuperOverviewFragment fragment = new SuperOverviewFragment();
        Bundle bundle = new Bundle();

        bundle.putString(ANSWER_HEADER, header);

        bundle.putBooleanArray(ANSWER_ONE_BOOLEAN_ARRAY, one);
        bundle.putBooleanArray(ANSWER_TWO_BOOLEAN_ARRAY, two);
        bundle.putBooleanArray(ANSWER_THREE_BOOLEAN_ARRAY, three);
        bundle.putBooleanArray(ANSWER_FOUR_BOOLEAN_ARRAY, four);
        bundle.putBooleanArray(ANSWER_FIVE_BOOLEAN_ARRAY, five);


        bundle.putString(ANSWER_PRELIM, prelim);

        fragment.setArguments(bundle);
        return fragment;
    }

    /***********************************************************************************************
     *creates fragment layout
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){

        View view = inflater.inflate(R.layout.fragment_super_overview, container, false);

        header = (TextView) view.findViewById(R.id.textview_overview_header);
        header.setText(getArguments().getString(ANSWER_HEADER));

        preliminaryAnswer = (TextView) view.findViewById(R.id.textview_preliminary_answer);
        preliminaryAnswer.setText(getArguments().getString(ANSWER_PRELIM));

        //Answer one setup
        boolean arrayOne[] = getArguments().getBooleanArray(ANSWER_ONE_BOOLEAN_ARRAY);

        checkBoxOneYes = (CheckBox) view.findViewById(R.id.checkbox_yes_overview_q1);
        checkBoxOneYes.setEnabled(false);

        checkBoxOneNo = (CheckBox) view.findViewById(R.id.checkbox_no_overview_q1);
        checkBoxOneNo.setEnabled(false);

        breathing = (TextView) view.findViewById(R.id.textview_shortnessBreath);
        wheezing = (TextView) view.findViewById(R.id.textview_wheezing);
        coughing = (TextView) view.findViewById(R.id.textview_coughing);
        choking = (TextView) view.findViewById(R.id.textview_choking);

        //set xml based on patient answers
        editAnswerOneXML(arrayOne);

        //answer two setup
        boolean arrayTwo[] = getArguments().getBooleanArray(ANSWER_TWO_BOOLEAN_ARRAY);

        checkBoxTwoYes = (CheckBox) view.findViewById(R.id.checkbox_yes_overview_q2);
        checkBoxTwoYes.setEnabled(false);

        checkBoxTwoNo = (CheckBox) view.findViewById(R.id.checkbox_no_overview_q2);
        checkBoxTwoNo.setEnabled(false);

        burning = (TextView) view.findViewById(R.id.textview_burning);
        tightness = (TextView) view.findViewById(R.id.textview_something_one);
        pain = (TextView) view.findViewById(R.id.textview_something_two);

        //set xml based on patient answers
        editAnswerTwoXML(arrayTwo);

        //answer three, four, five setup
        boolean arrayThree[] = getArguments().getBooleanArray(ANSWER_THREE_BOOLEAN_ARRAY);
        boolean arrayFour[] = getArguments().getBooleanArray(ANSWER_FOUR_BOOLEAN_ARRAY);
        boolean arrayFive[] = getArguments().getBooleanArray(ANSWER_FIVE_BOOLEAN_ARRAY);

        checkBoxThreeYes = (CheckBox) view.findViewById(R.id.checkbox_yes_overview_q3);
        checkBoxThreeYes.setEnabled(false);
        checkBoxThreeNo = (CheckBox) view.findViewById(R.id.checkbox_no_overview_q3);
        checkBoxThreeNo.setEnabled(false);

        //set xml based on patient answers
        editAnswerThreeXML(arrayThree);

        checkBoxFourYes = (CheckBox) view.findViewById(R.id.checkbox_yes_overview_q4);
        checkBoxFourYes.setEnabled(false);
        checkBoxFourNo = (CheckBox) view.findViewById(R.id.checkbox_no_overview_q4);
        checkBoxFourNo.setEnabled(false);

        //set xml based on patient answers
        editAnswerFourXML(arrayFour);

        checkBoxFiveYes = (CheckBox) view.findViewById(R.id.checkbox_yes_overview_q5);
        checkBoxFiveYes.setEnabled(false);
        checkBoxFiveNo = (CheckBox) view.findViewById(R.id.checkbox_no_overview_q5);
        checkBoxFiveNo.setEnabled(false);

        //set xml based on patient answers
        editAnswerFiveXML(arrayFive);

        //edit buttons setup
        editOne = (ImageButton) view.findViewById(R.id.edit_button_01);
        editOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editAnswerOne();
            }
        });

        editTwo = (ImageButton) view.findViewById(R.id.edit_button_02);
        editTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editAnswerTwo();
            }
        });

        editThree = (ImageButton) view.findViewById(R.id.edit_button_03);
        editThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editAnswerThree();
            }
        });

        editFour = (ImageButton) view.findViewById(R.id.edit_button_04);
        editFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editAnswerFour();
            }
        });

        editFive = (ImageButton) view.findViewById(R.id.edit_button_05);
        editFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editAnswerFive();
            }
        });

        return view;
    }

    /***********************************************************************************************
     *launches EditAnswerOneDialogFragment, sets SuperOverviewFragment as target fragment for return
     * answers from user.
     */
    private void editAnswerOne(){
        FragmentManager fragmentManager = getFragmentManager();
        EditAnswerOneDialogFragment dialogFragment = EditAnswerOneDialogFragment.newInstance();

        dialogFragment.setTargetFragment(SuperOverviewFragment.this, REQUEST_ANSWER_ONE);
        dialogFragment.show(fragmentManager, DIALOG_ANSWER);
    }

    /***********************************************************************************************
     *launches EditAnswerTwoDialogFragment, sets SuperOverviewFragment as target fragment for return
     * answers from user.
     */
    private void editAnswerTwo(){
        FragmentManager fragmentManager = getFragmentManager();
        EditAnswerTwoDialogFragment dialogFragment = EditAnswerTwoDialogFragment.newInstance();
        dialogFragment.setTargetFragment(SuperOverviewFragment.this, REQUEST_ANSWER_TWO);
        dialogFragment.show(fragmentManager, DIALOG_ANSWER);
    }

    /***********************************************************************************************
     *launches EditAnswerThreeDialogFragment, sets SuperOverviewFragment as target fragment for return
     * answers from user.
     */
    private void editAnswerThree(){
        FragmentManager fragmentManager = getFragmentManager();
        EditAnswerThreeDialogFragment dialogFragment = EditAnswerThreeDialogFragment.newInstance();
        dialogFragment.setTargetFragment(SuperOverviewFragment.this, REQUEST_ANSWER_THREE);
        dialogFragment.show(fragmentManager, DIALOG_ANSWER);
    }

    /***********************************************************************************************
     *launches EditAnswerFourDialogFragment, sets SuperOverviewFragment as target fragment for return
     * answers from user.
     */
    private void editAnswerFour(){
        FragmentManager fragmentManager = getFragmentManager();
        EditAnswerFourDialogFragment dialogFragment = EditAnswerFourDialogFragment.newInstance();
        dialogFragment.setTargetFragment(SuperOverviewFragment.this, REQUEST_ANSWER_FOUR);
        dialogFragment.show(fragmentManager, DIALOG_ANSWER);
    }

    /***********************************************************************************************
     *launches EditAnswerFiveDialogFragment, sets SuperOverviewFragment as target fragment for return
     * answers from user.
     */
    private void editAnswerFive(){
        FragmentManager fragmentManager = getFragmentManager();
        EditAnswerFiveDialogFragment dialogFragment = EditAnswerFiveDialogFragment.newInstance();
        dialogFragment.setTargetFragment(SuperOverviewFragment.this, REQUEST_ANSWER_FIVE);
        dialogFragment.show(fragmentManager, DIALOG_ANSWER);
    }

    /***********************************************************************************************
     *attaches listener
     */
    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        listener = (SuperOverviewListener) context;
    }

    /***********************************************************************************************
     *receives corrected answers from EditAnswer___DialogFragment. checks requestCode for which
     * answer to update in xml and in database
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode != Activity.RESULT_OK){
            return;
        }
        switch (requestCode){
            case REQUEST_ANSWER_ONE:
                boolean arrayOne[] = new boolean[ARRAY_SIZE];
                arrayOne[0] = data.getBooleanExtra(INTENT_YESNO, true);
                arrayOne[1] = data.getBooleanExtra(INTENT_CHECK_ONE, true);
                arrayOne[2] = data.getBooleanExtra(INTENT_CHECK_TWO, true);
                arrayOne[3] = data.getBooleanExtra(INTENT_CHECK_THREE, true);
                arrayOne[4] = data.getBooleanExtra(INTENT_CHECK_FOUR, true);

                //update xml with new answers
                editAnswerOneXML(arrayOne);

                //update patient object
                listener.onSelectedQuestionOne
                        (arrayOne[0], arrayOne[1], arrayOne[2], arrayOne[3], arrayOne[4]);
                listener.onSelectedAnswer(QUESTION_ONE, arrayOne[0], arrayOne[1],
                        arrayOne[2], arrayOne[3], arrayOne[4], "", Magic.stampThatFool());
                break;
            case REQUEST_ANSWER_TWO:
                boolean arrayTwo[] = new boolean[ARRAY_SIZE];
                arrayTwo[0] = data.getBooleanExtra(INTENT_YESNO, true);
                arrayTwo[1] = data.getBooleanExtra(INTENT_CHECK_ONE, true);
                arrayTwo[2] = data.getBooleanExtra(INTENT_CHECK_TWO, true);
                arrayTwo[3] = data.getBooleanExtra(INTENT_CHECK_THREE, true);

                //update xml with new answers
                editAnswerTwoXML(arrayTwo);

                //update patient object
                listener.onSelectedQuestionTwo
                        (arrayTwo[0], arrayTwo[1], arrayTwo[2], arrayTwo[3], false);
                listener.onSelectedAnswer(QUESTION_TWO, arrayTwo[0], arrayTwo[1],
                        arrayTwo[2], arrayTwo[3], false, "", Magic.stampThatFool());
                break;
            case REQUEST_ANSWER_THREE:
                boolean checkedThree[] = new boolean[ARRAY_SIZE];
                checkedThree[0] = data.getBooleanExtra(INTENT_YESNO, true);

                //update xml with new answers
                editAnswerThreeXML(checkedThree);

                //update patient object
                listener.onSelectedQuestionThree(checkedThree[0]);
                listener.onSelectedAnswer(QUESTION_THREE, checkedThree[0], false, false,
                        false, false, "", Magic.stampThatFool());
                break;
            case REQUEST_ANSWER_FOUR:
                boolean checkedFour[] = new boolean[ARRAY_SIZE];
                checkedFour[0] = data.getBooleanExtra(INTENT_YESNO, true);

                //update xml with new answers
                editAnswerFourXML(checkedFour);

                //update patient object
                listener.onSelectedQuestionFour(checkedFour[0]);
                listener.onSelectedAnswer(QUESTION_FOUR, checkedFour[0], false, false,
                        false, false, "", Magic.stampThatFool());
                break;
            case REQUEST_ANSWER_FIVE:
                boolean checkedFive[] = new boolean[ARRAY_SIZE];
                checkedFive[0]= data.getBooleanExtra(INTENT_YESNO, true);

                //update xml with new answers
                editAnswerFiveXML(checkedFive);

                //update patient object
                listener.onSelectedQuestionFive(checkedFive[0]);
                listener.onSelectedAnswer(QUESTION_FIVE, checkedFive[0], false, false,
                        false, false, "", Magic.stampThatFool());
                break;
        }
    }

    /***********************************************************************************************
     *sets xml to patient answers in onCreateView and used to update in onActivityResult
     */
    private void editAnswerOneXML(boolean arrayOne[]){
        if(arrayOne[0]){
            checkBoxOneYes.setChecked(arrayOne[0]);
            checkBoxOneNo.setChecked(!arrayOne[0]);
            int count = 0;
            for(boolean x : arrayOne){
                int visibility = x ? View.VISIBLE : View.INVISIBLE;
                switch (count){
                    case 1:
                        breathing.setVisibility(visibility);
                        break;
                    case 2:
                        wheezing.setVisibility(visibility);
                        break;
                    case 3:
                        coughing.setVisibility(visibility);
                        break;
                    case 4:
                        choking.setVisibility(visibility);
                        break;
                }
                count++;

            }
        }else{
            checkBoxOneYes.setChecked(arrayOne[0]);
            checkBoxOneNo.setChecked(!arrayOne[0]);
            breathing.setVisibility(View.INVISIBLE);
            wheezing.setVisibility(View.INVISIBLE);
            coughing.setVisibility(View.INVISIBLE);
            choking.setVisibility(View.INVISIBLE);
        }
    }

    /***********************************************************************************************
     *sets xml to patient answers in onCreateView and used to update in onActivityResult
     */
    private void editAnswerTwoXML(boolean arrayTwo[]){
        if(arrayTwo[0]){
            checkBoxTwoYes.setChecked(arrayTwo[0]);
            checkBoxTwoNo.setChecked(!arrayTwo[0]);
            int count = 0;
            for(boolean x : arrayTwo){
                int visibility = x ? View.VISIBLE : View.INVISIBLE;
                switch (count){
                    case 1:
                        burning.setVisibility(visibility);
                        break;
                    case 2:
                        tightness.setVisibility(visibility);
                        break;
                    case 3:
                        pain.setVisibility(visibility);
                        break;
                    case 4:
                        //somethingThree.setVisibility(visibility);
                        break;
                }
                count++;
            }
        }else{
            checkBoxTwoYes.setChecked(arrayTwo[0]);
            checkBoxTwoNo.setChecked(!arrayTwo[0]);
            burning.setVisibility(View.INVISIBLE);
            tightness.setVisibility(View.INVISIBLE);
            pain.setVisibility(View.INVISIBLE);
        }
    }

    /***********************************************************************************************
     *sets xml to patient answers in onCreateView and used to update in onActivityResult
     */
    private void editAnswerThreeXML(boolean checkedThree[]){
        if(checkedThree[0]){
            checkBoxThreeYes.setChecked(checkedThree[0]);
            checkBoxThreeNo.setChecked(!checkedThree[0]);
        }else{
            checkBoxThreeNo.setChecked(!checkedThree[0]);
            checkBoxThreeYes.setChecked(checkedThree[0]);
        }
    }

    /***********************************************************************************************
     *sets xml to patient answers in onCreateView and used to update in onActivityResult
     */
    private void editAnswerFourXML(boolean checkedFour[]){
        if(checkedFour[0]){
            checkBoxFourYes.setChecked(checkedFour[0]);
            checkBoxFourNo.setChecked(!checkedFour[0]);
        }else{
            checkBoxFourNo.setChecked(!checkedFour[0]);
            checkBoxFourYes.setChecked(checkedFour[0]);
        }

    }

    /***********************************************************************************************
     *sets xml to patient answers in onCreateView and used to update in onActivityResult
     */
    private void editAnswerFiveXML(boolean checkedFive[]){
        if(checkedFive[0]){
            checkBoxFiveYes.setChecked(checkedFive[0]);
            checkBoxFiveNo.setChecked(!checkedFive[0]);
        }else{
            checkBoxFiveNo.setChecked(!checkedFive[0]);
            checkBoxFiveYes.setChecked(checkedFive[0]);
        }
    }

}
