package com.mycompany.triage_v2.SuperNurse;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.mycompany.triage_v2.Helpers.BackgroundTask;
import com.mycompany.triage_v2.Helpers.Magic;
import com.mycompany.triage_v2.Kiosk.DemographicsFragment;
import com.mycompany.triage_v2.MainActivity;
import com.mycompany.triage_v2.Models.Action;
import com.mycompany.triage_v2.Models.Answer;
import com.mycompany.triage_v2.Models.AnswerEnum;
import com.mycompany.triage_v2.Models.Exposure;
import com.mycompany.triage_v2.Models.Patient;
import com.mycompany.triage_v2.Models.PatientMngr;
import com.mycompany.triage_v2.Models.ServerAnswer;
import com.mycompany.triage_v2.Models.Triage;
import com.mycompany.triage_v2.R;
import com.mycompany.triage_v2.Views.FragmentError;

import java.util.Date;

public class SuperNursePagerActivity extends AppCompatActivity implements
        PrimaryTriage.primaryTriageListener,
        VitalsFragment.vitalsFragmentListener,
        FinalTriageFragment.finalTriageListener,
        SuperOverviewFragment.SuperOverviewListener,
        DemographicsFragment.demographicsListener,
        PrimaryTriageDialogFragment.PrimaryTriageDialogListener,
        ConfirmExitDialogFragment.ConfirmExitDialogListener{

    //static varibles for naviation
    private static final String RESTART = "Restart";
    private static final String PATIENT_ID = "patientID";
    private static final String INTENT_DESTINATION = "intent destination";
    private static final String VANISH = "vanish";
    private static String DIALOG_CONFIRM_EXIT = "dialog_confirm_exit";

    //static variables for pushing to server
    private static final String ADD_TRIAGE_LVL = "add_triage_lvl";
    private static final String ADD_REC_TRIAGE_LVL = "add_rec_triage_lvl";
    private static final String ADD_QUESTION = "add_question";
    private static final String ADD_VITALS = "add_vitals";
    private static final String ADD_ADDRESS = "add_address";
    private static final String ADD_LATLONG = "add_lat_long";
    private static final String ACTION_NOTES = "action_notes";
    private static final String EXPOSURE_NOTES = "exposure_notes";
    private static final String ADD_COMMENTS = "add_comments";
    private static final String ADD_DEMOGRAPHICS = "add_demographics";
    private static final String ADD_USER = "add_user";

    //server controls

    private static final String TAG = "trace";
    private static final Double BOUNDS = 4.0;

    //titles
    private static final String PRIMARY = "Primary Triage";
    private static final String VITALS = "Vitals";
    private static final String KIOSK_OVERVIEW = "Kiosk Overview";
    private static final String MAP = "Map";
    private static final String FINAL_TRIAGE = "Final Triage";
    private static final String DEMOGRAPHICS = "Demographics";
    private static final String HOUSTON = "Houston, we have a problem";

    // widgets
    private SectionsPagerAdapter sectionsPagerAdapter;
    private ViewPager viewPager;
    private TabLayout tabLayout;

    //determines which pager to load at runtime
    private int destinationID = 0;

    //fragment count for pager set at runtime
    private int fragmentCount = 0;

    // track submission button presses
    private int triageCount = 0;
    private int vitalsCount = 0;

    //patient information
    private String patient_id;
    private String user_id;
    private Patient patient;
    private PatientMngr patientMngr;

    /***********************************************************************************************
     * creates new intent
     * destination determines which nurse pager to load
     * patientId identifies patient to be loaded
     * called from BarCodeActivity
     */
    public static Intent newIntent(Context context, String patientId, int destination){
        Intent intent = new Intent(context, SuperNursePagerActivity.class);
        intent.putExtra(INTENT_DESTINATION, destination);
        intent.putExtra(PATIENT_ID, patientId);
        return intent;
    }

    /***********************************************************************************************
     * creates activity
     */
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_super_nurse_pager);

        //get destination - determines which nurse mode to load
        destinationID = getIntent().getIntExtra(INTENT_DESTINATION, 0);
        setFragmentCount(destinationID);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_home_white_24dp);

        tabLayout = (TabLayout) findViewById(R.id.tabs);

        sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        viewPager = (ViewPager)findViewById(R.id.container);
        viewPager.setAdapter(sectionsPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        //check if there is a patient to load
        patient_id = getIntent().getStringExtra(PATIENT_ID);
        patientMngr = PatientMngr.get(this);
        user_id = patientMngr.getUserId();
        patient = patientMngr.getPatient(patient_id);
        actionBar.setTitle(patient.getTitle());

        //makes recommendations on patient triage lvls based on submitted data
        Magic.suggestedLvls(patient);

        submitUserPatient(user_id, patient.getPatientId());

        if(Magic.isConnected(this)) Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();

    }

    /***********************************************************************************************
     * determine the correct # of fragments the pager should hold
     */
    private void setFragmentCount(int fragCount){
        fragmentCount = fragCount == R.id.nav_primary ? 2 : 5;
    }

    /***********************************************************************************************
     * submits user and patient info to server.
     * used to record when a nurse interacts with a patient
     *
     * @param userId        nurse user id
     * @param patientId     patient id
     */
    public void submitUserPatient(String userId, String patientId){
        if(Magic.isConnected(this)){
            BackgroundTask backgroundTask = new BackgroundTask(this);
            backgroundTask.execute(ADD_USER, patientId, userId);
        } else {
            patientMngr.storeData(Magic.storeDataMiddleMan(ADD_USER, patientId, userId));
        }

    }


    /***********************************************************************************************
     * send final recommendations for triage lvl to server
     */
    public void finished(){
        if(Magic.isConnected(this)){
            BackgroundTask backgroundTask = new BackgroundTask(this);
            backgroundTask.execute(ADD_REC_TRIAGE_LVL, patient_id,
                    patient.getRecExposureLvl().toString(), patient.getRecAction().toString());
        } else {
            patientMngr.storeData(Magic.storeDataMiddleMan(ADD_REC_TRIAGE_LVL, patient_id,
                    patient.getRecExposureLvl().toString(), patient.getRecAction().toString()));
        }
    }


    /***********************************************************************************************
     * Records patients triage levels and sends data to server db
     *
     * @param primary       patient's primary triage level
     * @param exposure      patient's exposure level
     * @param action        patient's action level
     */
    public void onSelectedTriagelvl(Triage primary, Exposure exposure, Action action){
        triageCount++;
        if(Magic.isConnected(this)){
            BackgroundTask backgroundTask = new BackgroundTask(this);
            backgroundTask.execute(ADD_TRIAGE_LVL, patient_id,
                    primary.toString(), exposure.toString(), action.toString(),
                    Magic.stampThatFool());
            onSelectedDemographics(patient.getFirstName(), patient.getLastName(), patient.getDob());
        } else {
            patientMngr.storeData(Magic.storeDataMiddleMan(ADD_TRIAGE_LVL, patient_id,
                    primary.toString(), exposure.toString(), action.toString(),
                    Magic.stampThatFool()));
            onSelectedDemographics(patient.getFirstName(), patient.getLastName(), patient.getDob());

        }
    }

    /***********************************************************************************************
     * Records patient's demographics data and sends data to server db
     *
     * @param first     patient's first name
     * @param last      patient's last name
     * @param dob       patient's date of birth
     */
    public void onSelectedDemographics(String first, String last, Date dob){
        java.sql.Date sqlDate = new java.sql.Date(dob.getTime());
        if(Magic.isConnected(this)){
            BackgroundTask backgroundTask = new BackgroundTask(this);
            backgroundTask.execute(ADD_DEMOGRAPHICS, patient_id, first, last, sqlDate.toString());
        } else {
            patientMngr.storeData(Magic.storeDataMiddleMan(ADD_DEMOGRAPHICS, patient_id,
                    first, last, sqlDate.toString()));
        }
    }

    /***********************************************************************************************
     * Records patient vitals data and sends to server db
     *
     * @param heartRate     patient's heart rate
     * @param oxygen        patient's oxygen
     * @param respiration   patient's respiration
     * @param timeStamp     timestamp when patient finished inputting vitals data
     */
    public void onSelectedVitals(int heartRate, int oxygen, int respiration, String timeStamp){
        vitalsCount++;
        if(Magic.isConnected(this)){
            BackgroundTask backgroundTask = new BackgroundTask(this);
            backgroundTask.execute(ADD_VITALS, patient_id, Integer.toString(heartRate),
                    Integer.toString(oxygen), Integer.toString(respiration), timeStamp);
        } else {
            patientMngr.storeData(Magic.storeDataMiddleMan(ADD_VITALS, patient_id, Integer.toString(heartRate),
                    Integer.toString(oxygen), Integer.toString(respiration), timeStamp));
        }

        // sets patient vitals to current selection.
        // done b/c PatientMngr won't update once these vitals are passed and they need to be
        // displayed in FinalTriageFragment
        patient.setHeart(heartRate);
        patient.setOxygen(oxygen);
        patient.setRespiration(respiration);

        // get new rec action based on new vitals
        //makes recommendations on patient triage lvls based on submitted data
        Magic.suggestedLvls(patient);
    }

    /***********************************************************************************************
     * records patient answers and sends to server
     */
    public void onSelectedAnswer(String question, boolean radio, boolean checkOne, boolean checkTwo,
                                 boolean checkThree, boolean checkFour,
                                 String preliminary, String timeStamp){
        if(Magic.isConnected(this)){
            BackgroundTask backgroundTask = new BackgroundTask(this);
            backgroundTask.execute(ADD_QUESTION, patient_id,
                    question, Magic.stringify(radio), Magic.stringify(checkOne), Magic.stringify(checkTwo),
                    Magic.stringify(checkThree), Magic.stringify(checkFour), preliminary, timeStamp);
        } else {
            patientMngr.storeData(Magic.storeDataMiddleMan(ADD_QUESTION, patient_id,
                    question, Magic.stringify(radio), Magic.stringify(checkOne), Magic.stringify(checkTwo),
                    Magic.stringify(checkThree), Magic.stringify(checkFour), preliminary, timeStamp));
        }


    }

    /***********************************************************************************************
     * records changes to patient answer from QuestionDialogFragment and sets them to patient
     */
    public void onSelectedComments(String method, String notes){
        if(Magic.isConnected(this)){
            BackgroundTask backgroundTask = new BackgroundTask(this);
            backgroundTask.execute(ADD_COMMENTS, patient_id, method, notes, Magic.stampThatFool());
        } else {
            patientMngr.storeData(Magic.storeDataMiddleMan(ADD_COMMENTS, patient_id, method, notes,
                    Magic.stampThatFool()));
        }
    }

    
    /***********************************************************************************************
     * sets patient triage level in sqlite
     */
    public void onSelectedPrimaryTriage(Triage value){
        patient.setTriage(value);
        updatePatient();
    }

    /***********************************************************************************************
     * sets patient exposure level in sqlite
     */
    public void onSelectedExposure(Exposure exposure){
        patient.setExposureLvl(exposure);
        updatePatient();
    }

    /***********************************************************************************************
     * sets patient action level in sqlite
     */
    public void onSelectedAction(Action action){
        patient.setAction(action);
        updatePatient();
    }

    /***********************************************************************************************
     * records changes to patient answer from QuestionDialogFragment and sets them to patient
     */
    public void onSelectedQuestionOne(boolean radio, boolean checkOne, boolean checkTwo,
                                      boolean checkThree, boolean checkFour ){

        Answer qOne = new ServerAnswer("", radio, checkOne, checkTwo, checkThree, checkFour);
        patient.putAnswer(AnswerEnum.ONE, qOne);
        updatePatient();
        //makes recommendations on patient triage lvls based on submitted data
        Magic.suggestedLvls(patient);
    }

    /***********************************************************************************************
     * records changes to patient answer from QuestionDialogFragment and sets them to patient
     */
    public void onSelectedQuestionTwo(boolean radio, boolean checkOne, boolean checkTwo,
                                      boolean checkThree, boolean checkFour ){

        Answer qTwo = new ServerAnswer("", radio, checkOne, checkTwo, checkThree, checkFour);
        patient.putAnswer(AnswerEnum.TWO, qTwo);
        updatePatient();
        //makes recommendations on patient triage lvls based on submitted data
        Magic.suggestedLvls(patient);

    }

    /***********************************************************************************************
     * records changes to patient answer from QuestionDialogFragment and sets them to patient
     */
    public void onSelectedQuestionThree(boolean radio){
        Answer qThree = new ServerAnswer("", radio, false, false, false, false);
        patient.putAnswer(AnswerEnum.THREE, qThree);
        updatePatient();
        //makes recommendations on patient triage lvls based on submitted data
        Magic.suggestedLvls(patient);

    }

    /***********************************************************************************************
     * records changes to patient answer from QuestionDialogFragment and sets them to patient
     */
    public void onSelectedQuestionFour(boolean radio){
        Answer qFour = new ServerAnswer("", radio, false, false, false, false);
        patient.putAnswer(AnswerEnum.FOUR, qFour);
        updatePatient();
        //makes recommendations on patient triage lvls based on submitted data
        Magic.suggestedLvls(patient);

    }

    /***********************************************************************************************
     * records changes to patient answer from QuestionDialogFragment and sets them to patient
     */
    public void onSelectedQuestionFive(boolean radio){
        Answer qFive = new ServerAnswer("", radio, false, false, false, false);
        patient.putAnswer(AnswerEnum.FIVE, qFive);
        updatePatient();
        //makes recommendations on patient triage lvls based on submitted data
        Magic.suggestedLvls(patient);

    }

    /***********************************************************************************************
     * records changes to patient answer from QuestionDialogFragment and sets them to patient
     */
    public void onSelectedNotesExposure(String notes){
        patient.setExposureNotes(notes);
        updatePatient();
    }

    /***********************************************************************************************
     * records changes to patient answer from QuestionDialogFragment and sets them to patient
     */
    public void onSelectedNotesAction(String notes){
        patient.setActionNotes(notes);
        updatePatient();
    }

    /***********************************************************************************************
     * updates patient data on sqlite db
     */
    public void updatePatient(){
        patientMngr.updatePatient(patient);
        //Toast.makeText(this, R.string.patient_updated, Toast.LENGTH_SHORT).show();
    }

    /***********************************************************************************************
     * only here b/c of the drill
     */
    public void jumpForward(View view){}
    public void jumpBack(View view){}

    /***********************************************************************************************
     * send initial triage levels to server if patient entered through the kiosk
     */
    public void submitTriage(){
        // will check for connection within onSelectedTriagelvl
        onSelectedTriagelvl(patient.getTriage(), patient.getExposureLvl(), patient.getAction());

    }

    /***********************************************************************************************
     * returns user to home screen with the proper mode preselected
     */
    public void exitNurseMode(){
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(RESTART, android.R.id.home);
        intent.putExtra(INTENT_DESTINATION, destinationID);
        Log.i(TAG, "(SuperNurse) exitNurseMode ");
        startActivity(intent);
    }

    /***********************************************************************************************
     * Handels menu button clicks
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        switch (menuItem.getItemId()){
            //back to home screen with correct nurse mode
            //check which nurse mode is active.
            //launch dialog if in secondary, otherwise continue to Nurse home
            case android.R.id.home:
                if(destinationID == R.id.nav_secondary){
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    DialogFragment dialogFragment = ConfirmExitDialogFragment.newInstance(vitalsCount, triageCount);
                    dialogFragment.show(fragmentManager, DIALOG_CONFIRM_EXIT);
                } else {
                    Intent intent = new Intent(this, MainActivity.class);
                    intent.putExtra(RESTART, android.R.id.home);
                    intent.putExtra(INTENT_DESTINATION, destinationID);
                    startActivity(intent);
                }
                return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    /***********************************************************************************************
     * class to handle fragments in pageview
     */
    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {


        public SectionsPagerAdapter(FragmentManager fm){
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            //destinationID determines which NursePager to load.
            //works in conjunction with getPageTitle
            switch (destinationID) {
/*                case R.id.nav_super:
                    switch (position) {
                        case 0:
                            return PrimaryTriageResult.newInstance(patient.getTriage());
                        case 1:
                            return VitalsFragment.newInstance(patient.getVitals());
                        case 2:
                            if(!patient.getCompletedKiosk()){
                                return new FragmentError();
                            }else{
                                return SuperOverviewFragment.newInstance(patient.getTitle(),
                                        patient.getAnswer(AnswerEnum.PRELIM).getPrelimAns(),
                                        patient.getAnswer(AnswerEnum.ONE).getCheckable(),
                                        patient.getAnswer(AnswerEnum.TWO).getCheckable(),
                                        patient.getAnswer(AnswerEnum.THREE).getCheckable(),
                                        patient.getAnswer(AnswerEnum.FOUR).getCheckable(),
                                        patient.getAnswer(AnswerEnum.FIVE).getCheckable());
                            }
                        case 3:
                            return AddedLocationsFragment.newInstance(patient.getLatLng(),
                                    patient_id);
                        case 4:
                            return FinalTriageFragment.newInstance(patient.getVitals(), patient_id,
                                    patient.getTriage(), patient.getExposureLvl(), patient.getAction(),
                                    patient.getRecExposureLvl(), patient.getRecAction(),
                                    patient.getLatLng(),
                                    patient.getActionNotes(), patient.getExposureNotes());
                        default:
                            return null;
                    }
*/                case R.id.nav_secondary:
                    switch (position) {
                        case 0:
                            return PrimaryTriageResult.newInstance(patient.getTriage());
                        case 1:
                            return VitalsFragment.newInstance(patient.getVitals());
                        case 2:
                            if(!patient.getCompletedKiosk()){
                                return new FragmentError();
                            }else{
                                return SuperOverviewFragment.newInstance(patient.getTitle(),
                                        patient.getAnswer(AnswerEnum.PRELIM).getPrelimAns(),
                                        patient.getAnswer(AnswerEnum.ONE).getCheckable(),
                                        patient.getAnswer(AnswerEnum.TWO).getCheckable(),
                                        patient.getAnswer(AnswerEnum.THREE).getCheckable(),
                                        patient.getAnswer(AnswerEnum.FOUR).getCheckable(),
                                        patient.getAnswer(AnswerEnum.FIVE).getCheckable());
                            }
                        case 3:
                            return MapsTotalLocationsFragment.newInstance(patient.getLatLng());
                        case 4:
                            return FinalTriageFragment.newInstance(patient.getVitals(), patient_id,
                                    patient.getTriage(), patient.getExposureLvl(), patient.getAction(),
                                    patient.getRecExposureLvl(), patient.getRecAction(),
                                    patient.getLatLng(),
                                    patient.getActionNotes(), patient.getExposureNotes());
                        default:
                            return null;
                    }
                case R.id.nav_primary:
                    switch (position) {
                        case 0:
                            return PrimaryTriage.newInstance(patient.getTriage(), patient.getExposureLvl(), patient.getAction());
                        case 1:
                            return DemographicsFragment.newInstance(VANISH);
                        default:
                            return null;
                    }
                default:
                    return null;
            }
        }

        @Override
        public int getCount(){
            return fragmentCount;
        }

        @Override
        public CharSequence getPageTitle(int position){
            String tabTitle;
            switch (destinationID){
/*                case R.id.nav_super:
                    switch (position){
                        case 0:
                            tabTitle = PRIMARY;
                            break;
                        case 1:
                            tabTitle = VITALS;
                            break;
                        case 2:
                            tabTitle = KIOSK_OVERVIEW;
                            break;
                        case 3:
                            tabTitle = MAP;
                            break;
                        case 4:
                            tabTitle = FINAL_TRIAGE;
                            break;
                        default:
                            tabTitle = HOUSTON;
                    }
                    return tabTitle;
*/              case R.id.nav_secondary:
                    switch (position){
                        case 0:
                            tabTitle = PRIMARY;
                            break;
                        case 1:
                            tabTitle = VITALS;
                            break;
                        case 2:
                            tabTitle = KIOSK_OVERVIEW;
                            break;
                        case 3:
                            tabTitle = MAP;
                            break;
                        case 4:
                            tabTitle = FINAL_TRIAGE;
                            break;
                        default:
                            tabTitle = HOUSTON;
                    }
                    return tabTitle;
                case R.id.nav_primary:
                    switch (position){
                        case 0:
                            tabTitle = PRIMARY;
                            break;
                        case 1:
                            tabTitle = DEMOGRAPHICS;
                            break;
                        default:
                            tabTitle = HOUSTON;
                    }
                    return tabTitle;
                default:
                    return null;
            }
        }
    }
}

