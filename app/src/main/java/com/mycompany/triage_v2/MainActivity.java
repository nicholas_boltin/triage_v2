package com.mycompany.triage_v2;

import android.Manifest;
import android.annotation.TargetApi;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.mycompany.triage_v2.Helpers.BackgroundTask;
import com.mycompany.triage_v2.Helpers.Magic;
import com.mycompany.triage_v2.Kiosk.KioskWelcome;
import com.mycompany.triage_v2.Models.PatientMngr;
import com.mycompany.triage_v2.Views.FragmentMainContent;
import com.mycompany.triage_v2.Views.LogoutDialogFragment;
import com.mycompany.triage_v2.Views.NurseHomeScreen;

import java.util.ArrayList;
import java.util.List;

// Testing if this will work
@TargetApi(23)
public class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener,
        FragmentMainContent.mainContentListener,
        LogoutDialogFragment.LogoutDialogListener{

    //login that will delete sqlite db
    private static final String VALENTINE = "Valentine";

    // server strings
    private static final String UPDATE_PATIENTS = "update_patients_update";
    private static final String ADD_USER = "add_user";

    // dialog strings
    private static final String DIALOG_LOGOUT = "dialog_logout";

    // final variables for intents, permissions, and navigation
    private static final String RESTART = "Restart";
    private static final String INTENT_DESTINATION = "intent destination";
    private static final int REQUEST_CODE_PERMISSIONS = 200;
    private static final String ZERO = "0";
    private static final String TAG = "trace";

    private Toolbar toolbar;
    private DrawerLayout drawer;
    private PatientMngr patientMngr;
    private TextView nurseLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // toolbar layout in app_bar_main.xml
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // drawer layout in activity_main_drawer.xml
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        // get navigation view
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // get header view from within navigation view
        View headerView = navigationView.getHeaderView(0);
        // set to adjust nurse login
        nurseLogin = (TextView) headerView.findViewById(R.id.textView_navheader_nurse);

        // get patientMngr
        patientMngr = PatientMngr.get(this);

        // check if user is logged in
        if(patientMngr.getUserId().isEmpty()){
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        } else {
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            // set subtitle and navigation view to nurse logged in
            displayUserId(patientMngr.getUserId());
        }

        // check for internet connection before retrieving updated patient list
        if(Magic.isConnected(this)){
            //populates new patients before call to PatientMngr
            BackgroundTask backgroundTask = new BackgroundTask(this);
            backgroundTask.execute(UPDATE_PATIENTS, ZERO, patientMngr.getTimeStamp());
            Log.i(TAG, "Update time (MainActivity): " + patientMngr.getTimeStamp());

            // check queue for any data not sent to the server
            // send all data in the queue
            List<String[]> data = patientMngr.getData();
            if(data.size() > 0){
                for(int i = 0; i < data.size(); i++){
                    BackgroundTask bgTask = new BackgroundTask(this);
                    bgTask.execute(data.get(i));
                }
                // delete data from queue once all data has been sent
                patientMngr.deleteData();
            }

        } else {
            Toast.makeText(this, R.string.no_internet, Toast.LENGTH_SHORT).show();
        }

        //check for a return from a previous activity and then loads the appropriate fragment
        int restart = getIntent().getIntExtra(RESTART, 0);
        FragmentManager fragmentManager = getSupportFragmentManager();

        Fragment fragment;
        switch (restart) {
            case R.id.buttonFinished:
                toolbar.setTitle(R.string.kiosk);
                fragment = KioskWelcome.newInstance(R.id.nav_kiosk);
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                break;
            case android.R.id.home:
                int dest = getIntent().getIntExtra(INTENT_DESTINATION, 0);
                switch (dest) {
                    case R.id.nav_primary:
                        toolbar.setTitle(R.string.super_nurse);
                        fragment = NurseHomeScreen.newInstance(R.id.nav_primary);
                        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                        break;
                    case R.id.nav_secondary:
                        toolbar.setTitle(R.string.secondary_nurse);
                        fragment = NurseHomeScreen.newInstance(R.id.nav_secondary);
                        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                        break;
                    case R.id.nav_kiosk:
                        toolbar.setTitle(R.string.kiosk);
                        fragment = KioskWelcome.newInstance(R.id.nav_kiosk);
                        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                        break;
                    case R.id.nav_assisted_kiosk:
                        toolbar.setTitle(R.string.assisted_kiosk);
                        fragment = KioskWelcome.newInstance(R.id.nav_assisted_kiosk);
                        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                        break;

/*                   case R.id.nav_super:
                        // TODO: 6/14/2016 figure out why toolbar isn't updating here - DVJ
                        fragment = NurseHomeScreen.newInstance(R.id.nav_super);
                        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                        toolbar.setTitle(R.string.super_nurse);
                       break;
*/

                    default:
                        toolbar.setTitle(R.string.home);
                        fragmentManager.beginTransaction().replace(R.id.content_frame, new FragmentMainContent()).commit();
                        break;
                }
                break;
            default:
                toolbar.setTitle(R.string.home);
                fragmentManager.beginTransaction().replace(R.id.content_frame, new FragmentMainContent()).commit();
                break;
        }

        //check version for permissions
        if(marshmallow()) {
            //array to store permission checks
            List<String> permissions = new ArrayList<>();
            //get permission from manifest
            int permissionCheckMap = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            int permissionCheckCamera = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);

            //check to see if have permission.  If permission is granted, move on if not
            //add permission request to array for request
            if (permissionCheckMap != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
                permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);

            }
            if (permissionCheckCamera != PackageManager.PERMISSION_GRANTED){
                permissions.add(Manifest.permission.CAMERA);
            }
            //check if array has permissions that need to be checked
            if(!permissions.isEmpty()){
                //asks user for permission for each item in array
                requestPermissions(permissions.toArray(new String[permissions.size()]),REQUEST_CODE_PERMISSIONS);
            }
        }

    }

    /***********************************************************************************************
     * sets the user id for the nurse session
     * @param first - nurse first name
     * @param last - nurse last name
     */
    public void submitUserInfo(String first, String last){
        // unlock drawer
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        StringBuilder userId = new StringBuilder(first.trim() + last.trim());
        patientMngr.setUserId(userId.toString());
        displayUserId(userId.toString());
        //if login is Valentine, delete the database
        if(userId.toString().equals(VALENTINE)){
            patientMngr.clearDataBase();
        }
    }

    /***********************************************************************************************
     * reset user info
     */
    public void logoutUser(){
        patientMngr.resetUserId();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, new FragmentMainContent()).commit();
        displayUserId("");
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

    }

    /***********************************************************************************************
     * set views to current nurse logged in
     * 
     * @param userId
     */
    private void displayUserId(String userId){
        toolbar.setSubtitle(userId);
        nurseLogin.setText(userId);
    }

    public void hideKeyboard(){
        getWindow().setSoftInputMode((WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN));
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        //noinspection SimplifiableIfStatement
        if (item.getItemId() == R.id.action_settings) {
            //check if user logged on
            if(!patientMngr.getUserId().isEmpty()){
                FragmentManager fragmentManager = getSupportFragmentManager();
                DialogFragment dialogFragment = LogoutDialogFragment.newInstance();
                dialogFragment.show(fragmentManager, DIALOG_LOGOUT);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment;

        switch (item.getItemId()){
            case R.id.nav_home:
                toolbar.setTitle(R.string.home);
                Log.i(TAG, "(MainActivity) onNavigationItem ");
                fragmentManager.beginTransaction().replace(R.id.content_frame, new FragmentMainContent()).commit();
                break;
            case R.id.nav_primary:
                toolbar.setTitle(R.string.super_nurse);
                Log.i(TAG, "(MainActivity) onNavigationItem ");
                fragment = NurseHomeScreen.newInstance(R.id.nav_primary);
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                break;
            case R.id.nav_secondary:
                toolbar.setTitle(R.string.secondary_nurse);
                Log.i(TAG, "(MainActivity) onNavigationItem ");
                fragment = NurseHomeScreen.newInstance(R.id.nav_secondary);
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                break;
/*          case R.id.nav_super:
                toolbar.setTitle(R.string.super_nurse);
                fragment = NurseHomeScreen.newInstance(R.id.nav_super);
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                break;
*/          case R.id.nav_kiosk:
                toolbar.setTitle(R.string.kiosk);
                fragment = KioskWelcome.newInstance(R.id.nav_kiosk);
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                break;
            case R.id.nav_assisted_kiosk:
                toolbar.setTitle(R.string.assisted_kiosk);
                fragment = KioskWelcome.newInstance(R.id.nav_assisted_kiosk);
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //check for version
    private boolean marshmallow(){
        return(Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    /***********************************************************************************************
     * opens navigation drawer
     * called after new user logs in
     *
     */
    public void openDrawer(){
        drawer.openDrawer(GravityCompat.START);
    }
}
