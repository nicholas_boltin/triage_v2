package com.mycompany.triage_v2.Helpers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.mycompany.triage_v2.Models.Action;
import com.mycompany.triage_v2.Models.AnswerEnum;
import com.mycompany.triage_v2.Models.Exposure;
import com.mycompany.triage_v2.Models.Patient;
import com.mycompany.triage_v2.Models.PatientMngr;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

public class Magic {

    private static final String ADD_TIMESTAMP = "add_time_stamp";
    private static final String BLANK_ENTRY = "blank_entry";

    private static final String TAG = "trace";

    private static final int LATENCY_PERIOD = 8;
    private static final int DATA_SIZE = 10;
    private static final double LONGITUDE = -81.8375;

    /***********************************************************************************************
     *
     */
    public static String stampThatFool(){
        Calendar calendar = Calendar.getInstance();
        Timestamp timeStamp = new Timestamp(calendar.getTime().getTime());
        return timeStamp.toString();
    }

    /***********************************************************************************************
     *
     */
    public static String stringify(boolean answer){
        return Integer.toString((answer) ? 1 : 0);
    }

    /***********************************************************************************************
     *
     */
    public static void stampThis(Context context, String control, String patient_id){
        if(isConnected(context)){
            BackgroundTask backgroundTask = new BackgroundTask(context);
            backgroundTask.execute(ADD_TIMESTAMP, patient_id, control, stampThatFool());
        } else {
            PatientMngr patientMngr = PatientMngr.get(context);
            patientMngr.storeData(storeDataMiddleMan(ADD_TIMESTAMP, patient_id, control, stampThatFool()));
            Log.i(TAG, "Time stamp for " + control + " stored in queue (Magic)");
        }
    }

    /***********************************************************************************************
     * Determines whether there is currently a network connection
     *
     * @return      true if network is connected, false if network not connected
     */
    public static boolean isConnected(Context context){
        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    /***********************************************************************************************
     * Puts the data to be sent to the server in an array of predetermined size in order to be saved
     * to the sqlite db.  Any missing values are given in order for them not to be null.
     *
     * @param data  a collection of strings
     *
     * @return      the data stored in an array
     */
    public static String[] storeDataMiddleMan(String... data){
        String [] array = new String[DATA_SIZE];
        for(int i = 0; i < data.length; i++){
            array[i] = data[i];
        }
        for(int i = data.length; i < DATA_SIZE; i++){
            array[i] = BLANK_ENTRY;
        }
        return array;
    }


    /***********************************************************************************************
     *
     */
    public static Double distance(Double lat, Double lon){
        final int R = 3959; // radius of earth in miles (6371km)
        Double xo = 33.563331;  // lat of Graniteville SC
        Double yo = -81.8081622;  // lon of Graniteville SC
        Double latDistance = toRadians(lat - xo);
        Double lonDistance = toRadians(lon - yo);
        Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) +
                Math.cos(toRadians(xo)) * Math.cos(toRadians(lat)) *
                        Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return R * c;
    }

    /***********************************************************************************************
     *
     */
    private static Double toRadians(Double value){
        return value * Math.PI / 180;
    }

    /***********************************************************************************************
     *
     */
    public static void suggestedLvls(Patient patient){
        int count = 0;
        double lon = patient.getLatLng().longitude;

        //check the signs and symptoms of patients
        //cluster 1 s&s
        if(patient.getAnswer(AnswerEnum.ONE).isAnswerOne()) count++;
        //cluster 2 s&s
        if(patient.getAnswer(AnswerEnum.TWO).isAnswerOne()) count++;
        //cluster 3 s&s
        if(patient.getAnswer(AnswerEnum.THREE).isAnswerOne() ||
                patient.getAnswer(AnswerEnum.FOUR).isAnswerOne() ||
                patient.getAnswer(AnswerEnum.FIVE).isAnswerOne()) count++;

        //check the location of the patients
        if(count > 1 && lon > LONGITUDE){
            patient.setRecExposureLvl(Exposure.EXPOSED);
            Log.i(TAG, "EXPOSED FIRED");
        } else if(count < 2 && lon < LONGITUDE) {
            patient.setRecExposureLvl(Exposure.NOTEXPOSED);
            Log.i(TAG, "NOTEXPOSED FIRED");
        } else {
            patient.setRecExposureLvl(Exposure.POTENTIALEXPOSURE);
            Log.i(TAG, "POTENTIALEXPOSED FIRED");
        }

        Log.i(TAG, "(Magic) longitude: " + lon);
        Log.i(TAG, "(Magic) count: " + count);
        Log.i(TAG, "(Magic) Rec Exposure lvls: " + patient.getRecExposureLvl().toString());

        suggestedAction(patient, count);
        
    }

    /***********************************************************************************************
     * Determines the latency period of a patient based on their arrival time in the system compared
     * to the current time
     */
    public static int latencyPeriod(Date date){
        Date now = new Date();
        long hours = 0;

        try{
            long difference = now.getTime() - date.getTime();
            hours = difference / (60 * 60 * 1000);
        } catch (Exception e){

        }
        return (int) hours;
    }

    /***********************************************************************************************
     * checks if patient vitals for heart and respiration are in the danger zone.
     * returns true if any set is within danger zone criteria
     */
    private static boolean flags(int age, int heart, int respiration){

        // check heart rate danger zone vitals
        if(age < 4 && heart > 180) return true;
        if(age < 9 && heart > 160) return true;
        if(heart > 100) return true;

        // check respiration danger zone vitlas
        if(heart == 0) return false;
        if(age < 4 && respiration > 40) return true;
        if(age < 9 && respiration > 30) return true;
        if(respiration > 20) return true;

        return false;
    }

    /***********************************************************************************************
     *
     */
    private static void suggestedAction(Patient patient, int clusters){

        // patient set to urgent if oxygen vitals w/n danger zone
        if(patient.getOxygen() > 0 && patient.getOxygen() < 92){
            patient.setRecAction(Action.URGENT);
        } else {
            // check rec exposure level
            switch (patient.getRecExposureLvl()){

                //patient not exposed exits hospital system
                case NOTEXPOSED:
                    patient.setRecAction(Action.EXIT);
                    break;
                //patient potential exposure check for s&s
                //set to monitor if clusters > 1 exit otherwise
                case POTENTIALEXPOSURE:
                    if(clusters > 1){
                        patient.setRecAction(Action.MONITOR);
                    } else {
                        patient.setRecAction(Action.EXIT);
                    }
                    break;
                //patient exposure check for s&s
                //set to monitor if clusters > 1 exit otherwise
                case EXPOSED:
                    if(clusters > 1){
                        patient.setRecAction(Action.MONITOR);
                    } else {
                        patient.setRecAction(Action.EXIT);
                    }
                    break;
                //set to none on default
                default:
                    patient.setRecAction(Action.NONE);

            }
        }

    }

}
