package com.mycompany.triage_v2.Helpers;


import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.mycompany.triage_v2.Models.PatientMngr;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class BackgroundTask extends AsyncTask<String, Void, String>{

    private static final String TAG = "trace";


    /***********************************************************************************************
     * urls
     */
    private static final String URL_ADD_TRAIGE_LVL = "http://129.252.131.78/triage_db/add_triage_lvl.php";
    private static final String URL_ADD_REC_TRIAGE_LVL = "http://129.252.131.78/triage_db/add_rec_triage_lvl.php";
    private static final String URL_ADD_DEMOGRAPHICS = "http://129.252.131.78/triage_db/add_demographics.php";
    private static final String URL_ADD_ANSWERS = "http://129.252.131.78/triage_db/add_answers.php";
    private static final String URL_ADD_VITALS = "http://129.252.131.78/triage_db/add_vitals.php";
    private static final String URL_ADD_ADDRESS = "http://129.252.131.78/triage_db/add_address.php";
    private static final String URL_ADD_COMMENTS = "http://129.252.131.78/triage_db/add_comments.php";
    private static final String URL_ADD_TIMESTAMP = "http://129.252.131.78/triage_db/add_time_stamp.php";
    private static final String URL_ADD_USER = "http://129.252.131.78/triage_db/add_user.php";


    private static final String URL_GET_PATIENTS = "http://129.252.131.78/triage_db/get_patients.php";
    private static final String URL_GET_PATIENTS_NEW = "http://129.252.131.78/triage_db/get_patients_new.php";
    private static final String URL_UPDATE_PATIENTS = "http://129.252.131.78/triage_db/update_patients.php";

    /***********************************************************************************************
     * controls
     */
    private static final String ADD_TRIAGE_LVL = "add_triage_lvl";
    private static final String ADD_REC_TRIAGE_LVL = "add_rec_triage_lvl";
    private static final String ADD_DEMOGRAPHICS = "add_demographics";
    private static final String ADD_QUESTION = "add_question";
    private static final String ADD_VITALS = "add_vitals";
    private static final String ADD_ADDRESS = "add_address";
    private static final String ADD_LATLONG = "add_lat_long";
    private static final String ADD_COMMENTS = "add_comments";
    private static final String ADD_TIMESTAMP = "add_time_stamp";
    private static final String ADD_USER = "add_user";

    private static final String GET_PATIENTS = "get_patients";
    private static final String UPDATE_PATIENTS = "update_patients_update";

    //encoding
    private static final String UTF = "UTF-8";
    private static final String ISO = "iso-8859-1";
    private static final String POST = "POST";

    /***********************************************************************************************
     * db identifiers
     */

    //patient id
    private static final String PATIENT_ID = "patient_id";

    //user id
    private static final String USER_ID = "user_id";

    //primary triage
    private static final String PRIMARY_TRIAGE = "p_triage";
    private static final String EXPOSURE = "exposure";
    private static final String ACTION = "action";
    private static final String TIMESTAMP = "time_stamp";

    //demographics
    private static final String FIRSTNAME = "first_name";
    private static final String LASTNAME = "last_name";
    private static final String DOB = "dob";

    //answers
    private static final String QUESTION_NUMBER = "question_number";
    private static final String ANSWER_ONE = "answer_one";
    private static final String ANSWER_TWO = "answer_two";
    private static final String ANSWER_THREE = "answer_three";
    private static final String ANSWER_FOUR = "answer_four";
    private static final String ANSWER_FIVE = "answer_five";
    private static final String ANSWER_SIX = "answer_six";

    //vitals
    private static final String HEART_RATE = "heart_rate";
    private static final String OXYGEN = "oxygen";
    private static final String RESPIRATION = "respiration";

    //address
    private static final String ADDRESS = "address";
    private static final String LAT = "lat";
    private static final String LON = "lon";

    //comments
    private static final String NOTES = "notes";
    private static final String CONTROL = "control";

    private String JSON_STRING;

    Context context;

    public BackgroundTask(Context context){
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {

        String method = params[0];
        String patient_id = params[1];
        String time_stamp;
        String control;

        switch(method){
            case ADD_TRIAGE_LVL:

                String primary_triage_lvl = params[2];
                String exposure = params[3];
                String action = params[4];
                time_stamp = params[5];

                try{

                    URL url = new URL(URL_ADD_TRAIGE_LVL);
                    HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                    httpURLConnection.setRequestMethod(POST);
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);

                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, UTF));
                    String data =
                            URLEncoder.encode(PATIENT_ID, UTF) + "=" + URLEncoder.encode(patient_id, UTF) + "&" +
                                    URLEncoder.encode(PRIMARY_TRIAGE, UTF) + "=" + URLEncoder.encode(primary_triage_lvl, UTF) + "&" +
                                    URLEncoder.encode(EXPOSURE, UTF) + "=" + URLEncoder.encode(exposure, UTF) + "&" +
                                    URLEncoder.encode(ACTION, UTF) + "=" + URLEncoder.encode(action, UTF) + "&" +
                                    URLEncoder.encode(TIMESTAMP, UTF) + "=" + URLEncoder.encode(time_stamp, UTF);

                    bufferedWriter.write(data);
                    bufferedWriter.flush();
                    bufferedWriter.close();

                    outputStream.close();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,ISO));
                    String response = "";
                    String line = "";
                    while ((line = bufferedReader.readLine())!=null) {
                        response+= line;
                    }
                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();

                    return response;

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e){
                    e.printStackTrace();
                }

                break;

            case ADD_REC_TRIAGE_LVL:

                String recExposure = params[2];
                String recAction = params[3];

                try{

                    URL url = new URL(URL_ADD_REC_TRIAGE_LVL);
                    HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                    httpURLConnection.setRequestMethod(POST);
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);

                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, UTF));
                    String data =
                            URLEncoder.encode(PATIENT_ID, UTF) + "=" + URLEncoder.encode(patient_id, UTF) + "&" +
                                    URLEncoder.encode(EXPOSURE, UTF) + "=" + URLEncoder.encode(recExposure, UTF) + "&" +
                                    URLEncoder.encode(ACTION, UTF) + "=" + URLEncoder.encode(recAction, UTF);

                    bufferedWriter.write(data);
                    bufferedWriter.flush();
                    bufferedWriter.close();

                    outputStream.close();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,ISO));
                    String response = "";
                    String line = "";
                    while ((line = bufferedReader.readLine())!=null) {
                        response+= line;
                    }
                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();

                    return response;

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e){
                    e.printStackTrace();
                }

                break;

            case ADD_DEMOGRAPHICS:

                String first_name = params[2];
                String last_name = params[3];
                String dob = params[4];

                try{

                    URL url = new URL(URL_ADD_DEMOGRAPHICS);
                    HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                    httpURLConnection.setRequestMethod(POST);
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);

                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, UTF));
                    String data =
                            URLEncoder.encode(PATIENT_ID, UTF) + "=" + URLEncoder.encode(patient_id, UTF) + "&" +
                                    URLEncoder.encode(FIRSTNAME, UTF) + "=" + URLEncoder.encode(first_name, UTF) + "&" +
                                    URLEncoder.encode(LASTNAME, UTF) + "=" + URLEncoder.encode(last_name, UTF) + "&" +
                                    URLEncoder.encode(DOB, UTF) + "=" + URLEncoder.encode(dob, UTF);

                    bufferedWriter.write(data);
                    bufferedWriter.flush();
                    bufferedWriter.close();

                    outputStream.close();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, ISO));
                    String response = "";
                    String line = "";
                    while ((line = bufferedReader.readLine())!=null) {
                        response+= line;
                    }
                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();

                    return response;

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e){
                    e.printStackTrace();
                }
                break;

            case ADD_QUESTION:

                String question_number = params[2];
                String answer_one = params[3];
                String answer_two = params[4];
                String answer_three = params[5];
                String answer_four = params[6];
                String answer_five = params[7];
                String answer_six = params[8];
                time_stamp = params[9];

                try{

                    URL url = new URL(URL_ADD_ANSWERS);
                    HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                    httpURLConnection.setRequestMethod(POST);
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);

                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, UTF));
                    String data =
                            URLEncoder.encode(PATIENT_ID, UTF) + "=" + URLEncoder.encode(patient_id, UTF) + "&" +
                                    URLEncoder.encode(QUESTION_NUMBER, UTF) + "=" + URLEncoder.encode(question_number, UTF) + "&" +
                                    URLEncoder.encode(ANSWER_ONE, UTF) + "=" + URLEncoder.encode(answer_one, UTF) + "&" +
                                    URLEncoder.encode(ANSWER_TWO, UTF) + "=" + URLEncoder.encode(answer_two, UTF) + "&" +
                                    URLEncoder.encode(ANSWER_THREE, UTF) + "=" + URLEncoder.encode(answer_three, UTF) + "&" +
                                    URLEncoder.encode(ANSWER_FOUR, UTF) + "=" + URLEncoder.encode(answer_four, UTF) + "&" +
                                    URLEncoder.encode(ANSWER_FIVE, UTF) + "=" + URLEncoder.encode(answer_five, UTF) + "&" +
                                    URLEncoder.encode(ANSWER_SIX, UTF) + "=" + URLEncoder.encode(answer_six, UTF) + "&" +
                                    URLEncoder.encode(TIMESTAMP, UTF) + "=" + URLEncoder.encode(time_stamp, UTF);

                    bufferedWriter.write(data);
                    bufferedWriter.flush();
                    bufferedWriter.close();

                    outputStream.close();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, ISO));
                    String response = "";
                    String line;
                    while ((line = bufferedReader.readLine())!=null) {
                        response+= line;
                    }
                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();

                    return response;

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e){
                    e.printStackTrace();
                }
                break;

            case ADD_VITALS:

                String heart_rate = params[2];
                String oxygen = params[3];
                String respiration = params[4];
                time_stamp = params[5];

                try{

                    URL url = new URL(URL_ADD_VITALS);
                    HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                    httpURLConnection.setRequestMethod(POST);
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);

                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, UTF));
                    String data =
                            URLEncoder.encode(PATIENT_ID, UTF) + "=" + URLEncoder.encode(patient_id, UTF) + "&" +
                                    URLEncoder.encode(HEART_RATE, UTF) + "=" + URLEncoder.encode(heart_rate, UTF) + "&" +
                                    URLEncoder.encode(OXYGEN, UTF) + "=" + URLEncoder.encode(oxygen, UTF) + "&" +
                                    URLEncoder.encode(RESPIRATION, UTF) + "=" + URLEncoder.encode(respiration, UTF) + "&" +
                                    URLEncoder.encode(TIMESTAMP, UTF) + "=" + URLEncoder.encode(time_stamp, UTF);

                    bufferedWriter.write(data);
                    bufferedWriter.flush();
                    bufferedWriter.close();

                    outputStream.close();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, ISO));
                    String response = "";
                    String line = "";
                    while ((line = bufferedReader.readLine())!=null) {
                        response+= line;
                    }
                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();

                    return response;

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e){
                    e.printStackTrace();
                }
                break;

            case ADD_ADDRESS:

                String address = params[2];
                String lat = params[3];
                String lon = params[4];

                try{

                    URL url = new URL(URL_ADD_ADDRESS);
                    HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                    httpURLConnection.setRequestMethod(POST);
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);

                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, UTF));
                    String data =
                            URLEncoder.encode(PATIENT_ID, UTF) + "=" + URLEncoder.encode(patient_id, UTF) + "&" +
                                    URLEncoder.encode(ADDRESS, UTF) + "=" + URLEncoder.encode(address, UTF) + "&" +
                                    URLEncoder.encode(LAT, UTF) + "=" + URLEncoder.encode(lat, UTF) + "&" +
                                    URLEncoder.encode(LON, UTF) + "=" + URLEncoder.encode(lon, UTF);

                    bufferedWriter.write(data);
                    bufferedWriter.flush();
                    bufferedWriter.close();

                    outputStream.close();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, ISO));
                    String response = "";
                    String line;
                    while ((line = bufferedReader.readLine())!=null) {
                        response+= line;
                    }
                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();

                    return response;

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e){
                    e.printStackTrace();
                }
                break;

            case ADD_COMMENTS:

                control = params[2];
                String notes = params[3];
                time_stamp = params[4];

                try{

                    URL url = new URL(URL_ADD_COMMENTS);
                    HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                    httpURLConnection.setRequestMethod(POST);
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);

                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, UTF));
                    String data =
                            URLEncoder.encode(PATIENT_ID, UTF) + "=" + URLEncoder.encode(patient_id, UTF) + "&" +
                                    URLEncoder.encode(CONTROL, UTF) + "=" + URLEncoder.encode(control, UTF) + "&" +
                                    URLEncoder.encode(NOTES, UTF) + "=" + URLEncoder.encode(notes, UTF) + "&" +
                                    URLEncoder.encode(TIMESTAMP, UTF) + "=" + URLEncoder.encode(time_stamp, UTF);

                    bufferedWriter.write(data);
                    bufferedWriter.flush();
                    bufferedWriter.close();

                    outputStream.close();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, ISO));
                    String response = "";
                    String line = "";
                    while ((line = bufferedReader.readLine())!=null) {
                        response+= line;
                    }
                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();

                    return response;

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e){
                    e.printStackTrace();
                }
                break;

            case ADD_TIMESTAMP:

                control = params[2];
                time_stamp = params[3];

                try{

                    URL url = new URL(URL_ADD_TIMESTAMP);
                    HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                    httpURLConnection.setRequestMethod(POST);
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);

                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, UTF));
                    String data =
                            URLEncoder.encode(PATIENT_ID, UTF) + "=" + URLEncoder.encode(patient_id, UTF) + "&" +
                                    URLEncoder.encode(CONTROL, UTF) + "=" + URLEncoder.encode(control, UTF) + "&" +
                                    URLEncoder.encode(TIMESTAMP, UTF) + "=" + URLEncoder.encode(time_stamp, UTF);

                    bufferedWriter.write(data);
                    bufferedWriter.flush();
                    bufferedWriter.close();

                    outputStream.close();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, ISO));
                    String response = "";
                    String line = "";
                    while ((line = bufferedReader.readLine())!=null) {
                        response+= line;
                    }
                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();

                    return response;

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e){
                    e.printStackTrace();
                }

                break;

            case GET_PATIENTS:

                try{
                    //URL url = new URL(URL_GET_PATIENTS);
                    URL url = new URL(URL_GET_PATIENTS_NEW);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    StringBuilder stringBuilder = new StringBuilder();
                    while ((JSON_STRING = bufferedReader.readLine()) != null){

                        stringBuilder.append(JSON_STRING + "\n");
                    }

                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();

                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();

                    PatientMngr patientMngr = PatientMngr.get(context);
                    patientMngr.parseThis(stringBuilder.toString().trim());

                    return stringBuilder.toString().trim();

                }catch (MalformedURLException e){
                    e.printStackTrace();
                }catch (IOException e){
                    e.printStackTrace();
                }

                break;


            case UPDATE_PATIENTS:

                time_stamp = params[2];

                try{
                    //URL url = new URL(URL_GET_PATIENTS);
                    URL url = new URL(URL_UPDATE_PATIENTS);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod(POST);
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);

                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, UTF));
                    String data = URLEncoder.encode(TIMESTAMP, UTF) + "=" + URLEncoder.encode(time_stamp, UTF);
                    bufferedWriter.write(data);
                    bufferedWriter.flush();
                    bufferedWriter.close();

                    outputStream.close();

                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    StringBuilder stringBuilder = new StringBuilder();
                    while ((JSON_STRING = bufferedReader.readLine()) != null){

                        stringBuilder.append(JSON_STRING + "\n");
                    }

                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();

                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();

                    PatientMngr patientMngr = PatientMngr.get(context);
                    patientMngr.parseThis(stringBuilder.toString().trim());

                    return UPDATE_PATIENTS;

                }catch (MalformedURLException e){
                    e.printStackTrace();
                }catch (IOException e){
                    e.printStackTrace();
                }

                break;

            case ADD_USER:

                String userId = params[2];

                try{

                    URL url = new URL(URL_ADD_USER);
                    HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                    httpURLConnection.setRequestMethod(POST);
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);

                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, UTF));
                    String data =
                            URLEncoder.encode(PATIENT_ID, UTF) + "=" + URLEncoder.encode(patient_id, UTF) + "&" +
                                    URLEncoder.encode(USER_ID, UTF) + "=" + URLEncoder.encode(userId, UTF);

                    Log.i(TAG, "patient id (BackgroundTask): " + patient_id);

                    bufferedWriter.write(data);
                    bufferedWriter.flush();
                    bufferedWriter.close();

                    outputStream.close();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, ISO));
                    String response = "";
                    String line = "";
                    while ((line = bufferedReader.readLine())!=null) {
                        response+= line;
                    }
                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();

                    return response;

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e){
                    e.printStackTrace();
                }

                break;


        }
        return null;
    }


    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String result) {
        switch (result){
            case UPDATE_PATIENTS:
                Toast.makeText(context, "DB Updated", Toast.LENGTH_LONG).show();
                break;
            default:
                if (!result.isEmpty()) {
                    Toast.makeText(context, result, Toast.LENGTH_LONG).show();
                }
        }

    }

}
