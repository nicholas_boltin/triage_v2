package com.mycompany.triage_v2.Kiosk;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mycompany.triage_v2.Helpers.Magic;
import com.mycompany.triage_v2.R;

public class PreliminaryQuesFragment extends Fragment {

    private static final String PROGRESS_TITLE = "progress_title";
    private static final String QUESTION_NUMBER = "6";

    private EditText preliminaryAnswer;
    private TextView progress;
    private String answer;

    /***********************************************************************************************
     * listener and listener interface to call methods in activity
     */
    preliminaryListener listener;

    public interface preliminaryListener{
        void onSelectedPreliminary(String answer);
        void onSelectedAnswer(String question, boolean radio, boolean checkOne, boolean checkTwo,
                              boolean checkThree, boolean checkFour,
                              String preliminary, String timestamp);
        void jumpForward(View view);
        void jumpBack(View view);
    }

    /***********************************************************************************************
     * requried empty constructor
     */
    public PreliminaryQuesFragment() {}

    /***********************************************************************************************
     *starts new instance of fragment
     */
    public static PreliminaryQuesFragment newInstance(String title) {
        PreliminaryQuesFragment fragment = new PreliminaryQuesFragment();
        Bundle bundle = new Bundle();
        bundle.putString(PROGRESS_TITLE, title);
        fragment.setArguments(bundle);
        return fragment;
    }

    /***********************************************************************************************
     * creates view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_preliminary_ques, container, false);

        // set title w/ numbers
        progress = (TextView) view.findViewById(R.id.textview_progress);
        progress.setText(getArguments().getString(PROGRESS_TITLE));

        preliminaryAnswer = (EditText) view.findViewById(R.id.edit_text_preliminary);
        preliminaryAnswer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                answer = s.toString();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                answer = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

                answer = s.toString();
            }
        });

        Button next = (Button) view.findViewById(R.id.buttonNext);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(answer == null || answer.isEmpty()){
                    Toast.makeText(getActivity(), R.string.response, Toast.LENGTH_SHORT).show();
                }else{
                    listener.onSelectedPreliminary(answer);
                    listener.onSelectedAnswer(QUESTION_NUMBER,
                            false, false, false, false, false, answer, Magic.stampThatFool());
                    listener.jumpForward(v);
                }

            }
        });

        Button prev = (Button) view.findViewById(R.id.buttonBack);
        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.jumpBack(v);
            }
        });

        return view;
    }

    /***********************************************************************************************
     * attach listener
     */
    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        listener = (preliminaryListener) context;
    }

}
