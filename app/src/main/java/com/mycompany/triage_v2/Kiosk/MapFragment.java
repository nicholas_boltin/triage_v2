package com.mycompany.triage_v2.Kiosk;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mycompany.triage_v2.Helpers.GPSHelper;
import com.mycompany.triage_v2.Helpers.Magic;
import com.mycompany.triage_v2.R;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MapFragment extends Fragment implements
        OnMapReadyCallback,
        OnMapClickListener,
        OnMapLongClickListener{

    private static final String DIALOG_ADDRESS = "DialogAddress";
    private static final int REQUEST_ADDRESS = 0;

    //dialog controls
    private static final String INTENT_ADDRESS = "addressIntent";
    private static final String INTENT_LAT = "latIntent";
    private static final String INTENT_LNG = "lngIntent";
    private static final String BUNDLE_PATIENT_NAME = "patient_first";

    private GoogleMap map;
    private MapView mapView;
    private EditText addressEdit;
    //scope of view to be able to be accessed after dialog fragment confirmation
    private View view;

    private String patientName;

    private MarkerOptions markerOptions;
    private LatLng latLng;
    private LatLng latLngResult;

    /***********************************************************************************************
     * listener and listener interface to call methods in activity
     */
    mapsListener listener;

    public interface mapsListener{
        void onSelectedAddress(String address, LatLng latLng);
        void jumpForward(View view);
        void jumpBack(View view);
    }

    /***********************************************************************************************
     * required empty constructor
     */
    public MapFragment() {}

    /***********************************************************************************************
     *starts new instance of fragment, called from KioskTabPagerActivity
     */
    public static MapFragment newInstance(String name) {
        MapFragment fragment = new MapFragment();
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_PATIENT_NAME, name);
        fragment.setArguments(bundle);
        return fragment;
    }

    /***********************************************************************************************
     * creates view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_map, container, false);

        addressEdit = (EditText)view.findViewById(R.id.edit_text_address);

        Button searchButton = (Button)view.findViewById(R.id.button_search);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //get string address
                String address = addressEdit.getText().toString();
                //remove text from address
                addressEdit.setText("");
                //hide keyboard
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

                //check for internet before trying to get latlng from google
                if(Magic.isConnected(getContext())){
                    //submit address if one has been typed in
                    if(address != null && !address.equals("")){
                        //submit address to be found
                        new GeocoderTask().execute(address);
                        //submit the latlng of the marker on the map if not null
                    } else if (latLng != null){
                        address = getCompleteAddressString(latLng.latitude, latLng.longitude);
                        new GeocoderTask().execute(address);
                        //no address was entered
                    } else {
                        Toast.makeText(getContext(), R.string.no_address_entered, Toast.LENGTH_LONG).show();
                    }
                //no internet, place string address for collection
                } else {

                    if(address != null && !address.equals("")){
                        FragmentManager fragmentManager = getFragmentManager();
                        //load latlng and string address google maps returns
                        latLng = new LatLng(0,0);
                        ConfirmAddressDialogFragment dialogFragment = ConfirmAddressDialogFragment.newInstance(address, latLng, patientName);
                        //set target fragments and show dialog
                        dialogFragment.setTargetFragment(MapFragment.this, REQUEST_ADDRESS);
                        dialogFragment.show(fragmentManager, DIALOG_ADDRESS);
                    } else {
                        Toast.makeText(getContext(), R.string.no_address_entered, Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        Button next = (Button) view.findViewById(R.id.button_next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(latLngResult != null){
                    listener.jumpForward(v);
                }else{
                    Toast.makeText(getActivity(), R.string.no_address_entered, Toast.LENGTH_SHORT).show();
                }

            }
        });

        Button prev = (Button) view.findViewById(R.id.button_back);
        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.jumpBack(v);
            }
        });

        mapView = (MapView)view.findViewById(R.id.map_view);
        mapView.onCreate(savedInstanceState);

        mapView.getMapAsync(this);

        patientName = getArguments().getString(BUNDLE_PATIENT_NAME);

        return view;
    }


    /***********************************************************************************************
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setOnMapClickListener(this);
        map.setOnMapLongClickListener(this);
        ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION);
        GPSHelper gpsHelper = new GPSHelper(getContext());
        gpsHelper.getMyLocation();
        LatLng initialLocation = new LatLng(gpsHelper.getLatitude(), gpsHelper.getLongitude());
        map.setMyLocationEnabled(true);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(initialLocation, 15));
    }

    /***********************************************************************************************
     * receives response from ConfrimAddressDialog to confirm address selected or cancel
     * submits results to server
     * on ok move on to next screen in the kiosk
     *
     * @param requestCode   which request needs to be fulfilled
     * @param resultCode    the result of the transaction
     * @param data          data to be handeled
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if(resultCode != Activity.RESULT_OK){
            return;
        }
        if(requestCode == REQUEST_ADDRESS){
            String address = data.getStringExtra(INTENT_ADDRESS);
            double lat = data.getDoubleExtra(INTENT_LAT, 0);
            double lng = data.getDoubleExtra(INTENT_LNG, 0);
            latLngResult = new LatLng(lat, lng);
            //send results to activity
            listener.onSelectedAddress(address, latLngResult);
            //jump to next screen on confirm
            listener.jumpForward(view);
        }
    }

    /***********************************************************************************************
     * clears any old markers and places a new one on user clicks
     */
    @Override
    public void onMapClick(LatLng point) {
        map.clear();
        markerOptions = new MarkerOptions();
        markerOptions.position(point);
        markerOptions.title(point.toString());
        map.addMarker(markerOptions);
        latLng = point;
    }

    /***********************************************************************************************
     * clears any old markers and places a new one on long press user clicks
     */
    @Override
    public void onMapLongClick(LatLng point) {
        map.clear();
        markerOptions = new MarkerOptions();
        markerOptions.position(point);
        markerOptions.title(point.toString());
        map.addMarker(markerOptions);
        latLng = point;
    }

    /***********************************************************************************************
     * attach listener
     */
    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        listener = (mapsListener) context;
    }

    /***********************************************************************************************
     * resume map
     */
    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    /***********************************************************************************************
     * destroy map
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    /***********************************************************************************************
     * handle low memory issues
     */
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    /***********************************************************************************************
     * gets a string address for any submitted markers on the map
     */
    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
        try {
            //return 1 address based on latlng
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                // create string address
                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append(", ");
                }
                strAdd = strReturnedAddress.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strAdd;
    }

    /***********************************************************************************************
     * Background task to get address and lat long information from map
     */
    public class GeocoderTask extends AsyncTask<String, Void, List<Address>> {
        private static final String TAG = "GeocoderTask";

        @Override
        protected List<Address> doInBackground(String... locationName){
            Geocoder geocoder = new Geocoder(getContext());
            List<Address> addresses = null;
            try{
                //return only 1 address
                addresses = geocoder.getFromLocationName(locationName[0], 1);
            }catch (IOException e){
                e.printStackTrace();
            }
            return addresses;
        }


        @Override
        protected void onPostExecute(List<Address> addresses) {

            if(addresses==null || addresses.size()==0){
                Toast.makeText(getContext(), R.string.no_location, Toast.LENGTH_SHORT).show();
            }

            // Clears all the existing markers on the map
            map.clear();

            // Adding Markers on Google Map for each matching address
            for(int i=0;i < addresses.size(); i++){

                Address address = (Address) addresses.get(i);

                // Creating an instance of GeoPoint, to display in Google Map
                latLng = new LatLng(address.getLatitude(), address.getLongitude());

                // get the string address based on the retrieved latlng
                // string address to be displayed in dialog f/ patient to validate
                String addressText = getCompleteAddressString(latLng.latitude, latLng.longitude);

                // former method of displaying string address to patient which was the string
                // they typed in the kiosk
                /* convert address from list into string
                String addressText = String.format("%s, %s",
                        address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "",
                        address.getCountryName());
                */

                markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title(addressText);

                map.addMarker(markerOptions);

                // Locate the first location
                if(i==0) {
                    //map.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                    //call dialog fragment to confirm patient address
                    FragmentManager fragmentManager = getFragmentManager();
                    //load latlng and string address google maps returns
                    ConfirmAddressDialogFragment dialogFragment = ConfirmAddressDialogFragment.newInstance(addressText, latLng, patientName);
                    //set target fragments and show dialog
                    dialogFragment.setTargetFragment(MapFragment.this, REQUEST_ADDRESS);
                    dialogFragment.show(fragmentManager, DIALOG_ADDRESS);
                }
            }
        }
    }

}
