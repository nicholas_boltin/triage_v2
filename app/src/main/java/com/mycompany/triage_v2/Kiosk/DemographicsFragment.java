package com.mycompany.triage_v2.Kiosk;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mycompany.triage_v2.R;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Date;

public class DemographicsFragment extends Fragment {

    private static final String TAG = "trace";

    private static final String PROGRESS_TITLE = "progress_title";
    private static final String VANISH = "vanish";

    private EditText firstNameEdit;
    private EditText lastNameEdit;
    private DatePicker datePicker;
    private TextView progress;

    private String firstName;
    private String lastName;
    private Date dob;

    demographicsListener listener;

    public interface demographicsListener{
        void onSelectedDemographics(String first, String last, Date dob);
        void submitTriage();
        void jumpForward(View view);
        void jumpBack(View view);
    }

    public DemographicsFragment() {}

    public static DemographicsFragment newInstance(String title) {
        DemographicsFragment fragment = new DemographicsFragment();
        Bundle bundle = new Bundle();
        bundle.putString(PROGRESS_TITLE, title);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_demographics, container, false);

        // check to see if Demographics fragment is being used in kiosk or in Supervisor Nurse
        String vanish = getArguments().getString(PROGRESS_TITLE);
        if(!vanish.equals(VANISH)){
            progress = (TextView) view.findViewById(R.id.textview_progress);
            progress.setText(getArguments().getString(PROGRESS_TITLE));
        }

        // layouts that will be made invisible based on where the fragment is being used
        LinearLayout prevLayout = (LinearLayout) view.findViewById(R.id.navigation_button_one_layout);
        LinearLayout nextLayout = (LinearLayout) view.findViewById(R.id.navigation_button_two_layout);
        LinearLayout submit = (LinearLayout) view.findViewById(R.id.navigation_button_submit_layout);

        if(vanish.equals(VANISH)){
            // make navigation buttons invisible in Nurse mode
            prevLayout.setVisibility(View.INVISIBLE);
            nextLayout.setVisibility(View.INVISIBLE);
        } else {
            // make submit button invisible in Kiosk mode
            submit.setVisibility(View.INVISIBLE);
        }

        firstNameEdit = (EditText) view.findViewById(R.id.editText_firstName);
        firstNameEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                firstName = s.toString();
            }
        });

        lastNameEdit = (EditText)view.findViewById(R.id.editText_lastName);
        lastNameEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                lastName = s.toString();
            }
        });

        int year, month, day;
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        datePicker = (DatePicker) view.findViewById(R.id.date_picker_one);
        datePicker.init(year, month, day, new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker datePicker, int i, int i1, int i2) {
                dob = new GregorianCalendar(i, i1, i2).getTime();
            }
        });

        Button next = (Button) view.findViewById(R.id.buttonNext1);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(firstName == null || lastName == null || firstName.isEmpty() || lastName.isEmpty()){
                    Toast.makeText(getActivity(), R.string.demographics_check, Toast.LENGTH_SHORT).show();
                } else if(dob == null) {
                    Toast.makeText(getActivity(), R.string.dob_check, Toast.LENGTH_SHORT).show();
                } else {
                    listener.onSelectedDemographics(firstName, lastName, dob);
                    listener.jumpForward(v);
                }
            }
        });

        Button exit = (Button) view.findViewById(R.id.buttonBack1);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.jumpBack(v);
            }
        });

        Button submitButton = (Button) view.findViewById(R.id.button_submit);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(firstName == null || lastName == null || firstName.isEmpty() || lastName.isEmpty()){
                    Toast.makeText(getActivity(), R.string.demographics_check, Toast.LENGTH_SHORT).show();
                } else if(dob == null) {
                    Toast.makeText(getActivity(), R.string.dob_check, Toast.LENGTH_SHORT).show();
                }else {
                    listener.onSelectedDemographics(firstName, lastName, dob);
                }
            }
        });

        // submit initial triage levels to server
        listener.submitTriage();
        return view;



    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        listener = (demographicsListener) context;
    }

}
