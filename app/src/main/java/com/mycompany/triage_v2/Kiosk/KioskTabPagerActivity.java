package com.mycompany.triage_v2.Kiosk;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.mycompany.triage_v2.Helpers.BackgroundTask;
import com.mycompany.triage_v2.Helpers.Magic;
import com.mycompany.triage_v2.MainActivity;
import com.mycompany.triage_v2.Models.Action;
import com.mycompany.triage_v2.Models.Answer;
import com.mycompany.triage_v2.Models.AnswerEnum;
import com.mycompany.triage_v2.Models.Exposure;
import com.mycompany.triage_v2.Models.Patient;
import com.mycompany.triage_v2.Models.PatientMngr;
import com.mycompany.triage_v2.Models.ServerAnswer;
import com.mycompany.triage_v2.Models.Triage;
import com.mycompany.triage_v2.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class KioskTabPagerActivity extends AppCompatActivity implements
        DemographicsFragment.demographicsListener,
        PreliminaryQuesFragment.preliminaryListener,
        QuestionOneFragment.questionOneListener,
        QuestionTwoFragment.questionTwoListener,
        QuestionThreeFragment.questionThreeListener,
        QuestionFourFragment.questionFourListener,
        QuestionFiveFragment.questionFiveListener,
        VitalsKioskFragment.vitalsKioskListener,
        MapFragment.mapsListener,
        KioskOverview.KioskOverviewListener,
        KioskGoodbyeFragment.KioskGoodbyeListener{

    private static final String RESTART = "Restart";
    private static final String TAG = "trace";
    private static final String PATIENT_ID = "patientID";


    /***********************************************************************************************
     * controls
     */
    private static final String ADD_DEMOGRAPHICS = "add_demographics";
    private static final String ADD_QUESTION = "add_question";
    private static final String ADD_VITALS = "add_vitals";
    private static final String ADD_ADDRESS = "add_address";
    private static final String ADD_LATLONG = "add_lat_long";

    private static final String ADD_TRIAGE_LVL = "add_triage_lvl";

    private static final String KIOSK_EXIT = "kiosk_exit";

    private ViewPager mViewPager;

    //Patient object to record information
    private Patient patient;
    private String patient_id;

    private PatientMngr patientMngr;

    //answers
    Answer prelimAnswer;
    Answer qOne;
    Answer qTwo;
    Answer qThree;
    Answer qFour;
    Answer qFive;

    /***********************************************************************************************
     *starts new intent with patientId from BarCodeActivity
     */
    public static Intent newIntent(Context context, String patientId){
        Intent intent = new Intent(context, KioskTabPagerActivity.class);
        intent.putExtra(PATIENT_ID, patientId);
        return intent;
    }

    /***********************************************************************************************
     *creates activity
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kiosk_tab_pager);

        patientMngr = PatientMngr.get(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Create the adapter that will return a fragment for each of the
        // primary sections of the activity.
        SectionsPagerAdapter sectionsPagerAdapter =
                new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(sectionsPagerAdapter);

        // keyboard will be down when kiosk launches
        getWindow().setSoftInputMode((WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN));

        //get patientId and check it's not null
        patient_id = getIntent().getStringExtra(PATIENT_ID);
        if(patient_id != null){
            PatientMngr patientMngr = PatientMngr.get(this);
            patient = patientMngr.getPatient(patient_id);
        }

        if(Magic.isConnected(this)) Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();

    }

    /***********************************************************************************************
     *takes view from fragment and moves the page adapter forward to the next page
     */
    public void jumpForward(View view){
        if(view.getId() == R.id.button_submit_results) submit();
        getWindow().setSoftInputMode((WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN));
        mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1, true);
    }

    /***********************************************************************************************
     *takes view from fragment and moves the page adapter back to the previous page
     */
    public void jumpBack(View view){
        getWindow().setSoftInputMode((WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN));
        if(mViewPager.getCurrentItem() == 0){
            //send back to start of kiosk mode
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra(RESTART, R.id.buttonFinished);
            startActivity(intent);
        }
        mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1, true);
    }

    /***********************************************************************************************
     *takes view from fragment and returns to MainActivity with KioskWelcome loaded
     */
    public void onFinishedClicked() {
        // will check for connection within stampThis
        Magic.stampThis(this, KIOSK_EXIT, patient_id);

        //send back to start of kiosk mode
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(RESTART, R.id.buttonFinished);
        startActivity(intent);
    }

    /***********************************************************************************************
     * records demographics information from DemographicsFragment and sets them to patient
     */
    public void onSelectedDemographics(String first, String last, Date dob){

        if(Magic.isConnected(this)){
            java.sql.Date sqlDate = new java.sql.Date(dob.getTime());
            BackgroundTask backgroundTask = new BackgroundTask(this);
            backgroundTask.execute(ADD_DEMOGRAPHICS, patient_id, first, last, sqlDate.toString());
        } else {
            java.sql.Date sqlDate = new java.sql.Date(dob.getTime());
            patientMngr.storeData(Magic.storeDataMiddleMan(ADD_DEMOGRAPHICS,
                    patient_id, first, last, sqlDate.toString()));
            Log.i(TAG, "Data stored in queue - demographics (KioskTabPager)");
        }

        // write to sqlite
        patient.setFirstName(first);
        patient.setLastName(last);
        patient.setDob(dob);

    }

    /***********************************************************************************************
     * send initial triage levels to server if patient entered through the kiosk
     */
    public void submitTriage(){
        // will check for connection within onSelectedTriagelvl
        onSelectedTriagelvl(patient.getTriage(), patient.getExposureLvl(), patient.getAction());

    }

    /***********************************************************************************************
     * sets patient triage level and updates new patients have entered the system
     */
    public void onSelectedTriagelvl(Triage primary, Exposure exposure, Action action){

        if(Magic.isConnected(this)){
            BackgroundTask backgroundTask = new BackgroundTask(this);
            backgroundTask.execute(ADD_TRIAGE_LVL, patient_id,
                    primary.toString(), exposure.toString(), action.toString(),
                    Magic.stampThatFool());
        } else {
            patientMngr.storeData(Magic.storeDataMiddleMan(ADD_TRIAGE_LVL, patient_id,
                    primary.toString(), exposure.toString(), action.toString(),
                    Magic.stampThatFool()));
            Log.i(TAG, "Data stored in queue - triage levels (KioskTabPager)");
        }
    }


    /***********************************************************************************************
     * records patient answers and sends to the server
     */
    public void onSelectedAnswer(String question, boolean radio, boolean checkOne, boolean checkTwo,
                                 boolean checkThree, boolean checkFour,
                                 String preliminary, String timeStamp){
        if(Magic.isConnected(this)){
            BackgroundTask backgroundTask = new BackgroundTask(this);
            backgroundTask.execute(ADD_QUESTION, patient_id,
                    question, Magic.stringify(radio), Magic.stringify(checkOne), Magic.stringify(checkTwo),
                    Magic.stringify(checkThree), Magic.stringify(checkFour), preliminary, timeStamp);
        } else {
            patientMngr.storeData(Magic.storeDataMiddleMan(ADD_QUESTION, patient_id,
                    question, Magic.stringify(radio), Magic.stringify(checkOne), Magic.stringify(checkTwo),
                    Magic.stringify(checkThree), Magic.stringify(checkFour), preliminary, timeStamp));
            Log.i(TAG, "Data stored in queue - answer: " + question + " - (KioskTabPager)");
        }
    }

    /***********************************************************************************************
     * records patient vitals and sends to server
     */
    public void onSelectedVitals(int heartRate, int oxygen, int respiration, String timeStamp){
        if(Magic.isConnected(this)){
            BackgroundTask backgroundTask = new BackgroundTask(this);
            backgroundTask.execute(ADD_VITALS, patient_id, Integer.toString(heartRate),
                    Integer.toString(oxygen), Integer.toString(respiration), timeStamp);
        } else {
            patientMngr.storeData(Magic.storeDataMiddleMan(ADD_VITALS, patient_id,
                    Integer.toString(heartRate), Integer.toString(oxygen),
                    Integer.toString(respiration), timeStamp));
            Log.i(TAG, "Data stored in queue - Vitals (KioskTabPager)");
        }

        //write to sql
        patient.setOxygen(oxygen);
        patient.setHeart(heartRate);
    }

    /***********************************************************************************************
     * records patient timestamps for entering and exiting vitals fragment
     */
    public void vitalsTimestamp(String control){
        // will check for connection within stampThis
        Magic.stampThis(this, control, patient_id);
    }


    /***********************************************************************************************
     * records address and lat long information and sends to server
     */
    public void onSelectedAddress(String address, LatLng latLng){
        if(Magic.isConnected(this)){
            BackgroundTask backgroundTask = new BackgroundTask(this);
            backgroundTask.execute(ADD_ADDRESS, patient_id,
                    address, Double.toString(latLng.latitude), Double.toString(latLng.longitude));
        } else {
            patientMngr.storeData(Magic.storeDataMiddleMan(ADD_ADDRESS, patient_id,
                    address, Double.toString(latLng.latitude), Double.toString(latLng.longitude)));
            Log.i(TAG, "Data stored in queue - Address/latlng (KioskTabPager)");
        }

        //write to sqlite
        patient.setLatLng(latLng);
        patient.setAddress(address);
    }

    /***********************************************************************************************
     *records preliminary answer from PreliminaryQuesFragment and sets them to patient on sqlite db
     */
    public void onSelectedPreliminary(String answer){
        prelimAnswer = new ServerAnswer(answer, false, false, false, false, false);
        patient.putAnswer(AnswerEnum.PRELIM, prelimAnswer);
    }

    /***********************************************************************************************
     *records answers from QuestionOneFragment and sets them to patient on sqlite db
     */
    public void onSelectedQuestionOne(boolean radio, boolean checkOne, boolean checkTwo,
                                      boolean checkThree, boolean checkFour ){

        Answer one = new ServerAnswer("", radio, checkOne, checkTwo, checkThree, checkFour);
        patient.putAnswer(AnswerEnum.ONE, one);
    }

    /***********************************************************************************************
     *records answers from QuestionTwoFragment and sets them to patient on sqlite db
     */
    public void onSelectedQuestionTwo(boolean radio, boolean checkOne, boolean checkTwo,
                                      boolean checkThree, boolean checkFour ){
        Answer two = new ServerAnswer("", radio, checkOne, checkTwo, checkThree, checkFour);
        patient.putAnswer(AnswerEnum.TWO, two);
    }

    /***********************************************************************************************
     *records answer from QuestionThreeFragment and sets them to patient on sqlite db
     */
    public void onSelectedQuestionThree(boolean radio){
        Answer three = new ServerAnswer("", radio, false, false, false, false);
        patient.putAnswer(AnswerEnum.THREE, three);
    }

    /***********************************************************************************************
     *records answer from QuestionFourFragment and sets them to patient on sqlite db
     */
    public void onSelectedQuestionFour(boolean radio){
        Answer four = new ServerAnswer("", radio, false, false, false, false);
        patient.putAnswer(AnswerEnum.FOUR, four);
    }

    /***********************************************************************************************
     *records answer from QuestionFiveFragment and sets them to patient on sqlite db
     */
    public void onSelectedQuestionFive(boolean radio){
        Answer five = new ServerAnswer("", radio, false, false, false, false);
        patient.putAnswer(AnswerEnum.FIVE, five);
    }

    /***********************************************************************************************
     * updates database with new patient information
     */
    public void submit(){
        patient.setCompletedKiosk(true);
        PatientMngr patientMngr = PatientMngr.get(this);
        patientMngr.updatePatient(patient);
    }

    /***********************************************************************************************
     *
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /***********************************************************************************************
     *
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /***********************************************************************************************
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        private  int fragmentCount = 11;

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            int questions = fragmentCount - 1;
            String title;
            switch (position){
                case 0:
                    title = position + 1 + " of " + questions;
                    return DemographicsFragment.newInstance(title);
                case 1:
                    title = position + 1 + " of " + questions;
                    return PreliminaryQuesFragment.newInstance(title);
                case 2:
                    title = position + 1 + " of " + questions;
                    return QuestionOneFragment.newInstance(title);
                case 3:
                    title = position + 1 + " of " + questions;
                    return QuestionTwoFragment.newInstance(title);
                case 4:
                    title = position + 1 + " of " + questions;
                    return QuestionThreeFragment.newInstance(title);
                case 5:
                    title = position + 1 + " of " + questions;
                    return QuestionFourFragment.newInstance(title);
                case 6:
                    title = position + 1 + " of " + questions;
                    return QuestionFiveFragment.newInstance(title);
                case 7:
                    title = position + 1 + " of " + questions;
                    return VitalsKioskFragment.newInstance(title);
                case 8:
                    return MapFragment.newInstance(patient.fullName());
                case 9:
                    title = position + 1 + " of " + questions;
                    return KioskOverview.newInstance(patient.getTitle(), title,
                            patient.getAnswer(AnswerEnum.PRELIM).getPrelimAns(),
                            patient.getAnswer(AnswerEnum.ONE).getCheckable(),
                            patient.getAnswer(AnswerEnum.TWO).getCheckable(),
                            patient.getAnswer(AnswerEnum.THREE).getCheckable(),
                            patient.getAnswer(AnswerEnum.FOUR).getCheckable(),
                            patient.getAnswer(AnswerEnum.FIVE).getCheckable());
                case 10:
                    return KioskGoodbyeFragment.newInstance();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return fragmentCount;
        }

    }

}
