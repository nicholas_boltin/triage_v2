package com.mycompany.triage_v2.Kiosk;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.mycompany.triage_v2.Helpers.Magic;
import com.mycompany.triage_v2.R;

public class QuestionThreeFragment extends Fragment {

    private static final String PROGRESS_TITLE = "progress_title";
    private static final String QUESTION_NUMBER = "3";

    private RadioGroup radioGroup;
    private RadioButton yes;
    private RadioButton no;

    private TextView progress;

    private boolean yesNo = false;

    /***********************************************************************************************
     * listener and listener interface to call methods in activity
     */
    questionThreeListener listener;

    public interface questionThreeListener{
        void onSelectedQuestionThree(boolean radio);
        void onSelectedAnswer(String question, boolean radio, boolean checkOne, boolean checkTwo,
                              boolean checkThree, boolean checkFour,
                              String preliminary, String timestamp);
        void jumpForward(View view);
        void jumpBack(View view);
    }

    /***********************************************************************************************
     * required empty constructor
     */
    public QuestionThreeFragment() {}

    /***********************************************************************************************
     *starts new instance of fragment, called from KioskTabPagerActivity
     */
    public static QuestionThreeFragment newInstance(String title) {
        QuestionThreeFragment fragment = new QuestionThreeFragment();
        Bundle bundle = new Bundle();
        bundle.putString(PROGRESS_TITLE, title);
        fragment.setArguments(bundle);
        return fragment;
    }

    /***********************************************************************************************
     * creates view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_question_three, container, false);

        progress = (TextView) view.findViewById(R.id.textview_progress);
        progress.setText(getArguments().getString(PROGRESS_TITLE));

        radioGroup = (RadioGroup) view.findViewById(R.id.radio_group_question_three);
        yes = (RadioButton) view.findViewById(R.id.radio_q3yes);
        no = (RadioButton) view.findViewById(R.id.radio_q3no);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.radio_q3yes:
                        yesNo = true;
                        break;
                    case R.id.radio_q3no:
                        yesNo = false;
                        break;
                }
            }
        });

        Button next = (Button) view.findViewById(R.id.buttonNext);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // check if any answer was given, prompt for a response if no answer given
                if(!yes.isChecked() && !no.isChecked()){
                    Toast.makeText(getActivity(), R.string.response, Toast.LENGTH_SHORT).show();
                }else{
                    listener.onSelectedQuestionThree(yesNo);
                    listener.onSelectedAnswer(QUESTION_NUMBER, yesNo, false, false,
                            false, false, "", Magic.stampThatFool());
                    listener.jumpForward(v);
                }
            }
        });

        Button prev = (Button) view.findViewById(R.id.buttonBack);
        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.jumpBack(v);
            }
        });
        return view;
    }

    /***********************************************************************************************
     * attach listener
     */
    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        listener = (questionThreeListener) context;
    }

}
