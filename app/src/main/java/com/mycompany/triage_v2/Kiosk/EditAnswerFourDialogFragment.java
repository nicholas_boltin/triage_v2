package com.mycompany.triage_v2.Kiosk;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.mycompany.triage_v2.R;

public class EditAnswerFourDialogFragment extends DialogFragment{

    private static final String INTENT_YESNO = "yes_no";

    private RadioGroup radioGroup;
    private RadioButton yes;
    private RadioButton no;

    private boolean yesNo = false;


    public static EditAnswerFourDialogFragment newInstance(){
        return new EditAnswerFourDialogFragment();
    }

    public EditAnswerFourDialogFragment(){}

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_edit_answer_four, null);

        radioGroup = (RadioGroup) view.findViewById(R.id.radio_group_question_four);
        yes = (RadioButton) view.findViewById(R.id.radio_q4yes);
        no = (RadioButton) view.findViewById(R.id.radio_q4no);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.radio_q4yes:
                        yesNo = true;
                        break;
                    case R.id.radio_q4no:
                        yesNo = false;
                        break;
                }
            }
        });

        return new AlertDialog.Builder(getActivity())
                .setView(view)
                .setTitle(R.string.toxidrome_question_04)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                        if(!yes.isChecked() && !no.isChecked()){
                            // TODO: 7/13/2016 figure out how to start toast and not close the dialog - DVJ
                            //Toast.makeText(getActivity(), R.string.select_response, Toast.LENGTH_SHORT).show();
                        }else{
                            sendResult(Activity.RESULT_OK, yesNo);
                        }
                    }
                }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which){

                    }
                }).create();
    }

    private void sendResult(int resultCode, boolean radio){
        if(getTargetFragment() == null){
            return;
        }
        Intent intent = new Intent();
        intent.putExtra(INTENT_YESNO, radio);

        getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, intent);
    }

}
