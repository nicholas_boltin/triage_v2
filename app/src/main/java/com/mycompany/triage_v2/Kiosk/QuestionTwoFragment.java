package com.mycompany.triage_v2.Kiosk;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.mycompany.triage_v2.Helpers.Magic;
import com.mycompany.triage_v2.R;

public class QuestionTwoFragment extends Fragment {

    private static final String PROGRESS_TITLE = "progress_title";
    private static final String QUESTION_NUMBER = "2";

    private RadioGroup radioGroup;
    private RadioButton yes;
    private RadioButton no;
    private CheckBox checkBoxOne;
    private CheckBox checkBoxTwo;
    private CheckBox checkBoxThree;
    private CheckBox checkBoxFour;

    private TextView progress;

    private boolean yesNo, checkOne,checkTwo, checkThree, checkFour = false;

    /***********************************************************************************************
     * listener and listener interface to call methods in activity
     */
    questionTwoListener listener;

    public interface questionTwoListener{
        void onSelectedQuestionTwo(boolean radio, boolean checkOne, boolean checkTwo,
                                   boolean checkThree, boolean checkFour );
        void onSelectedAnswer(String question, boolean radio, boolean checkOne, boolean checkTwo,
                              boolean checkThree, boolean checkFour,
                              String preliminary, String timestamp);
        void jumpForward(View view);
        void jumpBack(View view);
    }

    /***********************************************************************************************
     * required empty constructor
     */
    public QuestionTwoFragment() {}

    /***********************************************************************************************
     *starts new instance of fragment, called from KioskTabPagerActivity
     */
    public static QuestionTwoFragment newInstance(String title) {
        QuestionTwoFragment fragment = new QuestionTwoFragment();
        Bundle bundle = new Bundle();
        bundle.putString(PROGRESS_TITLE, title);
        fragment.setArguments(bundle);
        return fragment;
    }

    /***********************************************************************************************
     * creates view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_question_two, container, false);

        progress = (TextView) view.findViewById(R.id.textview_progress);
        progress.setText(getArguments().getString(PROGRESS_TITLE));

        radioGroup = (RadioGroup) view.findViewById(R.id.radio_group_question_two);
        yes = (RadioButton) view.findViewById(R.id.radio_q2yes);
        no = (RadioButton) view.findViewById(R.id.radio_q2no);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.radio_q2yes:
                        yesNo = true;
                        checkBoxOne.setEnabled(true);
                        checkBoxTwo.setEnabled(true);
                        checkBoxThree.setEnabled(true);
                        //checkBoxFour.setEnabled(true);
                        break;
                    case R.id.radio_q2no:
                        yesNo = false;
                        checkBoxOne.setEnabled(false);
                        checkBoxTwo.setEnabled(false);
                        checkBoxThree.setEnabled(false);
                       // checkBoxFour.setEnabled(false);

                        //reset checkboxes and answers on no press
                        if(checkBoxOne.isChecked()) checkBoxOne.setChecked(false);
                        if(checkBoxTwo.isChecked()) checkBoxTwo.setChecked(false);
                        if(checkBoxThree.isChecked()) checkBoxThree.setChecked(false);
                      //  if(checkBoxFour.isChecked()) checkBoxFour.setChecked(false);
                        checkOne = checkTwo = checkThree = checkFour = false;
                        break;
                }
            }
        });

        checkBoxOne = (CheckBox) view.findViewById(R.id.checkbox_two_q1);
        checkBoxTwo = (CheckBox) view.findViewById(R.id.checkbox_two_q2);
        checkBoxThree = (CheckBox) view.findViewById(R.id.checkbox_two_q3);
       // checkBoxFour = (CheckBox) view.findViewById(R.id.checkbox_two_q4);

        checkBoxOne.setEnabled(false);
        checkBoxTwo.setEnabled(false);
        checkBoxThree.setEnabled(false);
       // checkBoxFour.setEnabled(false);

        checkBoxOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(((CheckBox)v).isChecked()){
                    checkOne = true;
                }else{
                    checkOne = false;
                }
            }
        });

        checkBoxTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(((CheckBox)v).isChecked()){
                    checkTwo = true;
                }else{
                    checkTwo = false;
                }
            }
        });

        checkBoxThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(((CheckBox)v).isChecked()){
                    checkThree = true;
                }else{
                    checkThree = false;
                }
            }
        });

        Button next = (Button) view.findViewById(R.id.buttonNext);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!yes.isChecked() && !no.isChecked()){
                    Toast.makeText(getActivity(), R.string.response, Toast.LENGTH_SHORT).show();
                }else{
                    listener.onSelectedQuestionTwo(yesNo, checkOne, checkTwo, checkThree, checkFour);
                    listener.onSelectedAnswer(QUESTION_NUMBER, yesNo, checkOne, checkTwo,
                            checkThree, checkFour, "", Magic.stampThatFool());
                    listener.jumpForward(v);
                }
            }
        });

        Button prev = (Button) view.findViewById(R.id.buttonBack);
        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.jumpBack(v);
            }
        });
        return view;
    }

    /***********************************************************************************************
     * attach listener
     */
    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        listener = (questionTwoListener) context;
    }

}
