package com.mycompany.triage_v2.Kiosk;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.mycompany.triage_v2.R;

public class EditAnswerTwoDialogFragment extends DialogFragment{

    private static final String INTENT_YESNO = "yes_no";
    private static final String INTENT_CHECK_ONE = "check_one";
    private static final String INTENT_CHECK_TWO = "check_two";
    private static final String INTENT_CHECK_THREE = "check_three";

    private RadioGroup radioGroup;
    private RadioButton yes;
    private RadioButton no;
    private CheckBox checkBoxOne;
    private CheckBox checkBoxTwo;
    private CheckBox checkBoxThree;


    private boolean yesNo, checkOne,checkTwo, checkThree = false;

    public static EditAnswerTwoDialogFragment newInstance(){
        return new EditAnswerTwoDialogFragment();
    }

    public EditAnswerTwoDialogFragment(){}


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_edit_answer_two, null);

        radioGroup = (RadioGroup) view.findViewById(R.id.radio_group_question_two);
        yes = (RadioButton) view.findViewById(R.id.radio_q2yes);
        no = (RadioButton) view.findViewById(R.id.radio_q2no);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.radio_q2yes:
                        yesNo = true;
                        checkBoxOne.setEnabled(true);
                        checkBoxTwo.setEnabled(true);
                        checkBoxThree.setEnabled(true);
                        break;
                    case R.id.radio_q2no:
                        yesNo = false;
                        checkBoxOne.setEnabled(false);
                        checkBoxTwo.setEnabled(false);
                        checkBoxThree.setEnabled(false);
                        if(checkBoxOne.isChecked()) checkBoxOne.setChecked(false);
                        if(checkBoxTwo.isChecked()) checkBoxTwo.setChecked(false);
                        if(checkBoxThree.isChecked()) checkBoxThree.setChecked(false);
                        checkOne = checkTwo = checkThree  = false;
                        break;
                }
            }
        });

        checkBoxOne = (CheckBox) view.findViewById(R.id.checkbox_two_q1);
        checkBoxTwo = (CheckBox) view.findViewById(R.id.checkbox_two_q2);
        checkBoxThree = (CheckBox) view.findViewById(R.id.checkbox_two_q3);

        checkBoxOne.setEnabled(false);
        checkBoxTwo.setEnabled(false);
        checkBoxThree.setEnabled(false);

        checkBoxOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(((CheckBox)v).isChecked()){
                    checkOne = true;
                }else{
                    checkOne = false;
                }
            }
        });

        checkBoxTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(((CheckBox)v).isChecked()){
                    checkTwo = true;
                }else{
                    checkTwo = false;
                }
            }
        });

        checkBoxThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(((CheckBox)v).isChecked()){
                    checkThree = true;
                }else{
                    checkThree = false;
                }
            }
        });



        return new AlertDialog.Builder(getActivity())
                .setView(view)
                .setTitle(R.string.toxidrome_question_02)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                        if(!yes.isChecked() && !no.isChecked()){
                            // TODO: 7/13/2016 figure out how to start toast and not close the dialog - DVJ
                            //Toast.makeText(getActivity(), R.string.select_response, Toast.LENGTH_SHORT).show();
                        }else{
                            sendResult(Activity.RESULT_OK, yesNo, checkOne, checkTwo, checkThree);
                        }
                    }
                }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which){

                    }
                }).create();
    }

    private void sendResult(int resultCode, boolean radio, boolean checkOne, boolean checkTwo,
                            boolean checkThree){
        if(getTargetFragment() == null){
            return;
        }
        Intent intent = new Intent();
        intent.putExtra(INTENT_YESNO, radio);
        intent.putExtra(INTENT_CHECK_ONE, checkOne);
        intent.putExtra(INTENT_CHECK_TWO, checkTwo);
        intent.putExtra(INTENT_CHECK_THREE, checkThree);

        getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, intent);
    }


}
