package com.mycompany.triage_v2.Kiosk;

import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.mycompany.triage_v2.R;
import com.mycompany.triage_v2.Views.BarCodeActivity;

public class KioskWelcome extends Fragment {

    private static final String BUNDLE_ID = "R.id";
    private int destinationID = 0;

    Button acceptButton;

    public static KioskWelcome newInstance(int id){
        KioskWelcome fragment = new KioskWelcome();
        Bundle bundle = new Bundle();
        bundle.putInt(BUNDLE_ID, id);
        fragment.setArguments(bundle);
        return fragment;
    }

    public KioskWelcome() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        destinationID = getArguments().getInt(BUNDLE_ID, 0);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
         View view = inflater.inflate(R.layout.kiosk_welcome, container, false);

        acceptButton = (Button) view.findViewById(R.id.buttonAccept);
        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(BarCodeActivity.newIntent(getActivity(), destinationID));
            }
        });

        return view;
    }



}
