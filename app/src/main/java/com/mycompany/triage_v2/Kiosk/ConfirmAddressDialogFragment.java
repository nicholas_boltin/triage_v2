package com.mycompany.triage_v2.Kiosk;


import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.mycompany.triage_v2.R;

import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 */
public class ConfirmAddressDialogFragment extends DialogFragment {

    private static final String BUNDLE_ADDRESS = "address";
    private static final String BUNDLE_PATIENT_NAME = "patient_first";
    private static final String BUNDLE_LAT = "lat";
    private static final String BUNDLE_LNG = "lng";
    private static final String INTENT_ADDRESS = "addressIntent";
    private static final String INTENT_PATIENT = "patientIntent";
    private static final String INTENT_LAT = "latIntent";
    private static final String INTENT_LNG = "lngIntent";

    String addressT;
    double latT;
    double lngT;


    /***********************************************************************************************
     * creates new instance of dialog fragment for display
     *
     * @param address   string address google maps returns to be confirmed
     * @param latLng    latlng associated with string address
     * @param name      name of the patient in the kiosk
     * @return          instance of dialog fragment
     */
    public static ConfirmAddressDialogFragment newInstance(String address, LatLng latLng, String name){
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_ADDRESS, address);
        bundle.putDouble(BUNDLE_LAT, latLng.latitude);
        bundle.putDouble(BUNDLE_LNG, latLng.longitude);
        bundle.putString(BUNDLE_PATIENT_NAME, name);
        ConfirmAddressDialogFragment fragment = new ConfirmAddressDialogFragment();
        fragment.setArguments(bundle);
        return fragment;
    }


    public ConfirmAddressDialogFragment() {}

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_confirm_address_dialog, null);

        TextView address = (TextView) view.findViewById(R.id.textview_address);
        addressT = getArguments().getString(BUNDLE_ADDRESS);
        address.setText(addressT);

//        TextView latlng = (TextView) view.findViewById(R.id.textview_latlng);
        latT = getArguments().getDouble(BUNDLE_LAT, 0);
        lngT = getArguments().getDouble(BUNDLE_LNG, 0);
        String name = getArguments().getString(BUNDLE_PATIENT_NAME);
        final LatLng latLng = new LatLng(latT, lngT);
//        latlng.setText(latLng.stringify());
        String header = getResources().getString(R.string.dialog_address_confirm);

        return new AlertDialog.Builder(getActivity())
                .setView(view)
                .setTitle(name + ", " + header)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which){
                sendResult(Activity.RESULT_OK,addressT,latLng);
                Toast.makeText(getActivity(), R.string.dialog_address_submitted, Toast.LENGTH_SHORT).show();
            }
        }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which){

            }
        }).create();
    }

    /***********************************************************************************************
     * sends results of confirmation back to MapFragment
     *
     * @param resultCode    the result of transaction
     * @param address       the string address patient is confirming
     * @param latLng        the latlng associated with the string
     */
    private void sendResult(int resultCode, String address, LatLng latLng){
        if(getTargetFragment() == null){
            return;
        }
        Intent intent = new Intent();
        intent.putExtra(INTENT_ADDRESS, address);
        double x = latLng.latitude;
        double y = latLng.longitude;
        intent.putExtra(INTENT_LAT, x);
        intent.putExtra(INTENT_LNG, y);
        getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, intent);
    }
}
