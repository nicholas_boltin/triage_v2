package com.mycompany.triage_v2.Kiosk;


import android.content.Context;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.mycompany.triage_v2.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class KioskGoodbyeFragment extends Fragment {

    //control for restarting kiosk
    private static final String RESTART = "Restart";

    private Button buttonFinished;

    /***********************************************************************************************
     * interface listener sends corrected Answers to KioskTabPagerActivity for update
     */
    KioskGoodbyeListener listener;

    public interface KioskGoodbyeListener{
        void onFinishedClicked();
    }

    /***********************************************************************************************
     *required empty constructor
     */
    public KioskGoodbyeFragment() {}

    /***********************************************************************************************
     * creates new instance of KioskGoodbye fragment. Called from KioskTabPagerActivity
     */
    public static KioskGoodbyeFragment newInstance() {
        KioskGoodbyeFragment fragment = new KioskGoodbyeFragment();
        return fragment;
    }

    /***********************************************************************************************
     * creates fragment layout
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_kiosk_goodbye, container, false);

        buttonFinished = (Button) view.findViewById(R.id.buttonFinished);
        buttonFinished.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onFinishedClicked();
            }
        });

        return view;
    }


    /***********************************************************************************************
     * attaches listener
     */
    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        listener = (KioskGoodbyeFragment.KioskGoodbyeListener) context;
    }
}
