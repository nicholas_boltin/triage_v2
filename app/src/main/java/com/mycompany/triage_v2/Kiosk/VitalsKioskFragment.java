package com.mycompany.triage_v2.Kiosk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.mycompany.triage_v2.Helpers.Magic;
import com.mycompany.triage_v2.Models.UniversalDialogFragment;
import com.mycompany.triage_v2.R;
import com.mycompany.triage_v2.SuperNurse.ConfirmExitDialogFragment;

public class VitalsKioskFragment extends Fragment {

    private static final String TAG = "trace";

    private static final String PROGRESS_TITLE = "progress_title";
    private static final String DIALOG_CONFIRM = "dialog_confirm";
    private static final int REQUEST_OXYGEN = 100;
    private static final int REQUEST_HEART = 200;

    //controls
    private static final String VITALS_START = "vitals_start";
    private static final String VITALS_EXIT = "vitals_end";

    private TextView progress;

    private NumberPicker oxygenNP;
    private NumberPicker heartNP;

    //min/max values for numberpickers
    private int oxygenMin = 40;
    private int oxygenMax = 100;

    private int heartMin = 0;
    private int heartMax = 200;
    private int setValue = 99;

    private int oxygen = -1;
    private int heartR = -1;
    private int respiration = 0;

    /***********************************************************************************************
     * listener and listener interface to call methods in activity
     */
    private vitalsKioskListener listener;

    public interface vitalsKioskListener{
        void jumpForward(View view);
        void jumpBack(View view);

        void onSelectedVitals(int heartRate, int oxygen, int respiration, String timeStamp);
        void vitalsTimestamp(String control);
    }

    /***********************************************************************************************
     * required empty constructor
     */
    public VitalsKioskFragment() {}

    /***********************************************************************************************
     *starts new instance of fragment, called from KioskTabPagerActivity
     */
    public static VitalsKioskFragment newInstance(String title) {
        VitalsKioskFragment fragment = new VitalsKioskFragment();
        Bundle bundle = new Bundle();
        bundle.putString(PROGRESS_TITLE, title);
        fragment.setArguments(bundle);
        return fragment;
    }

    /***********************************************************************************************
     * creates view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vitals_kiosk, container, false);

        progress = (TextView) view.findViewById(R.id.textview_progress);
        progress.setText(getArguments().getString(PROGRESS_TITLE));

        //set up numberPicker
        oxygenNP = (NumberPicker) view.findViewById(R.id.number_picker_o2);
        oxygenNP.setMinValue(oxygenMin);
        oxygenNP.setMaxValue(oxygenMax);
        oxygenNP.setWrapSelectorWheel(false);
        oxygenNP.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                oxygen = newVal;
            }
        });
        oxygenNP.setValue(setValue);

        //set up numberPicker
        heartNP = (NumberPicker) view.findViewById(R.id.number_picker_hr);
        heartNP.setMinValue(heartMin);
        heartNP.setMaxValue(heartMax);
        heartNP.setWrapSelectorWheel(false);
        heartNP.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                heartR = newVal;
            }
        });
        heartNP.setValue(setValue);

        Button noOxygen = (Button) view.findViewById(R.id.noSpo2);
        noOxygen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fragmentManager = getFragmentManager();
                DialogFragment dialogFragment = UniversalDialogFragment.newInstance(R.id.noSpo2);
                dialogFragment.setTargetFragment(VitalsKioskFragment.this, REQUEST_OXYGEN);
                dialogFragment.show(fragmentManager, DIALOG_CONFIRM);
            }
        });

        Button noHeart = (Button) view.findViewById(R.id.noPulse);
        noHeart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fragmentManager = getFragmentManager();
                DialogFragment dialogFragment = UniversalDialogFragment.newInstance(R.id.noPulse);
                dialogFragment.setTargetFragment(VitalsKioskFragment.this, REQUEST_HEART);
                dialogFragment.show(fragmentManager, DIALOG_CONFIRM);
            }
        });

        Button next = (Button) view.findViewById(R.id.buttonNext);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // check if any answer was given, prompt for a response if no answer given
                if(oxygen > -1 && heartR > -1){
                    listener.onSelectedVitals(heartR, oxygen, respiration, Magic.stampThatFool());
                    listener.vitalsTimestamp(VITALS_EXIT);
                    listener.jumpForward(v);
                }
            }
        });

        Button prev = (Button) view.findViewById(R.id.buttonBack);
        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.jumpBack(v);
            }
        });

        return view;
    }

    /***********************************************************************************************
     * attach listener
     */
    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        listener = (vitalsKioskListener) context;
    }

    /***********************************************************************************************
     *
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode != Activity.RESULT_OK){
            return;
        }
        switch (requestCode){
            case REQUEST_OXYGEN:
                oxygen = 0;

                break;
            case REQUEST_HEART:
                heartR = 0;

                break;
            default:
                return;
        }
    }
}
