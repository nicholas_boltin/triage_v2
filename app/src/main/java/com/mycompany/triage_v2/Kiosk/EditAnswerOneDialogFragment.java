package com.mycompany.triage_v2.Kiosk;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.mycompany.triage_v2.R;

public class EditAnswerOneDialogFragment extends DialogFragment {

    private static final String INTENT_YESNO = "yes_no";
    private static final String INTENT_CHECK_ONE = "check_one";
    private static final String INTENT_CHECK_TWO = "check_two";
    private static final String INTENT_CHECK_THREE = "check_three";
    private static final String INTENT_CHECK_FOUR = "check_four";

    private RadioGroup radioGroup;
    private RadioButton yes;
    private RadioButton no;
    private CheckBox checkBoxOne;
    private CheckBox checkBoxTwo;
    private CheckBox checkBoxThree;
    private CheckBox checkBoxFour;

    private boolean yesNo, checkOne,checkTwo, checkThree, checkFour = false;


    public static EditAnswerOneDialogFragment newInstance(){
        return new EditAnswerOneDialogFragment();
    }

    public EditAnswerOneDialogFragment(){}

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_edit_answer_one, null);

        radioGroup = (RadioGroup) view.findViewById(R.id.radio_group_question_one);
        yes = (RadioButton) view.findViewById(R.id.radio_q1yes);
        no = (RadioButton) view.findViewById(R.id.radio_q1no);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.radio_q1yes:
                        yesNo = true;
                        checkBoxOne.setEnabled(true);
                        checkBoxTwo.setEnabled(true);
                        checkBoxThree.setEnabled(true);
                        checkBoxFour.setEnabled(true);
                        break;
                    case R.id.radio_q1no:
                        yesNo = false;
                        checkBoxOne.setEnabled(false);
                        checkBoxTwo.setEnabled(false);
                        checkBoxThree.setEnabled(false);
                        checkBoxFour.setEnabled(false);
                        if(checkBoxOne.isChecked()) checkBoxOne.setChecked(false);
                        if(checkBoxTwo.isChecked()) checkBoxTwo.setChecked(false);
                        if(checkBoxThree.isChecked()) checkBoxThree.setChecked(false);
                        if(checkBoxFour.isChecked()) checkBoxFour.setChecked(false);
                        checkOne = checkTwo = checkThree = checkFour = false;
                        break;
                }
            }
        });

        checkBoxOne = (CheckBox) view.findViewById(R.id.checkbox_one_q1);
        checkBoxTwo = (CheckBox) view.findViewById(R.id.checkbox_one_q2);
        checkBoxThree = (CheckBox) view.findViewById(R.id.checkbox_one_q3);
        checkBoxFour = (CheckBox) view.findViewById(R.id.checkbox_one_q4);

        checkBoxOne.setEnabled(false);
        checkBoxTwo.setEnabled(false);
        checkBoxThree.setEnabled(false);
        checkBoxFour.setEnabled(false);

        checkBoxOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(((CheckBox)v).isChecked()){
                    checkOne = true;
                }else{
                    checkOne = false;
                }
            }
        });

        checkBoxTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(((CheckBox)v).isChecked()){
                    checkTwo = true;
                }else{
                    checkTwo = false;
                }
            }
        });

        checkBoxThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(((CheckBox)v).isChecked()){
                    checkThree = true;
                }else{
                    checkThree = false;
                }
            }
        });

        checkBoxFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(((CheckBox)v).isChecked()){
                    checkFour = true;
                }else{
                    checkFour = false;
                }
            }
        });

        return new AlertDialog.Builder(getActivity())
                .setView(view)
                .setTitle(R.string.toxidrome_question_01)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                        if(!yes.isChecked() && !no.isChecked()){
                            // TODO: 7/13/2016 figure out how to start toast and not close the dialog - DVJ
                            //Toast.makeText(getActivity(), R.string.select_response, Toast.LENGTH_SHORT).show();
                        }else{
                            sendResult(Activity.RESULT_OK, yesNo, checkOne, checkTwo, checkThree, checkFour);
                        }
                    }
                }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which){

                    }
                }).create();
    }



    private void sendResult(int resultCode, boolean radio, boolean checkOne, boolean checkTwo,
                            boolean checkThree, boolean checkFour){
        if(getTargetFragment() == null){
            return;
        }
        Intent intent = new Intent();
        intent.putExtra(INTENT_YESNO, radio);
        intent.putExtra(INTENT_CHECK_ONE, checkOne);
        intent.putExtra(INTENT_CHECK_TWO, checkTwo);
        intent.putExtra(INTENT_CHECK_THREE, checkThree);
        intent.putExtra(INTENT_CHECK_FOUR, checkFour);

        getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, intent);
    }

}
