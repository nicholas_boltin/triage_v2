package com.mycompany.triage_v2.Views;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mycompany.triage_v2.R;

//import butterknife.ButterKnife;

public class FragmentError extends Fragment {

    View mView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_error, container, false);
        //ButterKnife.bind(this, mView);
        return mView;
    }
}
