package com.mycompany.triage_v2.Views;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mycompany.triage_v2.Helpers.BackgroundTask;
import com.mycompany.triage_v2.Helpers.Magic;
import com.mycompany.triage_v2.Kiosk.KioskTabPagerActivity;
import com.mycompany.triage_v2.Models.Patient;
import com.mycompany.triage_v2.Models.PatientMngr;
import com.mycompany.triage_v2.R;
import com.mycompany.triage_v2.SuperNurse.SuperNursePagerActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/***************************************************************************************************
 * displays patient search results
 */
public class SearchResultsFragment extends DialogFragment {

    //private static final String TAG = "trace";

    private static final String BUNDLE_FULLNEAME = "full_name";
    private static final String BUNDLE_ID = "R.id";

    //server controls
    private static final String PRIMARY_ENTRY = "primary_entry";
    private static final String KIOSK_ENTRY = "kiosk_entry";
    private static final String SECONDARY_ENTRY = "secondary_entry";
    private static final String RESTART = "Restart";

    private int destinationId;

    private List<Patient> results;

    /***********************************************************************************************
     * creates new instance of fragment
     * fullName is the string that will be searched
     * destinationID used to determine which nurse mode to run
     */
    public static SearchResultsFragment newInstance(String fullName, int destinationId){
        Bundle bundle = new Bundle();

        bundle.putString(BUNDLE_FULLNEAME, fullName);
        bundle.putInt(BUNDLE_ID, destinationId);

        SearchResultsFragment fragment = new SearchResultsFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    /***********************************************************************************************
     * required empty constructor
     */
    public SearchResultsFragment(){}

    /***********************************************************************************************
     * create dialog view
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_search_results, null);

        destinationId = getArguments().getInt(BUNDLE_ID);

        getSearchResults(getArguments().getString(BUNDLE_FULLNEAME));

        TextView emptySet = (TextView) view.findViewById(R.id.empty_view);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.dialog_search_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        PatientAdapter adapter = new PatientAdapter(results);
        recyclerView.setAdapter(adapter);

        if (results.isEmpty()) {
            recyclerView.setVisibility(View.GONE);
            emptySet.setVisibility(View.VISIBLE);
        }
        else {
            recyclerView.setVisibility(View.VISIBLE);
            emptySet.setVisibility(View.GONE);
        }

        return  new AlertDialog.Builder(getActivity())
                .setView(view)
                .setTitle(R.string.search_results)
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which){}
                }).create();
    }

    private void getSearchResults(String fullName){
        PatientMngr patientMngr = PatientMngr.get(getActivity());
        List<Patient> patientList = patientMngr.getPatientList();
        results = new ArrayList<>();

        Scanner scanner = new Scanner(fullName);
        while(scanner.hasNext()){
            String name = scanner.next();
            name = purify(name);
            for(int i = 0; i < patientList.size(); i++){
                if(name.equalsIgnoreCase(patientList.get(i).getFirstName())){
                    results.add(patientList.get(i));
                }
                if(name.equalsIgnoreCase(patientList.get(i).getLastName())){
                    results.add(patientList.get(i));
                }
                if(name.equalsIgnoreCase(patientList.get(i).getPatientId())){
                    results.add(patientList.get(i));
                }
            }
        }
        scanner.close();
        patientMngr.checkForDoubles(results);
    }

    private String purify(String name){
        name = name.replaceAll("\\.", "");
        name = name.replaceAll(",", "");
        name = name.replaceAll("!", "");
        name = name.replaceAll("\\?", "");
        return name;
    }

    //class to hold patients in view for selection
    public class PatientHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView address;
        private TextView patientText;
        private TextView patientId;
        private TextView triageLvl, exposure, action;
        private Patient patientObject;

        public PatientHolder(View view){
            super(view);
            view.setOnClickListener(this);
            address = (TextView) view.findViewById(R.id.textview_address);
            patientText = (TextView) view.findViewById(R.id.textview_patient);
            patientId = (TextView) view.findViewById(R.id.textview_patient_id_number);
            triageLvl = (TextView) view.findViewById(R.id.triage_color_box);
            exposure = (TextView) view.findViewById(R.id.exposure_color_box);
            action = (TextView) view.findViewById(R.id.action_color_box);
        }

        public void bindPatient(Patient patient){
            patientObject = patient;
            address.setText(patient.getAddress());
            patientText.setText(patient.fullName());
            patientId.setText(patient.getPatientId());

            switch (patientObject.getTriage()){
                case CRITICAL:
                    triageLvl.setBackgroundColor(Color.parseColor("#D32F2F")); //Red
                    triageLvl.setText(R.string.blank);
                    break;
                case NONCRITICAL:
                    triageLvl.setBackgroundColor(Color.parseColor("#8BC34A")); //Green
                    triageLvl.setText(R.string.blank);
                    break;
                case NONE:
                    triageLvl.setBackgroundColor(Color.WHITE);
                    triageLvl.setText(R.string.na);
                    break;
            }

            switch (patientObject.getExposureLvl()){
                case EXPOSED:
                    exposure.setBackgroundColor(Color.parseColor("#F57C00")); //Orange
                    exposure.setText(R.string.blank);
                    break;
                case NOTEXPOSED:
                    exposure.setBackgroundColor(Color.parseColor("#8BC34A")); //Green
                    exposure.setText(R.string.blank);
                    break;
                case POTENTIALEXPOSURE:
                    exposure.setBackgroundColor(Color.parseColor("#FFEB3B")); //Yellow
                    exposure.setText(R.string.blank);
                    break;
                case NONE:
                    exposure.setBackgroundColor(Color.WHITE);
                    exposure.setText(R.string.na);
                    break;
            }

            switch (patientObject.getAction()){
                case URGENT:
                    action.setBackgroundColor(Color.parseColor("#D32F2F")); //Red
                    action.setText(R.string.blank);
                    break;
                case MONITOR:
                    action.setBackgroundColor(Color.parseColor("#FFEB3B")); //Yellow
                    action.setText(R.string.blank);
                    break;
                case EXIT:
                    action.setBackgroundColor(Color.GRAY);
                    action.setText(R.string.blank);
                    break;
                case NONE:
                    action.setBackgroundColor(Color.WHITE);
                    action.setText(R.string.na);
                    break;
            }
        }

        @Override
        public void onClick(View view){

            // record entry point for nurse
            switch (destinationId){
                case R.id.nav_primary:
                    if(Magic.isConnected(getContext())){
                        Magic.stampThis(getContext(), PRIMARY_ENTRY, patientObject.getPatientId());
                    }
                    break;

                case R.id.nav_secondary:
                    if(Magic.isConnected(getContext())){
                        Magic.stampThis(getContext(), SECONDARY_ENTRY, patientObject.getPatientId());
                    }
                    break;

                case R.id.nav_kiosk:
                    if(Magic.isConnected(getContext())){
                        Magic.stampThis(getContext(), KIOSK_ENTRY, patientObject.getPatientId());
                    }
                    break;
            }
            startActivity(SuperNursePagerActivity.newIntent(getActivity(), patientObject.getPatientId(), destinationId));
        }
    }

    //class to place all patients in recyclerView
    private class PatientAdapter extends RecyclerView.Adapter<PatientHolder>{

        private List<Patient> patientList;
        public PatientAdapter(List<Patient> addressLocations){
            patientList = addressLocations;
        }

        @Override
        public PatientHolder onCreateViewHolder(ViewGroup parent, int viewType){
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.list_item_address, parent, false);
            return new PatientHolder(view);
        }

        @Override
        public void onBindViewHolder(PatientHolder addressholder, int position){
            Patient patient = patientList.get(position);
            addressholder.bindPatient(patient);
        }

        @Override
        public int getItemCount(){ return patientList.size(); }



    }



}
