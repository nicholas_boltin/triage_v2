package com.mycompany.triage_v2.Views;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;



import com.mycompany.triage_v2.R;

public class LogoutDialogFragment extends DialogFragment{

    private static final String TAG = "trace";

    private LogoutDialogListener listener;

    public interface LogoutDialogListener{
        void logoutUser();
    }

    public static LogoutDialogFragment newInstance(){
        return new LogoutDialogFragment();
    }

    public LogoutDialogFragment(){}

    @Nullable
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_assited_kiosk, null);

        return new AlertDialog.Builder(getActivity())
                .setView(view)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                        listener.logoutUser();
                    }
                }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which){

                    }
                }).create();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (LogoutDialogListener) context;
    }


}
