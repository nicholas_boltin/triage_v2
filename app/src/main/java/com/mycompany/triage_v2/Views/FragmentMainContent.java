package com.mycompany.triage_v2.Views;

//import android.app.Fragment;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mycompany.triage_v2.R;

public class FragmentMainContent extends Fragment {

    private static String VALID_LOGIN = "Please provide valid login information";

    private Button loginButton;
    private EditText firstName;
    private EditText lastName;

    mainContentListener listener;

    /***********************************************************************************************
     * Interface listener sends data to activity for data collection
     */
    public interface mainContentListener{
        void submitUserInfo(String first, String last);
        void openDrawer();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_content, container, false);

        firstName = (EditText) view.findViewById(R.id.editText_login_firstName);
        lastName = (EditText) view.findViewById(R.id.editText_login_lastName);

        loginButton = (Button) view.findViewById(R.id.button_login);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!firstName.getText().toString().isEmpty() && !lastName.getText().toString().isEmpty() ){
                    listener.submitUserInfo(firstName.getText().toString(), lastName.getText().toString());
                    listener.openDrawer();
                    firstName.setText("");
                    lastName.setText("");
                } else {
                    Toast.makeText(getContext(), VALID_LOGIN, Toast.LENGTH_SHORT).show();
                }
            }
        });


        return view;
    }

    /***********************************************************************************************
     * attach listener
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (mainContentListener) context;
    }
}
