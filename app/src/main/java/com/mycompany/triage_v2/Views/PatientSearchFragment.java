package com.mycompany.triage_v2.Views;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mycompany.triage_v2.R;

/***************************************************************************************************
 * provides manual search option in BarCodeActivity
 */
public class PatientSearchFragment extends Fragment {

    //private static final String TAG = "trace";

    EditText barcodeSearch;
    Button searchButton;

    testFragmentListener listener;

    /***********************************************************************************************
     * interface listener sends patientId to BarCodeActivity to be searched
     */
    public interface testFragmentListener{
        void patientSearch(String patientId);
    }


    /***********************************************************************************************
     * required empty constructor
     */
    public PatientSearchFragment() {}


    /***********************************************************************************************
     * creates fragment view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_patient_search, container, false);

        barcodeSearch = (EditText) view.findViewById(R.id.edit_text_barcode_search);

        searchButton = (Button) view.findViewById(R.id.button_barcode_search);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.patientSearch(barcodeSearch.getText().toString());
            }
        });

        return view;
    }

    /***********************************************************************************************
     * attaches listener
     */
    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        listener = (testFragmentListener) context;
    }

}
