package com.mycompany.triage_v2.Views;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.abhi.barcode.frag.libv2.BarcodeFragment;
import com.abhi.barcode.frag.libv2.IScanResultHandler;
import com.abhi.barcode.frag.libv2.ScanResult;
import com.mycompany.triage_v2.Helpers.BackgroundTask;
import com.mycompany.triage_v2.Helpers.Magic;
import com.mycompany.triage_v2.Kiosk.KioskTabPagerActivity;
import com.mycompany.triage_v2.MainActivity;
import com.mycompany.triage_v2.Models.Patient;
import com.mycompany.triage_v2.Models.PatientMngr;
import com.mycompany.triage_v2.Models.Triage;
import com.mycompany.triage_v2.R;
import com.mycompany.triage_v2.SuperNurse.ConfirmExitDialogFragment;
import com.mycompany.triage_v2.SuperNurse.SuperNursePagerActivity;

public class BarCodeActivity extends AppCompatActivity implements
        IScanResultHandler,
        PatientSearchFragment.testFragmentListener{

    /***********************************************************************************************
     * controls
     */
    private static final String INTENT_DESTINATION = "intent destination";

    // server stuff
    private static final String ADD_USER = "add_user";

    //server controls
    private static final String PRIMARY_ENTRY = "primary_entry";
    private static final String KIOSK_ENTRY = "kiosk_entry";
    private static final String SECONDARY_ENTRY = "secondary_entry";
    private static final String RESTART = "Restart";


    private BarcodeFragment brf;
    private int destination = 0;

    private PatientMngr patientMngr;
    private Patient patient;

    /***********************************************************************************************
     * creates new intent
     * destination used to determine which mode to go to after receiving patient id
     */
    public static Intent newIntent(Context context, int destination){
        Intent intent = new Intent(context, BarCodeActivity.class);
        intent.putExtra(INTENT_DESTINATION, destination);
        return intent;
    }

    /***********************************************************************************************
     * creates activity
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode((WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN));

        // get patient manager
        patientMngr = PatientMngr.get(this);

        // determine nurse mode after patient found
        destination = getIntent().getIntExtra(INTENT_DESTINATION, 0);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_home_white_24dp);

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_bar_code);
        brf = (BarcodeFragment) getSupportFragmentManager().findFragmentById(R.id.barcodeFragment);
        brf.setDecodeFor(MODE.ONE_D_MODE);
        brf.setScanResultHandler(this);
    }

    /***********************************************************************************************
     * scans barcode
     */
    @Override
    public void scanResult(ScanResult result) {

        //patientSearch(result.getRawResult().getText());
    }

    /***********************************************************************************************
     * takes patientId
     * searches database if patient exists
     * if patient exists, sets patient id to patient object
     * if patient doesn't exist, creates patient and sets triage level to noncritical
     * destination used to determine which mode to pass patient id, and launches new intent
     */
    public void patientSearch(String patientId){

        if(patientId.isEmpty()){
            // TODO: 1/18/17 have valid constraints for patient id - DVJ
            Toast.makeText(this, R.string.enter_valid_id, Toast.LENGTH_SHORT).show();
        } else {
            // check if patient is in db
            if(patientMngr.patientExist(patientId)){
                patient = patientMngr.getPatient(patientId);
            }else{
                // if patient hasn't been put in the system, add them and set triage to noncritical
                patient = new Patient(patientId);
                patient.setTriage(Triage.NONCRITICAL);
                patientMngr.addPatient(patient, this);
            }

            // decide which mode to launch based on which mode started BarCodeActivity
            switch (destination){
                case R.id.nav_primary:
                    if(Magic.isConnected(this)){
                        Magic.stampThis(this, PRIMARY_ENTRY, patientId);
                    } else {

                    }
                    startActivity(SuperNursePagerActivity.
                            newIntent(this, patient.getPatientId(), destination));
                    break;
/*              case R.id.nav_super:
                    startActivity(SuperNursePagerActivity.
                            newIntent(this, patient.getPatientId(), destination));
                    break;
*/
                case R.id.nav_secondary:
                    if(Magic.isConnected(this)){
                        Magic.stampThis(this, SECONDARY_ENTRY, patientId);
                    } else {

                    }
                    startActivity(SuperNursePagerActivity.
                            newIntent(this, patient.getPatientId(), destination));
                    break;
                case R.id.nav_kiosk:
                    if(Magic.isConnected(this)){
                        Magic.stampThis(this, KIOSK_ENTRY, patientId);
                    } else {

                    }
                    startActivity(KioskTabPagerActivity.
                            newIntent(this, patient.getPatientId()));
                    break;
                case R.id.nav_assisted_kiosk:
                    if(Magic.isConnected(this)){
                        Magic.stampThis(this, KIOSK_ENTRY, patientId);
                        // submit user/patient data
                        BackgroundTask backgroundTask = new BackgroundTask(this);
                        backgroundTask.execute(ADD_USER, patientId, patientMngr.getUserId());
                    } else {
                        patientMngr.storeData(Magic.storeDataMiddleMan(ADD_USER, patientId, patientMngr.getUserId()));

                    }
                    startActivity(KioskTabPagerActivity.
                            newIntent(this, patient.getPatientId()));
                    break;
            }
        }

        // restart on faulty scan
        try{
            brf.restart();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***********************************************************************************************
     * Handels menu button clicks
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        switch (menuItem.getItemId()){
            //back to home screen with correct nurse mode
            //check which nurse mode is active.
            //launch dialog if in secondary, otherwise continue to Nurse home
            case android.R.id.home:
                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra(RESTART, android.R.id.home);
                intent.putExtra(INTENT_DESTINATION, destination);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }


}
