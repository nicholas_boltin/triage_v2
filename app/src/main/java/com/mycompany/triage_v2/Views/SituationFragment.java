package com.mycompany.triage_v2.Views;

import android.content.pm.PackageManager;
import android.support.v4.app.Fragment;
//import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.vision.text.Text;
import com.mycompany.triage_v2.Models.PatientMngr;
import com.mycompany.triage_v2.R;

/***************************************************************************************************
 * displays current counts and status of patients in database
 */
public class SituationFragment extends Fragment {

    private static final String TAG = "trace";

    //total
    TextView totalPatients;

    //triageLvl
    TextView criticalPatients, nonCriticalPatients, criticalNoAssessment;

    //exposure
    TextView exposedPatients, potentialExposedPatients, nonExposedPatients, exposureNoAssessment;

    //action
    TextView urgentPatients, monitorPatients, exitPatients, actionNoAssessment;

    /***********************************************************************************************
     * required empty constructor
     */
    public void SituationFragment() {}

    /***********************************************************************************************
     * creates fragment view
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_situation, container, false);


        PatientMngr patientMngr = PatientMngr.get(getActivity());
        int counts[] = patientMngr.getSituationCount();

        //total patients
        totalPatients = (TextView) view.findViewById(R.id.textview_total_patients);
        totalPatients.setText(Integer.toString(counts[0]));

        //triageLvl
        criticalPatients = (TextView) view.findViewById(R.id.textview_critical_patients);
        criticalPatients.setText(Integer.toString(counts[1]));

        nonCriticalPatients = (TextView) view.findViewById(R.id.textview_noncritical_patients);
        nonCriticalPatients.setText(Integer.toString(counts[2]));

        criticalNoAssessment = (TextView) view.findViewById(R.id.textview_critical_noassessment);
        criticalNoAssessment.setText("0");

        //exposure
        exposedPatients  = (TextView) view.findViewById(R.id.textview_exposed_patients);
        exposedPatients.setText(Integer.toString(counts[6]));


        potentialExposedPatients = (TextView) view.findViewById(R.id.textview_potential_exposed_patients);
        potentialExposedPatients.setText(Integer.toString(counts[5]));

        nonExposedPatients = (TextView) view.findViewById(R.id.textview_nonexposed_patients);
        nonExposedPatients.setText(Integer.toString(counts[4]));

        exposureNoAssessment = (TextView) view.findViewById(R.id.textview_exposed_noassessment);
        exposureNoAssessment.setText(Integer.toString(counts[3]));

        //action
        urgentPatients = (TextView) view.findViewById(R.id.textview_urgent_patients);
        urgentPatients.setText(Integer.toString(counts[10]));

        monitorPatients = (TextView) view.findViewById(R.id.textview_monitor_patients);
        monitorPatients.setText(Integer.toString(counts[9]));

        exitPatients = (TextView) view.findViewById(R.id.textview_exit_patients);
        exitPatients.setText(Integer.toString(counts[8]));

        actionNoAssessment = (TextView) view.findViewById(R.id.textview_action_noassessement);
        actionNoAssessment.setText(Integer.toString(counts[7]));

        return view;
    }
}
