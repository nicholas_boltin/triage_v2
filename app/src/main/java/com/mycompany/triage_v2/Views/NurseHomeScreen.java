package com.mycompany.triage_v2.Views;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mycompany.triage_v2.Helpers.BackgroundTask;
import com.mycompany.triage_v2.Helpers.Magic;
import com.mycompany.triage_v2.Models.Patient;
import com.mycompany.triage_v2.Models.PatientMngr;
import com.mycompany.triage_v2.R;
import com.mycompany.triage_v2.SuperNurse.SuperNursePagerActivity;

import java.util.List;

public class NurseHomeScreen extends Fragment{

    private static final String TAG = "trace";

    private static final String DIALOG_PATIENT_SEARCH = "patient_search";
    private static final String SECONDARY_ENTRY = "secondary_entry";

    private static final String BUNDLE_ID = "R.id";

    //controls
    private static final String UPDATE_PATIENTS = "update_patients_update";
    private static final String ZERO = "0";

    //controls which NursePager to load
    private int destinationID = 0;

    private EditText patientSearch;
    private RecyclerView recyclerView;
    private PatientAdapter adapter;
    private SwipeRefreshLayout swipeRefreshLayout;

    private PatientMngr patientMngr;

    private int previousTotal = 0;
    private boolean loading = true;
    private int visibleThreshold = 5;
    int firstVisibleItem, visibleItemCount, totalItemCount;

    private long refreshTimer = System.currentTimeMillis();
    private long newrefreshTimer = 0;

    /***********************************************************************************************
     * creates new instance of fragment
     * receives the id of the user selection from navigation drawer to determine which nurse mode
     * to launch
     */
    public static NurseHomeScreen newInstance(int id){
        NurseHomeScreen fragment = new NurseHomeScreen();
        Bundle bundle = new Bundle();
        bundle.putInt(BUNDLE_ID, id);
        fragment.setArguments(bundle);
        return fragment;
    }

    /***********************************************************************************************
     * required empty constructor
     */
    public NurseHomeScreen() {}

    /***********************************************************************************************
     * gets destination id before onCreate is called
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        destinationID = getArguments().getInt(BUNDLE_ID, 0);
    }

    /***********************************************************************************************
     * creates fragment view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_nurse_home_screen, container, false);

        patientMngr = PatientMngr.get(getContext());

        FloatingActionButton addPatient = (FloatingActionButton)view.
                findViewById(R.id.fab_patient_add);
        addPatient.setOnClickListener(new View.OnClickListener() {

            /***************************************************************************************
             * launches BarCodeActivity with destinationID to direct to the correct mode after
             * barcode scan
             */
            @Override
            public void onClick(View v) {
                startActivity(BarCodeActivity.newIntent(getActivity(), destinationID));
            }
        });

        //search bar field
        patientSearch = (EditText) view.findViewById(R.id.edittext_patient_search);

        Button searchPatient = (Button) view.findViewById(R.id.button_search_nurse_home_screen);

        searchPatient.setOnClickListener(new View.OnClickListener() {

            /***************************************************************************************
             * launches SearchResultsFragment with patient info to search
             */
            @Override
            public void onClick(View v) {
                // get patientMngr
                patientMngr = PatientMngr.get(getContext());

                // check for internet connection before retrieving updated patient list
                if(Magic.isConnected(getContext())) {
                    // update patient list from server to search the latest list
                    BackgroundTask backgroundTask = new BackgroundTask(getContext());
                    backgroundTask.execute(UPDATE_PATIENTS, ZERO, patientMngr.getTimeStamp());
                    Log.i(TAG, "Update time (NurseHomeScreen[Search]): " + patientMngr.getTimeStamp());
                }else {
                        Toast.makeText(getContext(), R.string.no_internet, Toast.LENGTH_SHORT).show();
                }

                FragmentManager fragmentManager = getFragmentManager();
                SearchResultsFragment dialogFragment = SearchResultsFragment.
                        newInstance(patientSearch.getText().toString(), destinationID);
                patientSearch.setText("");
                dialogFragment.show(fragmentManager, DIALOG_PATIENT_SEARCH);
            }
        });

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

 /*       //new stuff
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                firstVisibleItem = layoutManager.findFirstVisibleItemPosition();

                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                    }
                }
                if (!loading && (totalItemCount - visibleItemCount)
                        <= (firstVisibleItem + visibleThreshold)) {
                    // End has been reached

                    loading = true;
                }
            }
        });

*/
        // swipe to refresh recycler view
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(Magic.isConnected(getContext())){
                    // clear adapter of patients and set refreshing to true
                    adapter.clear();
                    swipeRefreshLayout.setRefreshing(true);

                    // update patient list from server
                    BackgroundTask backgroundTask = new BackgroundTask(getContext());
                    backgroundTask.execute(UPDATE_PATIENTS, ZERO, patientMngr.getTimeStamp());
                    Log.i(TAG, "Update time (NurseHomeScreen): " + patientMngr.getTimeStamp());

                    // allow two seconds for patient list to update
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // get new patient list
                            List<Patient> patientList = patientMngr.getPatientListNeedAssessment();
                            // add the new patient list to adapter and refresh to false
                            adapter.append(patientList);
                            swipeRefreshLayout.setRefreshing(false);
                            // update situation fragment
                            loadSituationFragment();
                        }
                    }, 2000);

                } else {
                    Toast.makeText(getContext(), R.string.no_internet, Toast.LENGTH_SHORT).show();
                }
            }

        });

        // set up recycler view w/ adapter
        updateUI();

        // load most up to date situation fragment
        loadSituationFragment();

//        newrefreshTimer = System.currentTimeMillis() - refreshTimer;
//        Log.i(TAG, "Time since last refresh: " + newrefreshTimer);
//
//
//        if(newrefreshTimer > 5000){
//
//            refreshTimer = System.currentTimeMillis();
//            Log.i(TAG, "This if statement works");
//        }


        return view;
    }

    /***********************************************************************************************
     * updates recycler view with new patients that have been added
     */
    private void updateUI(){
        PatientMngr patientMngr = PatientMngr.get(getActivity());
        //List<Patient> patientList = patientMngr.getPatientList();
        List<Patient> patientList = patientMngr.getPatientListNeedAssessment();
        if(adapter == null){
            adapter = new PatientAdapter(patientList);
            recyclerView.setAdapter(adapter);
        }else{
            adapter.notifyDataSetChanged();
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    /***********************************************************************************************
     * loads an updated situation fragment
     */

    public void loadSituationFragment(){
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        SituationFragment fragment = new SituationFragment();;
        fragmentTransaction.replace(R.id.situation_fragment, fragment).commit();
    }

    /***********************************************************************************************
     * creates a holder for patient info
     */
    public class PatientHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView address;
        private TextView patientText;
        private TextView patientId;
        private TextView triageLvl, exposure, action;
        private Patient patientObject;

        /*******************************************************************************************
         * holds patient info to be displayed in recycler view
         */
        public PatientHolder(View view){
            super(view);
            view.setOnClickListener(this);
            address = (TextView) view.findViewById(R.id.textview_address);
            patientText = (TextView) view.findViewById(R.id.textview_patient);
            patientId = (TextView) view.findViewById(R.id.textview_patient_id_number);
            triageLvl = (TextView) view.findViewById(R.id.triage_color_box);
            exposure = (TextView) view.findViewById(R.id.exposure_color_box);
            action = (TextView) view.findViewById(R.id.action_color_box);
        }

        /*******************************************************************************************
         * attaches patient info to holder
         */
        public void bindPatient(Patient patient){
            patientObject = patient;
            address.setText(patient.getAddress());
            patientText.setText(patient.fullName());
            patientId.setText(patient.getPatientId());

            //set triage color
            switch (patientObject.getTriage()){
                case CRITICAL:
                    triageLvl.setBackgroundColor(Color.parseColor("#D32F2F")); //Red
                    triageLvl.setText("Immediate");
                    break;
                case NONCRITICAL:
                    triageLvl.setBackgroundColor(Color.parseColor("#8BC34A")); //Green
                    triageLvl.setText("Not Critical");
                    break;
                case NONE:
                    triageLvl.setBackgroundColor(Color.WHITE);
                    triageLvl.setText(R.string.na);
                    break;
            }

            //set exposure color
            switch (patientObject.getExposureLvl()){
                case EXPOSED:
                    exposure.setBackgroundColor(Color.parseColor("#F57C00")); //Orange
                    exposure.setText("Exposed");
                    break;
                case NOTEXPOSED:
                    exposure.setBackgroundColor(Color.parseColor("#8BC34A")); //Green
                    exposure.setText("No Exposure");
                    break;
                case POTENTIALEXPOSURE:
                    exposure.setBackgroundColor(Color.parseColor("#FFEB3B")); //Yellow
                    exposure.setText("Potential");
                    break;
                case NONE:
                    exposure.setBackgroundColor(Color.WHITE);
                    exposure.setText(R.string.na);
                    break;
            }

            //set action color
            switch (patientObject.getAction()){
                case URGENT:
                    action.setBackgroundColor(Color.parseColor("#D32F2F")); //Red
                    action.setText("Urgent");
                    break;
                case MONITOR:
                    action.setBackgroundColor(Color.parseColor("#FFEB3B")); //Yellow
                    action.setText("Monitor");
                    break;
                case EXIT:
                    action.setBackgroundColor(Color.GRAY);
                    action.setText("Return");
                    break;
                case NONE:
                    action.setBackgroundColor(Color.WHITE);
                    action.setText(R.string.na);
                    break;
            }
        }

        /*******************************************************************************************
         * onClick event for recycler view
         * patientId is passed to SuperNursePagerActivity to load the patient selected
         * destinationID is passed to load the correct nurse mode
         */
        @Override
        public void onClick(View view){
            if(Magic.isConnected(getContext())){
                Magic.stampThis(getContext(), SECONDARY_ENTRY, patientObject.getPatientId());
            } else {
                Toast.makeText(getContext(), "No Internet", Toast.LENGTH_SHORT).show();
            }
            startActivity(SuperNursePagerActivity.
                    newIntent(getActivity(), patientObject.getPatientId(), destinationID));
        }
    }

    /***********************************************************************************************
     * creates recycler view
     */
    private class PatientAdapter extends RecyclerView.Adapter<PatientHolder>{

        private List<Patient> patientList;

        /*******************************************************************************************
         * constructor
         * gets the list of patients
         */
        public PatientAdapter(List<Patient> addressLocations){
            patientList = addressLocations;
        }

        /*******************************************************************************************
         * creates recycler view
         */
        @Override
        public PatientHolder onCreateViewHolder(ViewGroup parent, int viewType){
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.list_item_address, parent, false);
            return new PatientHolder(view);
        }

        /*******************************************************************************************
         * binds each patient to a holder
         */
        @Override
        public void onBindViewHolder(PatientHolder addressholder, int position){
            Patient patient = patientList.get(position);
            addressholder.bindPatient(patient);
        }

        /*******************************************************************************************
         * sets the size of recycler view
         */
        @Override
        public int getItemCount(){
            return patientList.size();
        }

        /*******************************************************************************************
         * clears patient list
         */
        private void clear(){
            patientList.clear();
            notifyDataSetChanged();
        }

        /*******************************************************************************************
         * adds new data to patient list
         */
        private void append(List<Patient> patients){
            patientList.addAll(patients);
            notifyDataSetChanged();
        }

    }

}
