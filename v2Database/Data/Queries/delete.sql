/*Delete all entries from triage db*/


/*see size of all tables before delete*/
SELECT
(SELECT COUNT(*) FROM patients) AS patients,
(SELECT COUNT(*) FROM triage_lvl) AS triage,
(SELECT COUNT(*) FROM answers) AS answers,
(SELECT COUNT(*) FROM vitals) AS vitals,
(SELECT COUNT(*) FROM user_patients) AS users_,
(SELECT COUNT(*) FROM rec_triage_lvl) AS rec_triage,
(SELECT COUNT(*) FROM time_stamp) AS time_stamp,
(SELECT COUNT(*) FROM comments) AS comments,
(SELECT COUNT(*) FROM users) AS users,
(SELECT COUNT(*) FROM mobile_update) as mobile;

/*delete all entries*/
DELETE from patients;
DELETE from triage_lvl;
DELETE from answers;
DELETE from vitals;
DELETE from users;
DELETE from rec_triage_lvl;
DELETE from time_stamp;
DELETE from comments;
DELETE from user_patients;
DELETE from mobile_update;

/*see size of all tables after delete*/
SELECT
(SELECT COUNT(*) FROM patients) AS patients,
(SELECT COUNT(*) FROM triage_lvl) AS triage,
(SELECT COUNT(*) FROM answers) AS answers,
(SELECT COUNT(*) FROM vitals) AS vitals,
(SELECT COUNT(*) FROM user_patients) AS users_,
(SELECT COUNT(*) FROM rec_triage_lvl) AS rec_triage,
(SELECT COUNT(*) FROM time_stamp) AS time_stamp,
(SELECT COUNT(*) FROM comments) AS comments,
(SELECT COUNT(*) FROM users) AS users,
(SELECT COUNT(*) FROM mobile_update) as mobile;




















