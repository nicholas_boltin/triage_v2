-- 129.252.131.78


-- select the last 10 patients to enter the system
/*
SELECT patient_id, kiosk_complete, initial_entry, updated 
FROM mobile_update
ORDER BY updated desc
limit 10;


-- select the last 10 patients to complete the kiosk
SELECT patient_id, kiosk_complete, initial_entry, updated 
FROM mobile_update
WHERE kiosk_complete = '1'
ORDER BY updated desc
limit 10;

-- select the last 10 patients to complete the kiosk and show completion time
SELECT patient_id, kiosk_entry, kiosk_exit, timediff(kiosk_exit, kiosk_entry) AS completion
FROM time_stamp
WHERE kiosk_exit IS NOT NULL
ORDER BY kiosk_exit desc
limit 10;


-- select the last 10 patients to complete the triage process
SELECT patient_id, entry_ts, exit_ts, timediff(exit_ts, entry_ts) AS completion
FROM time_stamp
WHERE exit_ts IS NOT NULL
ORDER BY exit_ts desc
limit 10;
*/

-- select the avg completion time of the kiosk
SELECT AVG (timestampdiff(minute, kiosk_entry, kiosk_exit)) AS average
FROM time_stamp
WHERE kiosk_exit IS NOT NULL
ORDER BY kiosk_exit desc;


-- select the avg completion time of the triage process
SELECT AVG (timestampdiff(minute, entry_ts, exit_ts)) AS average
FROM time_stamp
WHERE exit_ts IS NOT NULLtee
ORDER BY exit_ts desc;

-- get counts of triage situation
SELECT
(SELECT COUNT(*) FROM mobile_update) AS patients,
(SELECT COUNT(*) FROM mobile_update WHERE p_triage = 'NONCRITICAL') AS non_critical,
(SELECT COUNT(*) FROM mobile_update WHERE p_triage = 'CRITICAL') AS critical,
(SELECT COUNT(*) FROM mobile_update WHERE exposure = 'NONE') AS none_exp,
(SELECT COUNT(*) FROM mobile_update WHERE exposure = 'NOTEXPOSED') AS not_exposure,
(SELECT COUNT(*) FROM mobile_update WHERE exposure = 'POTENTIALEXPOSURE') AS potential,
(SELECT COUNT(*) FROM mobile_update WHERE exposure = 'EXPOSED') AS exposed,
(SELECT COUNT(*) FROM mobile_update WHERE action = 'NONE') AS none_act,
(SELECT COUNT(*) FROM mobile_update WHERE action = 'EXIT') AS return_hosp,
(SELECT COUNT(*) FROM mobile_update WHERE action = 'MONITOR') AS monitor,
(SELECT COUNT(*) FROM mobile_update WHERE action = 'URGENT') AS urgent;

/*
select patient_id, kiosk_entry, kiosk_exit
FROM time_stamp
WHERE patient_id in (SELECT patient_id
FROM mobile_update
WHERE kiosk_complete = '1'
ORDER BY updated desc
limit 10);

select timediff(kiosk_exit, kiosk_entry) as time
from time_stamp;

SELECT patient_id, kiosk_entry, kiosk_exit
FROM time_stamp
WHERE kiosk_exit IS NOT NULL
ORDER BY kiosk_exit desc
limit 10;

SELECT patient_id, kiosk_entry, kiosk_exit, timestampdiff(minute, kiosk_entry, kiosk_exit) as completion
FROM time_stamp
WHERE kiosk_exit IS NOT NULL
ORDER BY kiosk_exit desc
limit 10;

*/
