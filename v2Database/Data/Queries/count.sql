/*See size of all tables in triage db*/


/*see size of all tables*/
SELECT
(SELECT COUNT(*) FROM patients) AS patients,
(SELECT COUNT(*) FROM triage_lvl) AS triage,
(SELECT COUNT(*) FROM answers) AS answers,
(SELECT COUNT(*) FROM vitals) AS vitals,
(SELECT COUNT(*) FROM user_patients) AS users_,
(SELECT COUNT(*) FROM rec_triage_lvl) AS rec_triage,
(SELECT COUNT(*) FROM time_stamp) AS time_stamp,
(SELECT COUNT(*) FROM comments) AS comments,
(SELECT COUNT(*) FROM users) AS users,
(SELECT COUNT(*) FROM mobile_update) as mobile;
