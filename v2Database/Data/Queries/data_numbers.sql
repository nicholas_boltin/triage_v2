/*see size of all tables*/
SELECT
(SELECT COUNT(*) FROM patients) AS patients,
(SELECT COUNT(*) FROM triage_lvl) AS triage,
(SELECT COUNT(*) FROM answers) AS answers,
(SELECT COUNT(*) FROM vitals) AS vitals,
(SELECT COUNT(*) FROM user_patients) AS users_,
(SELECT COUNT(*) FROM rec_triage_lvl) AS rec_triage,
(SELECT COUNT(*) FROM time_stamp) AS time_stamp,
(SELECT COUNT(*) FROM comments) AS comments,
(SELECT COUNT(*) FROM users) AS users,
(SELECT COUNT(*) FROM mobile_update) as mobile;

-- get counts of triage situation
SELECT
(SELECT COUNT(*) FROM mobile_update) AS patients,
(SELECT COUNT(*) FROM mobile_update WHERE p_triage = 'NONCRITICAL') AS non_critical,
(SELECT COUNT(*) FROM mobile_update WHERE p_triage = 'CRITICAL') AS critical,
(SELECT COUNT(*) FROM mobile_update WHERE exposure = 'NONE') AS none_exp,
(SELECT COUNT(*) FROM mobile_update WHERE exposure = 'NOTEXPOSED') AS not_exposure,
(SELECT COUNT(*) FROM mobile_update WHERE exposure = 'POTENTIALEXPOSURE') AS potential,
(SELECT COUNT(*) FROM mobile_update WHERE exposure = 'EXPOSED') AS exposed,
(SELECT COUNT(*) FROM mobile_update WHERE action = 'NONE') AS none_act,
(SELECT COUNT(*) FROM mobile_update WHERE action = 'EXIT') AS return_hosp,
(SELECT COUNT(*) FROM mobile_update WHERE action = 'MONITOR') AS monitor,
(SELECT COUNT(*) FROM mobile_update WHERE action = 'URGENT') AS urgent;

-- count patients that received triage recommendation
SELECT COUNT(DISTINCT patient_id) AS triage_rec_counts FROM rec_triage_lvl;

-- get counts of rec triage from view recTriage
SELECT
(SELECT COUNT(*) FROM recTriage WHERE exposure = 'NONE') AS not_exposure,
(SELECT COUNT(*) FROM recTriage WHERE exposure = 'NOTEXPOSED') AS not_exposure,
(SELECT COUNT(*) FROM recTriage WHERE exposure = 'POTENTIALEXPOSURE') AS potential,
(SELECT COUNT(*) FROM recTriage WHERE exposure = 'EXPOSED') AS exposed,
(SELECT COUNT(*) FROM recTriage WHERE action = 'NONE') AS none_act,
(SELECT COUNT(*) FROM recTriage WHERE action = 'EXIT') AS return_hosp,
(SELECT COUNT(*) FROM recTriage WHERE action = 'MONITOR') AS monitor,
(SELECT COUNT(*) FROM recTriage WHERE action = 'URGENT') AS urgent;


-- view of all distinct patient recommendations
create view drill_rec_triage as select distinct patient_id, exposure, action from rec_triage_lvl;

-- show duplicate patient id f/ rec triage from view recTriage
SELECT patient_id, COUNT(*) AS dups FROM recTriage GROUP BY patient_id HAVING dups > 1;


-- view of patients w/ multiple recommendations
create view duplicates as select patient_id from (SELECT patient_id, COUNT(*) as dups FROM recTriage GROUP BY patient_id HAVING dups > 1)as test;

-- multiple recommendations for patients from view duplicate
SELECT DISTINCT patient_id, exposure, action FROM rec_triage_lvl WHERE patient_id IN (SELECT patient_id FROM duplicates) ORDER BY patient_id;

-- duplicates patients in time_stamp table
select patient_id, count(*) as counts from time_stamp group by patient_id having counts >1;


-- select the avg completion time of the kiosk from view drill_timestamp
-- drill_timestamp selects all the entries from time_stamp after 11:00 
SELECT AVG (timestampdiff(minute, kiosk_entry, kiosk_exit)) AS average
FROM drill_timestamp
WHERE kiosk_exit IS NOT NULL;


-- select the avg completion time of the triage process from view drill_timestamp
-- drill_timestamp selects all the entries from time_stamp after 11:00 
SELECT AVG (timestampdiff(minute, entry_ts, exit_ts)) AS average
FROM drill_timestamp
WHERE exit_ts IS NOT NULL;

-- patients w/ no action and exposure
select patient_id, exposure, action from mobile_update where action = 'NONE' and exposure = 'NONE';

-- show all addresses not in drill/training areas
select patient_id, address from patients where address not like '%columbia%' and address not like '%augusta%' and address not like '%aiken%';

select * from drill_timestamp where exit_ts is null;

-- sample set
create view drill_sample_set as select distinct patient_id from time_stamp where entry_ts < '2017-04-04 11:15:00';

-- select timestamps for sample set
select * from time_stamp where patient_id in (select patient_id from sampleSet);


-- select timestamps f/ each question and save to .csv
select patient_id, question_number, time_stamp 
from answers
order by patient_id, time_stamp
into outfile '/var/lib/mysql-files/answer_timestamps.csv'
fields terminated by ','
/* enclosed by '''' */
lines terminated by '\n';


-- create csv files of tables
select * from (
select 'id', 'patient id', 'num', '1', '2', '3', '4', '5', '6', 'time stamp'
union all
(select * from answers)
) resulting_set
into outfile '/var/lib/mysql-files/answers.csv'
fields terminated by '\t'
enclosed by ''''
lines terminated by '\n';


-- create csv files of mobile
select patient_id, last_name, first_name, dob, address, latitude, longitude,
heart_rate, oxygen, respiration_rate, p_triage, exposure, action,
q_one, q_one_a, q_one_b, q_one_c, q_one_d,
q_two, q_two_a, q_two_b, q_two_c,
q_three, q_four, q_five,
kiosk_complete, initial_entry, updated
from mobile_update
into outfile '/var/lib/mysql-files/mobile.csv'
fields terminated by '\t'
lines terminated by '\n';

select patient_id from mobile_update where updated > '2017-04-05 00:01:00';

--views of drill data
create view drill_answers as select * from answers where time_stamp > '2017-04-04 11:15:00';

create view drill_vitals as select * from vitals where time_stamp > '2017-04-04 11:15:00';

create view drill_patients as select patient_id from patients;

create view drill_triage as select * from triage_lvl where time_stamp > '2017-04-04 11:15:00';


create VIEW `drill_timestamp` AS select `time_stamp`.`_id` AS `_id`,`time_stamp`.`patient_id` AS `patient_id`,`time_stamp`.`entry_ts` AS `entry_ts`,`time_stamp`.`exit_ts` AS `exit_ts`,`time_stamp`.`kiosk_entry` AS `kiosk_entry`,`time_stamp`.`kiosk_exit` AS `kiosk_exit`,`time_stamp`.`secondary_entry` AS `secondary_entry`,`time_stamp`.`vitals_start` AS `vitals_start`,`time_stamp`.`vitals_end` AS `vitals_end` from `time_stamp` where (`time_stamp`.`entry_ts` > '2017-04-04 11:00:00');


-- answers w entry exit nurse id and save to file
select distinct a.patient_id, 
kiosk_entry,
a.question_number, a.time_stamp, 
b.question_number, b.time_stamp,
c.question_number, c.time_stamp,
d.question_number, d.time_stamp,
e.question_number, e.time_stamp,
f.question_number, f.time_stamp,
drill_vitals.time_stamp,
kiosk_exit,
user_id
from drill_answers as a, drill_answers as b, drill_answers as c, drill_answers as d, drill_answers as e, drill_answers as f,
drill_timestamp, drill_vitals, user_patients
where a.patient_id = b.patient_id
and b.patient_id = c.patient_id
and c.patient_id = d.patient_id
and d.patient_id = e.patient_id
and e.patient_id = f.patient_id
and drill_timestamp.patient_id = a.patient_id
and drill_vitals.patient_id = a.patient_id
and user_patients.patient_id = a.patient_id
and a.question_number = 6
and b.question_number = 1
and c.question_number = 2
and d.question_number = 3
and e.question_number = 4
and f.question_number = 5
and respiration_rate = 0
and a.time_stamp < kiosk_exit
and b.time_stamp < kiosk_exit
and c.time_stamp < kiosk_exit 
and d.time_stamp < kiosk_exit
and e.time_stamp < kiosk_exit
and f.time_stamp < kiosk_exit
and drill_vitals.time_stamp < kiosk_exit
order by a.patient_id, kiosk_entry, a.time_stamp, b.time_stamp, c.time_stamp, d.time_stamp, e.time_stamp, f.time_stamp, drill_vitals.time_stamp, kiosk_exit
into outfile '/var/lib/mysql-files/NicksData.csv'
fields terminated by ','
/* enclosed by '''' */
lines terminated by '\n';



-- search f/ missing patients


select patient_id from drill_patients where patient_id not in (select patient_id from test);


-- view of nicks data 63 missing patients - due to missing nurse field
create view test as 
select distinct a.patient_id, 
kiosk_entry,
a.question_number as one, a.time_stamp as 6t, 
b.question_number as two, b.time_stamp as 1t,
c.question_number as three, c.time_stamp as 2t,
d.question_number as four, d.time_stamp as 3t,
e.question_number as five , e.time_stamp as 4t,
f.question_number as six, f.time_stamp as 5t,
drill_vitals.time_stamp,
kiosk_exit,
user_id
from drill_answers as a, drill_answers as b, drill_answers as c, drill_answers as d, drill_answers as e, drill_answers as f,
drill_timestamp, drill_vitals, user_patients
where a.patient_id = b.patient_id
and b.patient_id = c.patient_id
and c.patient_id = d.patient_id
and d.patient_id = e.patient_id
and e.patient_id = f.patient_id
and drill_timestamp.patient_id = a.patient_id
and drill_vitals.patient_id = a.patient_id
and user_patients.patient_id = a.patient_id
and a.question_number = 6
and b.question_number = 1
and c.question_number = 2
and d.question_number = 3
and e.question_number = 4
and f.question_number = 5
and respiration_rate = 0
and a.time_stamp < kiosk_exit
and b.time_stamp < kiosk_exit
and c.time_stamp < kiosk_exit 
and d.time_stamp < kiosk_exit
and e.time_stamp < kiosk_exit
and f.time_stamp < kiosk_exit
and drill_vitals.time_stamp < kiosk_exit
order by a.patient_id, kiosk_entry, a.time_stamp, b.time_stamp, c.time_stamp, d.time_stamp, e.time_stamp, f.time_stamp, drill_vitals.time_stamp, kiosk_exit;


-- view of nicks data w/o nurse id 29 missing - due to missing timestamps in answer 6
create view test as 
select distinct a.patient_id, 
kiosk_entry,
a.question_number prelim, a.time_stamp pt, 
b.question_number one, b.time_stamp 1t,
c.question_number two, c.time_stamp 2t,
d.question_number three, d.time_stamp 3t,
e.question_number four, e.time_stamp 4t,
f.question_number five, f.time_stamp 5t,
drill_vitals.time_stamp,
kiosk_exit
from drill_answers as a, drill_answers as b, drill_answers as c, drill_answers as d, drill_answers as e, drill_answers as f,
drill_timestamp, drill_vitals
where a.patient_id = b.patient_id
and b.patient_id = c.patient_id
and c.patient_id = d.patient_id
and d.patient_id = e.patient_id
and e.patient_id = f.patient_id
and drill_timestamp.patient_id = a.patient_id
and drill_vitals.patient_id = a.patient_id
and a.question_number = 6
and b.question_number = 1
and c.question_number = 2
and d.question_number = 3
and e.question_number = 4
and f.question_number = 5
and respiration_rate = 0
and a.time_stamp < kiosk_exit
and b.time_stamp < kiosk_exit
and c.time_stamp < kiosk_exit 
and d.time_stamp < kiosk_exit
and e.time_stamp < kiosk_exit
and f.time_stamp < kiosk_exit
and drill_vitals.time_stamp < kiosk_exit
order by a.patient_id, kiosk_entry, a.time_stamp, b.time_stamp, c.time_stamp, d.time_stamp, e.time_stamp, f.time_stamp, drill_vitals.time_stamp, kiosk_exit
into outfile '/var/lib/mysql-files/NewTimeStampTable.csv'
fields terminated by ','
/* enclosed by '''' */
lines terminated by '\n';


-- view of nicks data w/o nurse id w/o prelim 7 missing due to missing timestamp f/ comp and vitals
create view test as 
select distinct b.patient_id, 
kiosk_entry,
b.question_number one, b.time_stamp 1t,
c.question_number two, c.time_stamp 2t,
d.question_number three, d.time_stamp 3t,
e.question_number four, e.time_stamp 4t,
f.question_number five, f.time_stamp 5t,
drill_vitals.time_stamp,
kiosk_exit
from drill_answers as b, drill_answers as c, drill_answers as d, drill_answers as e, drill_answers as f,
drill_timestamp, drill_vitals
where b.patient_id = c.patient_id
and c.patient_id = d.patient_id
and d.patient_id = e.patient_id
and e.patient_id = f.patient_id
and drill_timestamp.patient_id = b.patient_id
and drill_vitals.patient_id = b.patient_id
and b.question_number = 1
and c.question_number = 2
and d.question_number = 3
and e.question_number = 4
and f.question_number = 5
and respiration_rate = 0
and b.time_stamp < kiosk_exit
and c.time_stamp < kiosk_exit 
and d.time_stamp < kiosk_exit
and e.time_stamp < kiosk_exit
and f.time_stamp < kiosk_exit
and drill_vitals.time_stamp < kiosk_exit
order by b.patient_id, kiosk_entry, b.time_stamp, c.time_stamp, d.time_stamp, e.time_stamp, f.time_stamp, drill_vitals.time_stamp, kiosk_exit;


-- all questions answered before vitals timestamp

select distinct a.patient_id, 
kiosk_entry,
a.question_number prelim, a.time_stamp pt, 
b.question_number one, b.time_stamp 1t,
c.question_number two, c.time_stamp 2t,
d.question_number three, d.time_stamp 3t,
e.question_number four, e.time_stamp 4t,
f.question_number five, f.time_stamp 5t,
vitals_start,
vitals_end,
kiosk_exit
from drill_answers as a, drill_answers as b, drill_answers as c, drill_answers as d, drill_answers as e, drill_answers as f,
drill_timestamp
where a.patient_id = b.patient_id
and b.patient_id = c.patient_id
and c.patient_id = d.patient_id
and d.patient_id = e.patient_id
and e.patient_id = f.patient_id
and drill_timestamp.patient_id = a.patient_id
and a.question_number = 6
and b.question_number = 1
and c.question_number = 2
and d.question_number = 3
and e.question_number = 4
and f.question_number = 5
and a.time_stamp < vitals_start
and b.time_stamp < vitals_start
and c.time_stamp < vitals_start 
and d.time_stamp < vitals_start
and e.time_stamp < vitals_start
and f.time_stamp < vitals_start
and vitals_start < kiosk_exit
and vitals_end < kiosk_exit
order by a.patient_id, kiosk_entry, a.time_stamp, b.time_stamp, c.time_stamp, d.time_stamp, e.time_stamp, f.time_stamp, vitals_start, vitals_end, kiosk_exit
into outfile '/var/lib/mysql-files/NoEditsAtOverview.csv'
fields terminated by ','
/* enclosed by '''' */
lines terminated by '\n';


select distinct a.patient_id, a.exposure 
from drill_rec_triage as a
inner join (drill_triage as b)
on (a.patient_id = b.patient_id and a.exposure = b.exposure)
order by patient_id;

258 rows in set (0.00 sec)



select distinct a.patient_id, a.action 
from drill_rec_triage as a
inner join (drill_triage as b)
on (a.patient_id = b.patient_id and a.action = b.action)
order by patient_id;


create view drill_triage as select * from triage_lvl where time_stamp > '2017-04-04 11:15:00';

select patient_id, exposure, action 
from drill_triage 
order by time_stamp
into outfile '/var/lib/mysql-files/nurse_triage.csv'
fields terminated by ','
/* enclosed by '''' */
lines terminated by '\n';


select patient_id, exposure, action 
from drill_rec_triage 
order by patient_id
into outfile '/var/lib/mysql-files/sys_triage.csv'
fields terminated by ','
/* enclosed by '''' */
lines terminated by '\n';

select patient_id, updated from mobile_update order by patient_id
into outfile '/var/lib/mysql-files/patients.csv'
fields terminated by ','
lines terminated by '\n';

/* sudo -i to cd into mysql-files */

