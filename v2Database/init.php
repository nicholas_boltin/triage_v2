<?php

//connection script for triage db

$db_name = "triage";
$mysql_user = "triageserver";
$mysql_pass = "triageserver";
$server_name = "localhost";

$con = mysqli_connect($server_name, $mysql_user, $mysql_pass, $db_name);

if(!$con){
	die("Error in connection ". mysqli_connect_error());
} else {
	//echo "Database connection success";
}

?>
