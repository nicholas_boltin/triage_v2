<?php

//get the current patients

//require login file
require "init.php";

//sql query
$sql_get_patients = "SELECT * FROM patients;";
//sql_get_patients = "SELECT * FROM mobile_update;";

$sql_get_answers = "SELECT * FROM answers;";
$sql_get_vitals = "SELECT * FROM vitals;";
$sql_get_triage = "SELECT * FROM triage_lvl";


//results from query
$result = mysqli_query($con, $sql_get_patients);
$result_answers = mysqli_query($con, $sql_get_answers);
$result_vitals = mysqli_query($con, $sql_get_vitals);
$result_triage = mysqli_query($con, $sql_get_triage);


//array to hold results that will be encoded and sent
$response = array();
$response_answers = array();
$response_vitals = array();
$response_triage = array();


//place all results in array
while($row = mysqli_fetch_array($result)){
	//place the result at row in the spot in the array with the label
	array_push($response, array("patient_id"=>$row[1], "last_name"=>$row[2], "first_name"=>$row[3], "dob"=>$row[4], "address"=>$row[5], "latitude"=>$row[6], "longitude"=>$row[7]));
}


while($row = mysqli_fetch_array($result_answers)){
	array_push($response_answers, array("patient_id"=>$row[1], "question_number"=>$row[2], "answer_one"=>$row[3], "answer_two"=>$row[4], "answer_three"=>$row[5], "answer_four"=>$row[6], "answer_five"=>$row[7], "answer_six"=>$row[8]));
}


while($row = mysqli_fetch_array($result_vitals)){
	array_push($response_vitals, array("patient_id"=>$row[1], "heart_rate"=>$row[2], "oxygen"=>$row[3], "respiration_rate"=>$row[4]));
}


while($row = mysqli_fetch_array($result_triage)){
	array_push($response_triage, array("patient_id"=>$row[1], "p_triage"=>$row[2], "exposure"=>$row[3], "action"=>$row[4]));
}


//send array as an encoded json to app
echo json_encode(array("server_response" => $response, "patient_answers" => $response_answers, "patient_vitals" => $response_vitals, "patient_triage" => $response_triage));

//close sql session
mysqli_close($con);

?>
