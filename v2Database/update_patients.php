<?php

//get the current patients

//require login file
require "init.php";

$updated = $_POST['time_stamp'];

//sql query
$sql_get_patients = "SELECT * FROM mobile_update WHERE updated > '$updated' ORDER BY updated;";

//results from query
$result = mysqli_query($con, $sql_get_patients);

//array to hold results that will be encoded and sent
$response = array();

//place all results in array
while($row = mysqli_fetch_array($result)){
        //place the result at row in the spot in the array with the label
        array_push($response, array("patient_id"=>$row[1], "last_name"=>$row[2], "first_name"=>$row[3], "dob"=>$row[4], "address"=>$row[5], "latitude"=>$row[6], "longitude"=>$row[7], "heart_rate"=>$row[8], "oxygen"=>$row[9], "respiration_rate"=>$row[10], "p_triage"=>$row[11], "exposure"=>$row[12], "action"=>$row[13], "q_six"=>$row[14], "q_one"=>$row[15], "q_one_a"=>$row[16], "q_one_b"=>$row[17], "q_one_c"=>$row[18], "q_one_d"=>$row[19], "q_two"=>$row[20], "q_two_a"=>$row[21], "q_two_b"=>$row[22], "q_two_c"=>$row[23], "q_three"=>$row[24], "q_four"=>$row[25], "q_five"=>$row[26], "kiosk_complete"=>$row[27], "e_comments"=>$row[28], "a_comments"=>$row[29], "initial_entry"=>$row[30], "updated"=>$row[31]));
}

//send array as an encoded json to app
echo json_encode(array("server_response" => $response));

//close sql session
mysqli_close($con);

?>







