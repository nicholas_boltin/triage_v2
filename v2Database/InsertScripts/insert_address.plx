#!/usr/bin/perl
#insert_address.plx
use DBI;
#use DBD::mysql;
use warnings;
use strict;


my $source = "FabricatedAddresses.txt";
my @array;

my $address;
my $lat;
my $lon;

#---

#db name
my $database = "triage";

my $connection = ConnectDB($database);

my $query = "";

my $statement = $connection -> prepare($query);

#---

open IN, $source or die "Can't read source file $source $!\n";



while(<IN>){

	chomp $_;
	@array = split /:/, $_;
	#street num, street, city, state, zip
	$address = "@array[0] @array[1] @array[2] @array[3] @array[4]";
	$lat = @array[5];
	$lon = @array[6];
	print "$address\n";
	print "$lat\t$lon\n";
}

close(IN);

#---sub-routine---
sub ConnectDB{
#-----------------

my ($db) = @_;

#open access file to retrieve db, user, password
open(ACCESS, "access") || die "Can't access db";

#access variables
my $database = <ACCESS>;
my $userid = <ACCESS>;
my $passwd = <ACCESS>;

#connect to db
my $connect = "dbi:mysql:$db";

#close file
close(ACCESS);

chomp($database, $userid, $passwd);

my $connection = DBI -> connect($connect, $userid, $passwd);

return $connection;

}
#---end sub-routine---
