db_connect.php
<?php

class DB_CONNECT{
	
	// constructor - connects to db
	function __construct(){
		$this -> connect();
	}

	// destructor - closes db connection
	function __destruct(){
		$this -> close();
	}

	/** connect to db
	*/
	function connect(){

		require_once __DIR__ . '/db_config.php';
		
		// connect to mysql db
		$con = mysqli_connect(SERVER, DB_USER, DB_PASSWORD, DB_NAME) or die (mysql_error());

		return $con;

	}

	/** close connection to db
	*/
	function close(){
		mysql_close();
	}

}


?>

