<?php
//insert_db.php
//reads file to be processed into triage


//connect to triage db
require "init.php";

//open file read only to be processed
$file = fopen("FabricatedAddresses.txt", "r") or die();

//variables that will be inserted into db
$patient_id = 2001;
$lat = "";
$lon = "";
$address = "";
$exposure = "";
$action = "";
$triage = "NONCRITICAL";

echo "Reading file". $file. "\n";

//while not reaching eof
while(!feof($file)){
	
	//get line of file
	$line = fgets($file);

	//parse line at ":"
	$string = explode(":", $line);
	//street address
	//street # - 0, street name - 1, city - 2, state - 3, zip - 4
	$address = $string[0]. " " .$string[1]. " " .$string[2]. " " .$string[3]. " " .$string[4];
	//latitude and longitude
	$lat = $string[5];
	$lon = $string[6];
	//make sure string is of appropriate size
	$address = substr($address, 0, 255);
	$lat = substr($lat, 0, 7);
	$lon = substr($lon, 0, 7);


	//insert statement - must come after variables have been set
	$sql = "INSERT INTO mobile_update(patient_id, address, latitude, longitude) VALUES('$patient_id', '$address', '$lat', '$lon');";

	//adding triage, exposure, and action
	if ($patient_id < 2150) {
		$exposure = "NONE";
		$action = "NONE";
	} else {
		$exposure = "EXPOSED";
		$action = "URGENT";
	}

	if ($patient_id > 2250) {
		$exposure = "NOTEXPOSED";
		$action = "MONITOR";
	}
	//set triage levels update statement
	$sql_update = "INSERT INTO mobile_update(patient_id, p_triage, exposure, action) VALUES('$patient_id', '$triage', '$exposure', '$action') ON DUPLICATE KEY UPDATE p_triage = '$triage', exposure = '$exposure', action = '$action';";	

	//mysqli_query($con, $sql_update);


	//insert data into db
	//echo success
	//echo error w/ sql error statement

	if(mysqli_query($con, $sql)){
		echo "successful insertion of " .$address. " " .$lat ." " .$lon. "Id:" .$patient_id. "\n";
		mysqli_query($con, $sql_update);
	} else {
		echo "Error: " . $sql . "<br>" . mysqli_error($con). "\n";
	}

	//if($patient_id == 2301) break;

	//increment id
	$patient_id++;
	
}

echo "end of op\n";
//echo fread($file, filesize("FabricatedAddresses.txt"));

//close file
fclose($file);
?>
